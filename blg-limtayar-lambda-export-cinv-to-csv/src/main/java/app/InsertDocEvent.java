package app;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.DocumentEventServiceImpl;
import com.bigledger.core1.adapter.integration.services.IDocumentEventService;
import com.bigledger.core1.adapter.integration.services.IPurchaseOrderService;
import com.bigledger.core1.adapter.integration.services.PurchaseOrderServiceImpl;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.api.ResponseCodeConstants;
import com.bigledger.core1.api.criteria.DocEventCriteria;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.CustomFieldDto;
import com.bigledger.core1.dto.GenericDocEventDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.id.PagingResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

public class InsertDocEvent {

    public boolean insertEvent(String guid, Map<String, String> eventCode, RequestHeaders requestHeaders, LambdaLogger logger) throws InterruptedException, AkaunApiServerException, NetworkException {




                GenericDocEventDto exportMessageEventSynced = funConvertToGenDocEvent(guid, eventCode, requestHeaders);

                IDocumentEventService docEventService = new DocumentEventServiceImpl();

                try {
                    docEventService.createDocumentEvent(exportMessageEventSynced, requestHeaders);
                }
                catch (AkaunApiServerException e){
                    System.out.println(e.getMessage());
                    return false;
                }
                catch (NetworkException e){
                    System.out.println(e.getMessage());
                    return false;
                }
                catch (Exception e){
                    System.out.println(e.getMessage());
                    return false;
                }



        return true;

    }

    private static GenericDocEventDto funConvertToGenDocEvent(String docHdrGuid,  Map<String, String> eventCode, RequestHeaders requestHeaders) throws InterruptedException, AkaunApiServerException, NetworkException {

        GenericDocEventDto genericDocEventDto = new GenericDocEventDto();

        DocEventCriteria DocEventCriteria = new DocEventCriteria();
        DocEventCriteria.setGuidDochdr(docHdrGuid);
        DocEventCriteria.setAction("SALES_INVOICE_CREATED");
        IDocumentEventService CinveventService = new DocumentEventServiceImpl();
//            PagingResponse SOEventResponse = new PagingResponse();
        PagingResponse<GenericDocEventDto> SOEventResponse = CinveventService.getGenericDocEventDtoByCriteriaProcess(DocEventCriteria, requestHeaders);

        List<GenericDocEventDto> genericDocEventDtosList = SOEventResponse.getObjectList();

        GenericDocEventDto genericDocEventDto1 = genericDocEventDtosList.get(0);

        genericDocEventDto.setGuidDocHdr(docHdrGuid);
        genericDocEventDto.setDateTxn(ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur")));
        genericDocEventDto.setCreatedDate(ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur")));
        genericDocEventDto.setEventCode(eventCode.get("eventCode"));
        genericDocEventDto.setAction(eventCode.get("action"));
        genericDocEventDto.setDescription(eventCode.get("desc"));
        genericDocEventDto.setTxnType(eventCode.get("txnType"));
        genericDocEventDto.setLinkGuid(genericDocEventDto1.getGuid());
        return genericDocEventDto;


    }

}
