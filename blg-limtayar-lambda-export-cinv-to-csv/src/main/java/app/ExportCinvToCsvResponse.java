package app;

public class ExportCinvToCsvResponse {

    private String hello;

    public ExportCinvToCsvResponse(String hello) {
        this.hello = hello;
    }

    public ExportCinvToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
