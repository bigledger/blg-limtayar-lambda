package app;
public class ExportCinvToCsvRequest {

    private String input;

    public ExportCinvToCsvRequest(String input) {
        this.input = input;
    }

    public ExportCinvToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
