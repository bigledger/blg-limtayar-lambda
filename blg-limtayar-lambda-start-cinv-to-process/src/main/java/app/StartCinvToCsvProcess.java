package app;


import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.ExecutionDoesNotExistException;
import com.amazonaws.services.stepfunctions.model.InvalidArnException;
import com.amazonaws.services.stepfunctions.model.StartExecutionRequest;
import com.amazonaws.services.stepfunctions.model.StartExecutionResult;


import com.bigledger.core1.adapter.integration.services.ILoginService;
import com.bigledger.core1.adapter.integration.services.LoginServiceImpl;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.id.LoginResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.util.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.concurrent.TimeUnit;

public class StartCinvToCsvProcess implements RequestHandler<Object, Map<String, String>> {


    @Override
    public Map<String, String> handleRequest(Object eventObject, Context context) {



        final String CONTENT_TYPE = "application/json";
        final long start = Calendar.getInstance().getTimeInMillis();

        LambdaLogger logger = context.getLogger();
        // Write log to CloudWatch using LambdaLogger.
        logger.log("StartCinvExport.handleRequest");

        // final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
        Map<String, String> pMap = new HashMap<String, String>();
        Map <String, String> outputMap = new HashMap <String, String>();



        String jsonOutString = "";


        String tenantCode = System.getenv("tenantCode");
        String credentialA = System.getenv("credentialA");
        String credentialB = System.getenv("credentialB");
//        String supplierCode = System.getenv("supplierCode");
        String limitExport = System.getenv("limitExport");
        String docType = System.getenv("docType");
        String supplierOrgID = System.getenv("supplierOrgID");
        String retailer = System.getenv("retailer");
        String s3Bucket = System.getenv("s3Bucket");
        String s3BucketTempJson = System.getenv("s3BucketTempJson");
        String s3BucketProcess = System.getenv("s3BucketProcess");

        if (docType==null){docType="CKL";}
        if (supplierOrgID==null){supplierOrgID="CustomerInvoice";}
//        if (retailer==null){retailer="ASCENTIS";}
        if (limitExport==null){limitExport="1";}
        if (s3Bucket==null){s3Bucket="limtayar-integration-folder-incoming";}
        if (s3BucketProcess==null){s3BucketProcess="limtayar-integration-folder-processing";}
        if (s3BucketTempJson==null){s3BucketTempJson="limtayar-integration-folder-temp-json";}

//        String[] vendorCodes=null;
//        String cvsSplitBy = ",";
//
//        if (supplierCode!=null){vendorCodes = supplierCode.split(cvsSplitBy);}


        pMap.put("s3BucketProcess", s3BucketProcess);
        pMap.put("s3Bucket", s3Bucket);
        pMap.put("s3BucketTempJson", s3BucketTempJson);
        pMap.put("limitExport", limitExport);
        pMap.put("docType", docType);
//        pMap.put("supplierOrgID", supplierOrgID);
        pMap.put("retailer", retailer);
        pMap.put("tenantCode", tenantCode);

        // connect to API
        System.out.println("Connecting to API....");

        ILoginService iLoginService = new LoginServiceImpl();
        ApiResponse<LoginResponse> apiResponse = null;

        try {


            apiResponse = iLoginService.login(credentialA, credentialB);

            LoginResponse loginResponse = apiResponse.getData();

            String token = loginResponse.getAuthToken();

            String appId = "limtayar-lambda";

            pMap.put("appId", appId);
            pMap.put("token", token);



        // export PO begins here

//        for (int z=0; z < vendorCodes.length; z++)
        {



            // Create CSV file name
            java.text.DecimalFormat nft = new java.text.DecimalFormat("#00.###");
            nft.setDecimalSeparatorAlwaysShown(false);
            LocalDateTime now = LocalDateTime.now();


            String yyyy = Integer.toString(now.getYear());
            String mm = nft.format(now.getMonthValue());
            String dd = nft.format(now.getDayOfMonth());
            String hh = nft.format(now.getHour());
            String min = nft.format(now.getMinute());
            String sec = nft.format(now.getSecond());

//            String uniqueRunningNumber = '0'+ dd + hh + min + sec ;

            String csvFileName = "CKL_INV_LISTING/"+ docType + "_" + supplierOrgID + "_" + yyyy + mm + dd + hh + min + ".csv";

//            logger.log("Supplier Code:" + vendorCodes[z]);

            pMap.put("s3Key", csvFileName);
//            pMap.put("vendorCode", vendorCodes[z]);

            AWSLambda awsLambda = AWSLambdaClientBuilder.standard()
                    .withRegion(Regions.AP_SOUTHEAST_1).build();

            logger.log("pMap :" + pMap);

            try
            {
                ObjectMapper outputMapMapper = new ObjectMapper();
                outputMapMapper.registerModule(new JavaTimeModule());
                outputMapMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
                jsonOutString = outputMapMapper.writeValueAsString(pMap);
                System.out.println(jsonOutString);
            }
            catch (JsonProcessingException e) {
                e.printStackTrace();
                logger.log(e.getMessage());
            }
            finally
            {
                logger.log("Print of JSON going to Output");

            }


            try {
                final AWSStepFunctions stepFunctionsClient = AWSStepFunctionsClientBuilder.standard().withRegion(Regions.AP_SOUTHEAST_1).build();
                final StartExecutionResult result = stepFunctionsClient.startExecution(new StartExecutionRequest()
                        .withStateMachineArn("arn:aws:states:ap-southeast-1:839936404815:stateMachine:limtayar-ascentis-blg-process-sales-invoice").withInput(jsonOutString));
                logger.log("Execute step function - state machine : " +result.getExecutionArn());
            }
            catch (ExecutionDoesNotExistException e) {
                e.printStackTrace();
                logger.log(e.getMessage());
            } catch (InvalidArnException e) {
                e.printStackTrace();
                logger.log(e.getMessage());
            }
            finally
            {
                logger.log("Execution of step function - state machine");
            }

            // wait 1 second
            TimeUnit.SECONDS.sleep(1);


        }
            logger.log("end for loop");

        } catch(Exception ex){
        System.out.println(ex);
    }

        final long end = Calendar.getInstance().getTimeInMillis();
        System.out.println(" export cinv time spent: " + (end - start) / 1000 + "s");



        return outputMap;
    }






}
