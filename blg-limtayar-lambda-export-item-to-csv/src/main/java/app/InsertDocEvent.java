package app;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.bigledger.core1.adapter.integration.constants.Constants;
import com.bigledger.core1.adapter.integration.services.ISubqueryService;
import com.bigledger.core1.adapter.integration.services.SubqueryServiceImpl;
import com.bigledger.core2.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.common.exception.BlgValidationException;
import com.bigledger.core1.dto.AppMessageEventDto;
import com.bigledger.core1.util.StringUtil;
import com.bigledger.core2.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core2.adapter.integration.exception.NetworkException;
import com.bigledger.core2.adapter.integration.services.IEntityEventService;
import com.bigledger.core2.adapter.integration.services.IItemEventService;
import com.bigledger.core2.adapter.integration.services.ItemEventServiceImpl;
import com.bigledger.core2.dal.model.FinancialItemContainers.FinancialItemEventContainer;
import com.bigledger.core2.dal.table.bl_fi_mst_item_event;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InsertDocEvent {

    public boolean insertEvent(String guid, Map<String, String> eventCode, RequestHeaders requestHeaders, com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1, LambdaLogger logger) throws InterruptedException, NetworkException, AkaunApiServerException {


        FinancialItemEventContainer exportMessageEventSynced = funConvertToGenDocEvent(guid, eventCode, requestHeaders,requestHeaders1);

        IItemEventService itemEventService = new ItemEventServiceImpl();

        try {
//            responseEntityEvent = createEntityEvents(exportMessageEventSynced, requestHeaders);
            itemEventService.createFinancialItemEvents(exportMessageEventSynced, requestHeaders);
        } catch (AkaunApiServerException e) {
            System.out.println(e.getMessage());
            return false;
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }


       return true;



//        AppMessageEventDto appMessageEventDto = funConvertToGenDocEvent(guid, eventCode, requestHeaders);



//                IDocumentEventService docEventService = new DocumentEventServiceImpl();
//
//                try {
//                    docEventService.createDocumentEvent(exportMessageEventSynced, requestHeaders);
//                }
//                catch (AkaunApiServerException e){
//                    System.out.println(e.getMessage());
//                    return false;
//                }
//                catch (NetworkException e){
//                    System.out.println(e.getMessage());
//                    return false;
//                }
//                catch (Exception e){
//                    System.out.println(e.getMessage());
//                    return false;
//                }
//
//        ApiResponse apiResponse= new ApiResponse();
//        try {
//
//            RestTemplate restTemplate = new RestTemplate();
//            String REST_SERVICE_URL = Constants.BASE_URL_PROD+"app-message-events";
//            HttpHeaders httpHeader = new HttpHeaders();
//            httpHeader.setContentType(MediaType.APPLICATION_JSON);
//            httpHeader.add("Authorization", requestHeaders.getToken());
//            httpHeader.add("tenant-code", requestHeaders.getTenantCode());
//            httpHeader.add("appId", requestHeaders.getAppId());
//
//            HttpEntity httpEntity = new HttpEntity(appMessageEventDto, httpHeader);
//
//            ResponseEntity<ApiResponse> response = restTemplate.exchange(REST_SERVICE_URL, HttpMethod.POST, httpEntity,ApiResponse.class);
//            apiResponse = response.getBody();
//
//        }
//        catch (BlgValidationException blg){
//            System.out.print("----------------------VALIDATION ERROR --------------------------");
//            System.out.println(blg.getErrorList());
//        } catch (Exception e){
//                    System.out.println(e.getMessage());
//                    return false;
//        }
    }

//    private static AppMessageEventDto funConvertToGenDocEvent(String itemGuid,  Map<String, String> eventCode, RequestHeaders requestHeaders){
//
//
//    String linkGuid = funGetLinkGuid(itemGuid, requestHeaders);
//
//        AppMessageEventDto appMessageEventDto = new AppMessageEventDto();
//
//        //genericDocEventDto.setGuid(StringUtil.getGuid());
//        appMessageEventDto.setRemoteDocId(itemGuid);
//        appMessageEventDto.setLocalUpdatedTime(Timestamp.from(Instant.now()));
//        appMessageEventDto.setLocalCreatedTime(Timestamp.from(Instant.now()));
//        appMessageEventDto.setEventCode(eventCode.get("eventCode"));
//        appMessageEventDto.setDescr(eventCode.get("desc"));
//        appMessageEventDto.setTxnType(eventCode.get("txnType"));
//
//        if (!StringUtil.isNullOrEmpty(linkGuid)) {
//            appMessageEventDto.setLinkGuid(linkGuid);
//        }
//
//
//
//        return appMessageEventDto ;
//
//
//
//
//    }
//
//
//    private static String funGetLinkGuid(String itemGuid,  RequestHeaders requestHeaders){
//        List<String> guids = new ArrayList<>();
//        Map<String, String> subquery = new HashMap<String, String>();
//        String itemSql = "SELECT e1.guid AS neededguid FROM app_message_event AS e1 " +
//                " WHERE e1.event_code = 'PROCESSED_ITEM_CREATED'" +
//                " and e1.remote_doc_id = '" + itemGuid + "' " +
//                ";";
//
//        subquery.put("subquery", itemSql);
//
//        System.out.println("Querying database for item listing");
//        System.out.println(itemSql);
//
//
//        try {
//
//
//            //  get all accepted and new panasonic purchase orders
//            ISubqueryService subqueryService = new SubqueryServiceImpl();
//
//            ApiResponse responseSubquery = subqueryService.getGuidsBySubquery(subquery, requestHeaders);
//
//            JSONParser parser = new JSONParser();
//            JSONObject json = (JSONObject) parser.parse(responseSubquery.asJson());
//            System.out.println("GUIDs: " + json.get("data").toString());
//
//
//            ObjectMapper mapper = new ObjectMapper();
//            mapper.registerModule(new JavaTimeModule());
//            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//            guids = mapper.readValue(json.get("data").toString(), ArrayList.class);
//
//
//
//        }catch (Exception e){}
//
//
//        if (guids.size()>0)
//        {
//            return guids.get(0);
//        } else{
//            return null;
//        }
//
//   }

    private static String funGetLinkGuid(String itemGuid, com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1) {
        List<String> guids = new ArrayList<>();
        Map<String, String> subquery = new HashMap<String, String>();
        String custSql = "SELECT e1.guid AS neededguid FROM bl_fi_mst_item_event AS e1 " +
                " WHERE e1.event_code = 'ITEM_CREATED'" +
                " and e1.hdr_guid = '" + itemGuid + "' " +
                ";";

        subquery.put("subquery", custSql);

        System.out.println("Querying database for customer listing");
        System.out.println(custSql);


        try {


            ISubqueryService subqueryService = new SubqueryServiceImpl();

            ApiResponse responseSubquery = subqueryService.getGuidsBySubquery(subquery, requestHeaders1);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseSubquery.asJson());
            System.out.println("GUIDs: " + json.get("data").toString());


            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            guids = mapper.readValue(json.get("data").toString(), ArrayList.class);


        } catch (Exception e) {
        }


        if (guids.size() > 0)
        {
            return guids.get(0);

        } else {
            return null;
        }
    }

    //protected
    private FinancialItemEventContainer funConvertToGenDocEvent(String itemGuid, Map<String, String> eventCode, RequestHeaders requestHeaders, com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1) throws NetworkException, AkaunApiServerException, InterruptedException, com.bigledger.core2.adapter.integration.exception.AkaunApiServerException, com.bigledger.core2.adapter.integration.exception.NetworkException {


        String linkGuid = funGetLinkGuid(itemGuid, requestHeaders1);

        //custGuid
        FinancialItemEventContainer financialItemEventContainer = new FinancialItemEventContainer();

//        EntityEventQueryCriteria entityEventQueryCriteria = new EntityEventQueryCriteria();
//        entityEventQueryCriteria.setHdr_guid(custGuid);
//        System.out.println("custGuid" + custGuid);
//        entityEventQueryCriteria.setEvent_code("CUSTOMER_CREATED");

//        IEntityEventService custeventService = new EntityEventServiceImlp();

        //     com.bigledger.core2.common.api.ApiResponse<List<EntityEventContainers>> CustEventResponse = custeventService.getEntityEventByCriteriaProcess(entityEventQueryCriteria, requestHeaders);

//        com.bigledger.core2.common.api.ApiResponse getEntityEventByGuid = custeventService.getEntityEventByGuid(linkGuid, requestHeaders);
        com.bigledger.core2.common.api.ApiResponse<List<FinancialItemEventContainer>> response = new com.bigledger.core2.common.api.ApiResponse<>();
////                FinancialFilingContainer filingContainer1 = new FinancialFilingContainer();
////                List<FinancialFilingContainer> FinancialfilingContainer = new ArrayList<>();
////
        response = getFinancialItemEventsByGuid(linkGuid, requestHeaders);

        System.out.println("response data" + response.getData());

        ObjectMapper itemResponseMapper = new ObjectMapper();
        itemResponseMapper.registerModule(new JavaTimeModule());
        itemResponseMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        FinancialItemEventContainer financialItemContainer = itemResponseMapper.convertValue(response.getData(), FinancialItemEventContainer.class);

        System.out.println("Data guid1 is set");

        bl_fi_mst_item_event event = new bl_fi_mst_item_event();
        financialItemEventContainer.setBl_fi_mst_item_event(event);
        event.setHdr_guid(financialItemContainer.getBl_fi_mst_item_event().getHdr_guid());
        event.setTxn_type(eventCode.get("txnType"));
        event.setAction(eventCode.get("action"));
        event.setDate_txn(ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur")));
        event.setEvent_code(eventCode.get("eventCode"));
        event.setStatus("ACTIVE");
        event.setLink_guid(financialItemContainer.getBl_fi_mst_item_event().getGuid());

        return financialItemEventContainer;

    }

    protected com.bigledger.core2.common.api.ApiResponse getFinancialItemEventsByGuid(String linkGuid, com.bigledger.core2.adapter.integration.utils.RequestHeaders requestHeaders) {
        com.bigledger.core2.common.api.ApiResponse apiResponse = new com.bigledger.core2.common.api.ApiResponse<>();


        try {


            RestTemplate restTemplate = new RestTemplate();
            String REST_SERVICE_URL = "https://api.akaun.com/core2/ms/item-events/"+linkGuid;
            HttpHeaders httpHeader = new HttpHeaders();
            httpHeader.setContentType(MediaType.APPLICATION_JSON);
            httpHeader.add("Authorization", requestHeaders.getToken());
            httpHeader.add("tenantCode", requestHeaders.getTenantCode());
            httpHeader.add("appId", requestHeaders.getAppId());

            HttpEntity httpEntity = new HttpEntity(linkGuid, httpHeader);
            ParameterizedTypeReference<com.bigledger.core2.common.api.ApiResponse> parameterizedTypeReference = new ParameterizedTypeReference<com.bigledger.core2.common.api.ApiResponse>() {};

            ResponseEntity<com.bigledger.core2.common.api.ApiResponse> response = restTemplate.exchange(REST_SERVICE_URL, org.springframework.http.HttpMethod.GET, httpEntity, parameterizedTypeReference);
            apiResponse = response.getBody();

//            ResponseEntity<ApiResponse> response = restTemplate.exchange(REST_SERVICE_URL, HttpMethod.GET, httpEntity,ApiResponse.class);
//            apiResponse = response.getBody();

        }
        catch (BlgValidationException ex){
            System.out.println("--------------- BLG VALIDATION ERROR -------");
            System.out.println(ex.getErrorList());
        }
//        catch (ResourceAccessException e){
//            e.printStackTrace();
//            throw new NetworkException(apiResponse);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new AkaunApiServerException(apiResponse);
//        }
        return apiResponse;
    }

}
