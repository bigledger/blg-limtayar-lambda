package app;

public class ExportItemToCsvResponse {

    private String hello;

    public ExportItemToCsvResponse(String hello) {
        this.hello = hello;
    }

    public ExportItemToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
