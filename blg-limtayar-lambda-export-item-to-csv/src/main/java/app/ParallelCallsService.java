package app;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.bigledger.core2.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core2.adapter.integration.exception.NetworkException;
import com.bigledger.core2.adapter.integration.utils.RequestHeaders;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ParallelCallsService {


    InsertDocEvent insertDocEvent = new InsertDocEvent();



        public List<Boolean> getToDos(List<String> ids, RequestHeaders requestHeaders,com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1, Map<String, String> eventCode, LambdaLogger logger){

            List<CompletableFuture<Boolean>> futures =
                    ids.stream()
                            .map(id -> getToDoAsync(id, requestHeaders,requestHeaders1,  eventCode, logger))
                            .collect(Collectors.toList());

            List<Boolean> result =
                    futures.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList());

            return result;
        }

        CompletableFuture<Boolean> getToDoAsync(String id, RequestHeaders requestHeaders,com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1,Map<String, String> eventCode, LambdaLogger logger){

            CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(new Supplier<Boolean>() {
                @Override
                public Boolean get() {

                    boolean response = false;


                    try {
                        response = insertDocEvent.insertEvent(id,eventCode, requestHeaders,requestHeaders1,logger);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (com.bigledger.core2.adapter.integration.exception.NetworkException e) {
                        e.printStackTrace();
                    } catch (com.bigledger.core2.adapter.integration.exception.AkaunApiServerException e) {
                        e.printStackTrace();
                    }

                    return response;


                }
            });

            return future;
        }


}
