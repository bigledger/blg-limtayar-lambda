package app;



import app.constants.EventQueueConstants;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

//import com.google.gson.Gson;



public class ExportItemToCsv implements RequestHandler<Map<String, String>, Map<String, String>> {


    @Override
    public Map<String, String> handleRequest(Map<String, String> pMap, Context context) {

        final long start = Calendar.getInstance().getTimeInMillis();

        final String CONTENT_TYPE = "application/json";

        LambdaLogger logger = context.getLogger();
        // Write log to CloudWatch using LambdaLogger.
        logger.log("Entered ExportItemToCsv.handleRequest");
        //AmazonS3Client s3Client = new AmazonS3Client(new DefaultAWSCredentialsProviderChain());

        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();



        System.out.println("Input Map: " + pMap);
        System.out.println("Export CSV Filename: " + pMap.get("s3Key"));
        //System.out.println("Vendor Code: " + pMap.get("vendorCode"));
        System.out.println("Processing Bucket: " + pMap.get("s3BucketProcess"));
        System.out.println("Temp Json Bucket: " + pMap.get("s3BucketTempJson"));
        System.out.println("Guids Filename: " + pMap.get("s3KeyGuids"));

        Map<String, String> eventCode = new HashMap<String, String>();


        // initial variables for auto export item

        eventCode.put("eventCode", EventQueueConstants.MESSAGE_EXPORT_EVENT_CODE);
        eventCode.put("action", EventQueueConstants.MESSAGE_EXPORT_ACTION);
        eventCode.put("desc", EventQueueConstants.MESSAGE_EXPORT_DESC);
        eventCode.put("txnType", EventQueueConstants.TXN_TYPE);

        RequestHeaders requestHeaders = new RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));
        com.bigledger.core2.adapter.integration.utils.RequestHeaders requestHeaders1 = new com.bigledger.core2.adapter.integration.utils.RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));


        // Read Items guids from S3
        List<String> guids=getItemGuids(s3Client,pMap,logger);

        logger.log("Guids: " + guids);

        // Use parallel call to update document status

        ParallelCallsService parallelCallItemDto = new ParallelCallsService();

    if(guids!=null) {
        List<Boolean> responses = parallelCallItemDto.getToDos(guids,requestHeaders1, requestHeaders, eventCode, logger);


        if (responses.contains(false)){

            logger.log("Error inserting document event");

        }
        else{

            // Move CSV file to main bucket if successful
            String status = moveFromIncomingToProcessing(s3Client, pMap.get("s3BucketProcess"), pMap.get("s3Key"),  pMap.get("s3Bucket"),  pMap.get("s3Key"),  logger);

            if (status=="succeeded"){
                logger.log("CSV files moved successfully!!!");
            }
            else {
                logger.log("Errror moving CSV file");
            }
        }
    }


        final long end = Calendar.getInstance().getTimeInMillis();
        logger.log(" export item time spent: " + (end - start) / 1000 + "s");

        return pMap;
    }


    protected List<String> getItemGuids (AmazonS3 s3Client,Map<String, String> pMap, LambdaLogger logger)
    {

        List<String> records = new ArrayList<String>();

        // initialize the input stream and buffer reader
        InputStream inputStream = null;
        BufferedReader br = null;

        if (pMap.get("s3KeyGuids")!=null)
        {
            String s3KeyGuids = pMap.get("s3KeyGuids");
            String s3BucketTempJson = pMap.get("s3BucketTempJson");
            logger.log("found id: " + s3BucketTempJson + " " + s3KeyGuids);
            // retrieve s3 object
            S3Object object = s3Client.getObject(new GetObjectRequest(s3BucketTempJson, s3KeyGuids));


            try {

                inputStream = object.getObjectContent();

                br = new BufferedReader(new InputStreamReader(inputStream));


                List<String> recordsChunk = new ArrayList<String>();


                int lines = 1;     // initialize the required number of lines
                int currentLine = 0; // initiate the current read line of the string buffer
                int fileNumber = 0; // initialize the file number to save the splitted files


                String line = "";

                // read all the lines from the input buffer stream
                while ((line = br.readLine()) != null) {
                    records.add(line);
                }


            } //end for (split file)
            catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else{
            records=null;
        }


        return records;
    }

    protected String moveFromIncomingToProcessing (AmazonS3 s3Client, String bucketFrom, String s3KeyFrom, String bucketTo, String s3KeyTo,  LambdaLogger logger)
    {
        String status = "succeeded";
        boolean copySucceeded = false;
        boolean deleteSucceeded = false;


        try {
            logger.log("moveFromIncomingToProcessing : bucketFrom :" + bucketFrom);
            logger.log("moveFromIncomingToProcessing : bucketTo :" + bucketTo);
            logger.log("moveFromIncomingToProcessing : s3KeyFrom :" + s3KeyFrom);
            logger.log("moveFromIncomingToProcessing : s3KeyTo :" + s3KeyTo);
            CopyObjectResult copyObjectResult = s3Client.copyObject(bucketFrom, s3KeyFrom, bucketTo, s3KeyTo);
            copySucceeded = true;

        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            status = "failed";
            //System.exit(1);
        }
        finally
        {

        }

        if (copySucceeded == true) {
            try {
                s3Client.deleteObject(bucketFrom, s3KeyFrom);
                deleteSucceeded = true;
            } catch (AmazonServiceException e) {
                System.err.println(e.getErrorMessage());
                status = "failed";
                //System.exit(1);
            } finally {

            }
        }

        return status;
    }

}
