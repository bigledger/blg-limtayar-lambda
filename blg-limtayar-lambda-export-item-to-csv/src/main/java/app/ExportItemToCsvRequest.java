package app;
public class ExportItemToCsvRequest {

    private String input;

    public ExportItemToCsvRequest(String input) {
        this.input = input;
    }

    public ExportItemToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
