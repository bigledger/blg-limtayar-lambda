package app;

public class ExportCustToCsvResponse {

    private String hello;

    public ExportCustToCsvResponse(String hello) {
        this.hello = hello;
    }

    public ExportCustToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
