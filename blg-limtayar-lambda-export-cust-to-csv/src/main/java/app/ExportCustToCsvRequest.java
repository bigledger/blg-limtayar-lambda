package app;
public class ExportCustToCsvRequest {

    private String input;

    public ExportCustToCsvRequest(String input) {
        this.input = input;
    }

    public ExportCustToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
