package app;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.bigledger.core1.adapter.integration.services.ISubqueryService;
import com.bigledger.core1.adapter.integration.services.SubqueryServiceImpl;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.AppMessageEventDto;
import com.bigledger.core2.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core2.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.DocumentEventServiceImpl;
import com.bigledger.core1.adapter.integration.services.IDocumentEventService;
import com.bigledger.core2.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.dto.GenericDocEventDto;
import com.bigledger.core1.dto.id.PagingResponse;
import com.bigledger.core2.adapter.integration.services.EntityEventServiceImlp;
import com.bigledger.core2.adapter.integration.services.IEntityEventService;
import com.bigledger.core2.common.exception.BlgValidationException;
import com.bigledger.core2.dal.criteria.EntityEventQueryCriteria;
import com.bigledger.core2.dal.model.CustomerContainers.CustomerContainer;
import com.bigledger.core2.dal.model.EntityContainers.EntityHeaderContainer;
import com.bigledger.core2.dal.model.EntityEventContainers;
import com.bigledger.core2.dal.model.FinancialFilingContainers.FinancialFilingContainer;
import com.bigledger.core2.dal.table.bl_fi_mst_entity_event;
import com.bigledger.core2.utils.StringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

public class InsertDocEvent {

    public boolean insertEvent(String guid, Map<String, String> eventCode, RequestHeaders requestHeaders, com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1, LambdaLogger logger) throws NetworkException, AkaunApiServerException, InterruptedException {


//        com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1 = new  com.bigledger.core2.adapter.integration.utils.RequestHeaders;
//        ApiResponse appMessageEvent = funConvertToGenDocEvent(guid, eventCode, requestHeaders);
        EntityEventContainers exportMessageEventSynced = funConvertToGenDocEvent(guid, eventCode, requestHeaders,requestHeaders1);

        IEntityEventService entitydocEventService = new EntityEventServiceImlp();

        try {
//            responseEntityEvent = createEntityEvents(exportMessageEventSynced, requestHeaders);
            entitydocEventService.createEntityEvents(exportMessageEventSynced, requestHeaders);
        } catch (AkaunApiServerException e) {
            System.out.println(e.getMessage());
            return false;
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

//        ApiResponse apiResponse= new ApiResponse();
//        try {
//
//            RestTemplate restTemplate = new RestTemplate();
//            String REST_SERVICE_URL = Constants.BASE_URL_PROD+"app-message-events";
//            HttpHeaders httpHeader = new HttpHeaders();
//            httpHeader.setContentType(MediaType.APPLICATION_JSON);
//            httpHeader.add("Authorization", requestHeaders.getToken());
//            httpHeader.add("tenant-code", requestHeaders.getTenantCode());
//            httpHeader.add("appId", requestHeaders.getAppId());
//
//            HttpEntity httpEntity = new HttpEntity(appMessageEventDto, httpHeader);
//
//            ResponseEntity<ApiResponse> response = restTemplate.exchange(REST_SERVICE_URL, HttpMethod.POST, httpEntity,ApiResponse.class);
//            apiResponse = response.getBody();
//
//        }
//        catch (BlgValidationException blg){
//            System.out.print("----------------------VALIDATION ERROR --------------------------");
//            System.out.println(blg.getErrorList());
//        } catch (Exception e){
//            System.out.println(e.getMessage());
//            return false;
//        }

        return true;
    }

//    private static  funConvertToGenDocEvent(String custGuid,  Map<String, String> eventCode, com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1){
//
//        String linkGuid = funGetLinkGuid(custGuid, requestHeaders1);
//
//        EntityEventContainers entityEventContainers = new EntityEventContainers();
//
//        //genericDocEventDto.setGuid(StringUtil.getGuid());
////        entityEventContainers.setRemoteDocId(custGuid);
////        entityEventContainers.setLocalUpdatedTime(Timestamp.from(Instant.now()));
////        entityEventContainers.setLocalCreatedTime(Timestamp.from(Instant.now()));
////        entityEventContainers.setEventCode(eventCode.get("eventCode"));
////        entityEventContainers.setDescr(eventCode.get("desc"));
////        entityEventContainers.setTxnType(eventCode.get("txnType"));
//
//        entityEventContainers.getBl_fi_mst_entity_event().setHdr_guid(custGuid);
//        entityEventContainers.getBl_fi_mst_entity_event().setDate_txn(ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur")));
//        entityEventContainers.getBl_fi_mst_entity_event().setEvent_code(eventCode.get("eventCode"));
//        entityEventContainers.getBl_fi_mst_entity_event().setAction(eventCode.get("action"));
//        entityEventContainers.getBl_fi_mst_entity_event().setDescr(eventCode.get("desc"));
//        entityEventContainers.getBl_fi_mst_entity_event().setTxn_type(eventCode.get("txnType"));
//        entityEventContainers.getBl_fi_mst_entity_event().setStatus("ACTIVE");
////        entityEventContainers.getBl_fi_mst_entity_event().setRevision(UUID.randomUUID().toString().toUpperCase());
//
//        if (!StringUtil.isNullOrEmpty(linkGuid)) {
//            entityEventContainers.getBl_fi_mst_entity_event().setLink_guid(linkGuid);
//        }
//
//
//        return ;
//
////        GenericDocEventDto genericDocEventDto = new GenericDocEventDto();
////
////        DocEventCriteria DocEventCriteria = new DocEventCriteria();
////        DocEventCriteria.setGuidDochdr(docHdrGuid);
////        DocEventCriteria.setAction("SALES_INVOICE_CREATED");
////        IDocumentEventService CinveventService = new DocumentEventServiceImpl();
//////            PagingResponse SOEventResponse = new PagingResponse();
////        PagingResponse<GenericDocEventDto> SOEventResponse = CinveventService.getGenericDocEventDtoByCriteriaProcess(DocEventCriteria, requestHeaders);
////
////        List<GenericDocEventDto> genericDocEventDtosList = SOEventResponse.getObjectList();
////
////        GenericDocEventDto genericDocEventDto1 = genericDocEventDtosList.get(0);
////
////        genericDocEventDto.setGuidDocHdr(docHdrGuid);
////        genericDocEventDto.setDateTxn(ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur")));
////        genericDocEventDto.setCreatedDate(ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur")));
////        genericDocEventDto.setEventCode(eventCode.get("eventCode"));
////        genericDocEventDto.setAction(eventCode.get("action"));
////        genericDocEventDto.setDescription(eventCode.get("desc"));
////        genericDocEventDto.setTxnType(eventCode.get("txnType"));
////        genericDocEventDto.setLinkGuid(genericDocEventDto1.getGuid());
////        return genericDocEventDto;
//
//
//    }
//
//
//
//
//
//
//
    private static String funGetLinkGuid(String custGuid, com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1) {
        List<String> guids = new ArrayList<>();
        Map<String, String> subquery = new HashMap<String, String>();
        String custSql = "SELECT e1.guid AS neededguid FROM bl_fi_mst_entity_event AS e1 " +
                " WHERE e1.event_code = 'CUSTOMER_CREATED'" +
                " and e1.hdr_guid = '" + custGuid + "' " +
                ";";

                subquery.put("subquery", custSql);

        System.out.println("Querying database for customer listing");
        System.out.println(custSql);


        try {


            ISubqueryService subqueryService = new SubqueryServiceImpl();

            ApiResponse responseSubquery = subqueryService.getGuidsBySubquery(subquery, requestHeaders1);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseSubquery.asJson());
            System.out.println("GUIDs: " + json.get("data").toString());


            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            guids = mapper.readValue(json.get("data").toString(), ArrayList.class);


        } catch (Exception e) {
        }


        if (guids.size() > 0)
        {
            return guids.get(0);

        } else {
            return null;
        }
    }

    //protected
    private EntityEventContainers funConvertToGenDocEvent(String custGuid, Map<String, String> eventCode, RequestHeaders requestHeaders, com.bigledger.core1.adapter.integration.utils.RequestHeaders requestHeaders1) throws NetworkException, AkaunApiServerException, InterruptedException, com.bigledger.core2.adapter.integration.exception.AkaunApiServerException, com.bigledger.core2.adapter.integration.exception.NetworkException {


        String linkGuid = funGetLinkGuid(custGuid, requestHeaders1);

        //custGuid
        EntityEventContainers entityEventContainers = new EntityEventContainers();

//        EntityEventQueryCriteria entityEventQueryCriteria = new EntityEventQueryCriteria();
//        entityEventQueryCriteria.setHdr_guid(custGuid);
//        System.out.println("custGuid" + custGuid);
//        entityEventQueryCriteria.setEvent_code("CUSTOMER_CREATED");

//        IEntityEventService custeventService = new EntityEventServiceImlp();

   //     com.bigledger.core2.common.api.ApiResponse<List<EntityEventContainers>> CustEventResponse = custeventService.getEntityEventByCriteriaProcess(entityEventQueryCriteria, requestHeaders);

//        com.bigledger.core2.common.api.ApiResponse getEntityEventByGuid = custeventService.getEntityEventByGuid(linkGuid, requestHeaders);
        com.bigledger.core2.common.api.ApiResponse<List<EntityEventContainers>> response = new com.bigledger.core2.common.api.ApiResponse<>();
////                FinancialFilingContainer filingContainer1 = new FinancialFilingContainer();
////                List<FinancialFilingContainer> FinancialfilingContainer = new ArrayList<>();
////
        response = getEntityEventByGuid(linkGuid, requestHeaders);

                System.out.println("response data" + response.getData());

        ObjectMapper filingResponseMapper = new ObjectMapper();
        filingResponseMapper.registerModule(new JavaTimeModule());
        filingResponseMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        EntityEventContainers filingContainer = filingResponseMapper.convertValue(response.getData(), EntityEventContainers.class);

        System.out.println("Data guid1 is set");

        bl_fi_mst_entity_event event = new bl_fi_mst_entity_event();
        entityEventContainers.setBl_fi_mst_entity_event(event);
        event.setHdr_guid(filingContainer.getBl_fi_mst_entity_event().getHdr_guid());
        event.setTxn_type(eventCode.get("txnType"));
        event.setAction(eventCode.get("action"));
        event.setDate_txn(ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur")));
        event.setEvent_code(eventCode.get("eventCode"));
        event.setStatus("ACTIVE");
        event.setLink_guid(filingContainer.getBl_fi_mst_entity_event().getGuid());

        return entityEventContainers;

    }

    protected com.bigledger.core2.common.api.ApiResponse getEntityEventByGuid(String linkGuid, com.bigledger.core2.adapter.integration.utils.RequestHeaders requestHeaders) {
        com.bigledger.core2.common.api.ApiResponse apiResponse = new com.bigledger.core2.common.api.ApiResponse<>();


        try {


            RestTemplate restTemplate = new RestTemplate();
            String REST_SERVICE_URL = "https://api.akaun.com/core2/ms/entity-events/"+linkGuid;
            HttpHeaders httpHeader = new HttpHeaders();
            httpHeader.setContentType(MediaType.APPLICATION_JSON);
            httpHeader.add("Authorization", requestHeaders.getToken());
            httpHeader.add("tenantCode", requestHeaders.getTenantCode());
            httpHeader.add("appId", requestHeaders.getAppId());

            HttpEntity httpEntity = new HttpEntity(linkGuid, httpHeader);
            ParameterizedTypeReference<com.bigledger.core2.common.api.ApiResponse> parameterizedTypeReference = new ParameterizedTypeReference<com.bigledger.core2.common.api.ApiResponse>() {};

            ResponseEntity<com.bigledger.core2.common.api.ApiResponse> response = restTemplate.exchange(REST_SERVICE_URL, org.springframework.http.HttpMethod.GET, httpEntity, parameterizedTypeReference);
            apiResponse = response.getBody();

//            ResponseEntity<ApiResponse> response = restTemplate.exchange(REST_SERVICE_URL, HttpMethod.GET, httpEntity,ApiResponse.class);
//            apiResponse = response.getBody();

        }
        catch (BlgValidationException ex){
            System.out.println("--------------- BLG VALIDATION ERROR -------");
            System.out.println(ex.getErrorList());
        }
//        catch (ResourceAccessException e){
//            e.printStackTrace();
//            throw new NetworkException(apiResponse);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new AkaunApiServerException(apiResponse);
//        }
        return apiResponse;
    }

//    protected com.bigledger.core2.common.api.ApiResponse createEntityEventTable(CustomerContainer customerContainer) throws InterruptedException, AkaunApiServerException, NetworkException {
//        System.out.println("createEntityEvent");
//
//        com.bigledger.core2.common.api.ApiResponse apiResponse = new com.bigledger.core2.common.api.ApiResponse();
//
//        EntityEventContainers entityEventContainers = new EntityEventContainers();
//        bl_fi_mst_entity_event event = new bl_fi_mst_entity_event();
//        entityEventContainers.setBl_fi_mst_entity_event(event);
//        event.setHdr_guid(customerContainer.getBl_fi_mst_entity_hdr().getGuid());
//        event.setTxn_type("SYS_APPLET");
//        event.setAction("CUSTOMER_CREATED");
//        ZonedDateTime zdtCurrent = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("Asia/Kuala_Lumpur"));
//        event.setLocal_created_time(zdtCurrent);
//        event.setEvent_code("CUSTOMER_CREATED");
//        event.setStatus("ACTIVE");
//
//        //IEntityEventService iEntityEventService = new EntityEventServiceImlp();
//        apiResponse = createEntityEvents(entityEventContainers, requestHeaders);
//
//        return apiResponse;
//    }
//
//
//    protected com.bigledger.core2.common.api.ApiResponse createEntityEvents(EntityEventContainers entityEventContainers , RequestHeaders requestHeaders) throws AkaunApiServerException, NetworkException {
//        com.bigledger.core2.common.api.ApiResponse<EntityEventContainers> apiResponse= new com.bigledger.core2.common.api.ApiResponse<>();
//
//        try {
//
//            RestTemplate restTemplate = new RestTemplate();
//            String REST_SERVICE_URL = "https://api.akaun.com/core2/ms/entity-events";
//            HttpHeaders httpHeader = new HttpHeaders();
//            httpHeader.setContentType(MediaType.APPLICATION_JSON);
//            httpHeader.add("Authorization", requestHeaders.getToken());
//            httpHeader.add("tenantCode", requestHeaders.getTenantCode());
//            httpHeader.add("appId", requestHeaders.getAppId());
//
//
//            HttpEntity httpEntity = new HttpEntity(entityEventContainers, httpHeader);
//            ParameterizedTypeReference<ApiResponse<EntityEventContainers>> parameterizedTypeReference = new ParameterizedTypeReference<com.bigledger.core2.common.api.ApiResponse<EntityEventContainers>>() {};
//
//            ResponseEntity<com.bigledger.core2.common.api.ApiResponse<EntityEventContainers>> response = restTemplate.exchange(REST_SERVICE_URL, HttpMethod.POST, httpEntity, parameterizedTypeReference);
//            apiResponse = response.getBody();
//
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        return apiResponse;
//
//    }


}