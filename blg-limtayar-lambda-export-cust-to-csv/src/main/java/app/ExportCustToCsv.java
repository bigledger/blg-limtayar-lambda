package app;



import app.constants.EventQueueConstants;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.*;

import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification;


import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.*;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.ILoginService;
import com.bigledger.core1.adapter.integration.services.IPurchaseOrderService;
import com.bigledger.core1.adapter.integration.services.LoginServiceImpl;
import com.bigledger.core1.adapter.integration.services.PurchaseOrderServiceImpl;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.adapter.integration.wrapper.JsonWrapper;
import com.bigledger.core1.adapter.integration.wrapper.validationObjects.*;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.CustomFieldDto;
import com.bigledger.core1.dto.GenericDocEventDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.id.LoginResponse;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;
//import com.google.gson.Gson;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.JsonbException;



public class ExportCustToCsv implements RequestHandler<Map<String, String>, Map<String, String>> {


    @Override
    public Map<String, String> handleRequest(Map<String, String> pMap, Context context) {

        final long start = Calendar.getInstance().getTimeInMillis();

        final String CONTENT_TYPE = "application/json";

        LambdaLogger logger = context.getLogger();
        // Write log to CloudWatch using LambdaLogger.
        logger.log("Entered ExportCustToCsv.handleRequest");
        //AmazonS3Client s3Client = new AmazonS3Client(new DefaultAWSCredentialsProviderChain());

        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();



        System.out.println("Input Map: " + pMap);
        System.out.println("Export CSV Filename: " + pMap.get("s3Key"));
        System.out.println("Vendor Code: " + pMap.get("vendorCode"));
        System.out.println("Processing Bucket: " + pMap.get("s3BucketProcess"));
        System.out.println("Temp Json Bucket: " + pMap.get("s3BucketTempJson"));
        System.out.println("Guids Filename: " + pMap.get("s3KeyGuids"));

        Map<String, String> eventCode = new HashMap<String, String>();


        // initial variables for auto export Custs

        eventCode.put("eventCode", EventQueueConstants.MESSAGE_EXPORT_EVENT_CODE);
        eventCode.put("action", EventQueueConstants.MESSAGE_EXPORT_ACTION);
        eventCode.put("desc", EventQueueConstants.MESSAGE_EXPORT_DESC);
        eventCode.put("txnType", EventQueueConstants.TXN_TYPE);

        com.bigledger.core2.adapter.integration.utils.RequestHeaders requestHeaders1 = new com.bigledger.core2.adapter.integration.utils.RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));
        RequestHeaders requestHeaders = new RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));


        // Read Custs guids from S3
        List<String> guids=getCustGuids(s3Client,pMap,logger);

        logger.log("Guids: " + guids);

        // Use parallel call to update document status

        ParallelCallsService parallelCallPoDto = new ParallelCallsService();

    if(guids!=null) {
        List<Boolean> responses = parallelCallPoDto.getToDos(guids, requestHeaders1,requestHeaders, eventCode, logger);


        if (responses.contains(false)){

            logger.log("Error inserting document event");

        }
        else{

            // Move CSV file to main bucket if successful
            String status = moveFromIncomingToProcessing(s3Client, pMap.get("s3BucketProcess"), pMap.get("s3Key"),  pMap.get("s3Bucket"),  pMap.get("s3Key"),  logger);

            if (status=="succeeded"){
                logger.log("CSV files moved successfully!!!");
            }
            else {
                logger.log("Errror moving CSV file");
            }
        }
    }


        final long end = Calendar.getInstance().getTimeInMillis();
        logger.log(" export Custs time spent: " + (end - start) / 1000 + "s");

        return pMap;
    }


    protected List<String> getCustGuids (AmazonS3 s3Client,Map<String, String> pMap, LambdaLogger logger)
    {


        List<String> records = new ArrayList<String>();

        // initialize the input stream and buffer reader
        InputStream inputStream = null;
        BufferedReader br = null;

        if (pMap.get("s3KeyGuids")!=null)
        {
            String s3KeyGuids = pMap.get("s3KeyGuids");
            String s3BucketTempJson = pMap.get("s3BucketTempJson");
            logger.log("found id: " + s3BucketTempJson + " " + s3KeyGuids);
            // retrieve s3 object
            S3Object object = s3Client.getObject(new GetObjectRequest(s3BucketTempJson, s3KeyGuids));


            try {

                inputStream = object.getObjectContent();

                br = new BufferedReader(new InputStreamReader(inputStream));


                List<String> recordsChunk = new ArrayList<String>();


                int lines = 1;     // initialize the required number of lines
                int currentLine = 0; // initiate the current read line of the string buffer
                int fileNumber = 0; // initialize the file number to save the splitted files


                String line = "";

                // read all the lines from the input buffer stream
                while ((line = br.readLine()) != null) {
                    records.add(line);
                }


            } //end for (split file)
            catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else{
            records=null;
        }


        return records;
    }

    protected String moveFromIncomingToProcessing (AmazonS3 s3Client, String bucketFrom, String s3KeyFrom, String bucketTo, String s3KeyTo,  LambdaLogger logger)
    {
        String status = "succeeded";
        boolean copySucceeded = false;
        boolean deleteSucceeded = false;


        try {
            logger.log("moveFromIncomingToProcessing : bucketFrom :" + bucketFrom);
            logger.log("moveFromIncomingToProcessing : bucketTo :" + bucketTo);
            logger.log("moveFromIncomingToProcessing : s3KeyFrom :" + s3KeyFrom);
            logger.log("moveFromIncomingToProcessing : s3KeyTo :" + s3KeyTo);
            CopyObjectResult copyObjectResult = s3Client.copyObject(bucketFrom, s3KeyFrom, bucketTo, s3KeyTo);
            copySucceeded = true;

        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            status = "failed";
            //System.exit(1);
        }
        finally
        {

        }

        if (copySucceeded == true) {
            try {
                s3Client.deleteObject(bucketFrom, s3KeyFrom);
                deleteSucceeded = true;
            } catch (AmazonServiceException e) {
                System.err.println(e.getErrorMessage());
                status = "failed";
                //System.exit(1);
            } finally {

            }
        }

        return status;
    }

}
