package app;
public class PushCustDtoFromCsvRequest {

    private String input;

    public PushCustDtoFromCsvRequest(String input) {
        this.input = input;
    }

    public PushCustDtoFromCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
