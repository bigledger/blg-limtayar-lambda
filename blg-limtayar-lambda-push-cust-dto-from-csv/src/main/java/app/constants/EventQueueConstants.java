package app.constants;

public interface EventQueueConstants {


    //EMP QUEUE	EVENTS

    String TXN_TYPE = "SYS_APPLET";

    String MESSAGE_EXPORT_EVENT_CODE = "CKL_SALES_INVOICE_EXPORTED";
    String MESSAGE_EXPORT_ACTION = "CKL_SALES_INVOICE_EXPORTED";
    String MESSAGE_EXPORT_DESC = "CKL SALES INVOICE HAS BEEN EXPORTED";

//    String MESSAGE_ACCEPT_EVENT_CODE = "SALES_INVOICE_UPDATED";
//    String MESSAGE_ACCEPT_ACTION = "SALES_INVOICE_CREATED";
//    String MESSAGE_ACCEPT_DESC = "Auto changed status to accepted";

    //Sleep timers
    Integer HOUR = 3600;
    Integer ONE_MINUTE = 60;
    Integer FIVE_MINUTES = ONE_MINUTE * 5;
    Integer TEN_MINUTES = ONE_MINUTE * 10;
    Integer DAY = HOUR * 24;
}
