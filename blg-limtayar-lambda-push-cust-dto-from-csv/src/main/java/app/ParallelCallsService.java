package app;

import app.constants.LimtayarCustCsvFields;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.dto.EntityDto;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ParallelCallsService {

    PushCustDto pushCustDto = new PushCustDto();

    public List<EntityDto> pushCustDto(List<EntityDto> customerList, RequestHeaders requestHeaders) {

        List<CompletableFuture<EntityDto>> futures =
                customerList.stream().map(customer -> pushCustDtoAsync(customer, requestHeaders))
                        .filter(Objects::nonNull).collect(Collectors.toList());

        List<EntityDto> result = futures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());


        return result;
    }

    CompletableFuture<EntityDto> pushCustDtoAsync(EntityDto customer, RequestHeaders requestHeaders) {

        CompletableFuture<EntityDto> future = CompletableFuture.supplyAsync(new Supplier<EntityDto>() {

            @Override
            public EntityDto get() {
                boolean custExist = false;
                EntityDto entityDto = new EntityDto();
                try {
                    custExist = pushCustDto.customerExist(customer, requestHeaders);

                    if (custExist) {
                        entityDto = pushCustDto.updateCustomer(customer, requestHeaders);
                    } else {
                        entityDto = pushCustDto.insertCustomer(customer, requestHeaders);
                    }

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

                return entityDto;
            }
        });

        return future;
    }

}
