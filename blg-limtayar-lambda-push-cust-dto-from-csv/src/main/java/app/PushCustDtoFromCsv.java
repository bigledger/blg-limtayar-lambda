package app;


import app.constants.LimtayarCustCsvFields;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;


import java.io.*;
import java.util.*;

import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.dto.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.springframework.core.ParameterizedTypeReference;


public class PushCustDtoFromCsv implements RequestHandler<Map<String, String>, Map<String, String>> {

    @Override
    public Map<String, String> handleRequest(Map<String, String> pMap, Context context) {

        final long start = Calendar.getInstance().getTimeInMillis();
        LambdaLogger logger = context.getLogger();

        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        System.out.println("Input Map: " + pMap);
        System.out.println("Filename: " + pMap.get("s3Key"));
        System.out.println("Vendor Code: " + pMap.get("vendorCode"));
        System.out.println("Processing Bucket: " + pMap.get("s3BucketProcess"));

        String bucketName = pMap.get("s3BucketName");
        String fileName = pMap.get("s3FileName");
        String jsonCustArray = pMap.get("jsonCustArray");

        RequestHeaders requestHeaders = new RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));

        // validate the customer object before insert data
        System.out.println("jsonCustArray: " + jsonCustArray);

        ObjectMapper objectMapper = new ObjectMapper();
        List<EntityDto> customerList = new ArrayList<>();

        try {

            customerList = objectMapper.readValue(jsonCustArray, ArrayList.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (customerList.size() > 0) {
            ParallelCallsService parallelCallsService = new ParallelCallsService();

            List<EntityDto> pushedCustList = parallelCallsService.pushCustDto(customerList, requestHeaders);

        } else {
            logger.log("customerList is empty");
        }


        final long end = Calendar.getInstance().getTimeInMillis();
        System.out.println("push cust dto time spent: " + (end - start) / 1000 + "s");

        return pMap;

    }

}





