package app;

public class PushCustDtoFromCsvResponse {

    private String hello;

    public PushCustDtoFromCsvResponse(String hello) {
        this.hello = hello;
    }

    public PushCustDtoFromCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
