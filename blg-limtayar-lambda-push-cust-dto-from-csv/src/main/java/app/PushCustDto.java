package app;

import app.constants.LimtayarCustCsvFields;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.CustomerServiceImpl;
import com.bigledger.core1.adapter.integration.services.ICustomerService;
import com.bigledger.core1.adapter.integration.services.ISubqueryService;
import com.bigledger.core1.adapter.integration.services.SubqueryServiceImpl;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.adapter.integration.utils.TimeZoneConversions;
import com.bigledger.core1.api.ResponseCodeConstants;
import com.bigledger.core1.api.constants.ExtensionParamTypes;
import com.bigledger.core1.api.criteria.CustomerCriteria;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.CustomFieldDto;
import com.bigledger.core1.dto.EntityDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;
import com.bigledger.core1.dto.id.PagingResponse;
import com.bigledger.core1.util.StringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class PushCustDto {

    public boolean customerExist(EntityDto customer, RequestHeaders requestHeaders) {

        boolean custExist = false;
        String customerPkid = "";
        List<CustomFieldDto> customFieldDtoList = customer.getCustomFieldDtos();

        // get customer pkid provided in the csv file
        for (CustomFieldDto customFieldDto : customFieldDtoList) {
            if (customFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_PKID")) {
                customerPkid = customFieldDto.getValueString();
                break;
            }
        }

        // check the existence of customer base on customer pkid
        if (!StringUtil.isNullOrEmpty(customerPkid)) {
//            String entityHdrGuid = getEntityHdrGuid(customerPkid, requestHeaders);
            EntityDto existedCust = getEntityDtoByPkid(customerPkid, requestHeaders);
            if (existedCust != null) {
                custExist = true;
                customer.setGuid(existedCust.getGuid());

                List<CustomFieldDto> existedCustomFieldList = existedCust.getCustomFieldDtos();
                for (int i = 0; i < customer.getCustomFieldDtos().size(); i++) {
                    for (int j = 0; j < existedCustomFieldList.size(); j++) {
                        if (customer.getCustomFieldDtos().get(i).getParamCode().equalsIgnoreCase(existedCustomFieldList.get(j).getParamCode())) {
                            String customFieldGuid = existedCustomFieldList.get(j).getGuid();
                            customer.getCustomFieldDtos().get(i).setGuid(customFieldGuid);
                        }
                    }
                }
            }
        }

        return custExist;
    }

    protected String getEntityHdrGuid(String id, RequestHeaders requestHeaders) {
        // assuming the waveletInternalId is pkid
        String sql = "SELECT entity_hdr_guid as neededguid FROM bl_fi_mst_entity_ext " +
                " WHERE param_code = 'EMP_CUSTOMER_PKID' " +
                " AND value_string = '" + id + "' ;";
        System.out.println("sql=" + sql);
        List<String> guids = new ArrayList<>();
        Map<String,String> subquery = new HashMap<String, String>();
        subquery.put("subquery", sql);

        try{
            ISubqueryService subqueryService = new SubqueryServiceImpl();

            ApiResponse responseSubquery = subqueryService.getGuidsBySubquery(subquery,requestHeaders);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseSubquery.asJson());
            System.out.println("GUIDs: " + json.get("data").toString());

            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            guids = mapper.readValue(json.get("data").toString(), ArrayList.class);

        } catch (AkaunApiServerException e){
            System.out.println("AkaunApiServerException");
            e.printStackTrace();
            System.out.println(e.getMessage());
        } catch (NetworkException e){
            System.out.println("NetworkException");
            e.printStackTrace();
            System.out.println(e.getMessage());
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            if (guids.size() > 0) {
                return guids.get(0);
            } else {
                return null;
            }
        }
    }

    protected EntityDto getEntityDtoByPkid(String pkid, RequestHeaders requestHeaders) {
        System.out.println("custAccountPkid: " + pkid);

        PagingResponse pagingResponseGetCustomer = new PagingResponse();
        CustomerCriteria customerCriteria = new CustomerCriteria();
        ICustomerService customerService = new CustomerServiceImpl();
        EntityDto customer = new EntityDto();

        customerCriteria.setParamCode("EMP_CUSTOMER_PKID");
        customerCriteria.setValueString(pkid);

        try {
            pagingResponseGetCustomer = customerService.getCustomerDtoByCriteriaProcess(customerCriteria, requestHeaders);

        } catch (AkaunApiServerException e) {
            System.out.println(e.getMessage());
            return null;
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

        if (pagingResponseGetCustomer.getObjectList().size() > 0) {
            List<EntityDto> blgCustomerDto = pagingResponseGetCustomer.getObjectList();

            customer = blgCustomerDto.get(0);
            return customer;
        } else {
            System.out.println("pagingResponseGetCustomer size: " + pagingResponseGetCustomer.getObjectList().size());
            return null;
        }
    }

    public EntityDto insertCustomer(EntityDto customer, RequestHeaders requestHeaders) {
        ApiResponse response = new ApiResponse<>();
        ICustomerService customerService = new CustomerServiceImpl();

        try {
            response = customerService.createCustomer(customer, requestHeaders);

            if (response.getCode().equals(ResponseCodeConstants.OK_RESPONSE)) {
                System.out.println("sendData OK");
                return customer;
            } else {
                System.out.println("sendData Failed");
                return null;
            }
        } catch (AkaunApiServerException e) {
            System.out.println(e.getMessage());
            return null;
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    public EntityDto updateCustomer(EntityDto customer, RequestHeaders requestHeaders) {
        ApiResponse response = new ApiResponse<>();
        ICustomerService customerService = new CustomerServiceImpl();
        String returnMsg = "";
        try {
            response = customerService.updateCustomer(customer, requestHeaders);
            returnMsg = response.getCode();
            if (response.getCode().equals(ResponseCodeConstants.OK_RESPONSE)) {
                System.out.println("OK_RESPONSE (UPDATED)");
                return customer;
            } else {
                System.out.println("FAILED (UPDATED)");
                return null;
            }
        } catch (AkaunApiServerException e) {
            return null;
        } catch (NetworkException e) {
            return null;
        } catch (Exception e) {
            return null;
        }

    }

}
