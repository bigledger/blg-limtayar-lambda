package app;
public class TransformCustToCsvRequest {

    private String input;

    public TransformCustToCsvRequest(String input) {
        this.input = input;
    }

    public TransformCustToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
