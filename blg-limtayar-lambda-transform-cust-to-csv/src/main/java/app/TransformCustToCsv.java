package app;


import app.constants.CsvConstants;
import app.constants.LimtayarCustCsvHeader;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaAsyncClient;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification;


import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.*;
import com.bigledger.core1.adapter.integration.constants.Constants;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.*;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.api.ResponseCodeConstants;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.common.exception.BlgValidationException;
import com.bigledger.core1.dto.*;
import com.bigledger.core1.dto.id.LoginResponse;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import app.constants.EventQueueConstants;
import org.springframework.http.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;


public class TransformCustToCsv implements RequestHandler<Map<String, String>, Map<String, String>> {

    @Override
    public Map<String, String> handleRequest(Map<String, String> pMap, Context context) {

        final long start = Calendar.getInstance().getTimeInMillis();

        List<EntityDto> entityDtoList = new ArrayList<EntityDto>();
        Map<String, String> subquery = new HashMap<String, String>();
        List<String> guids = new ArrayList<>();

        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        System.out.println("Input Map: " + pMap);
        System.out.println("Filename: " + pMap.get("s3Key"));
        System.out.println("Processing Bucket: " + pMap.get("s3BucketProcess"));

        RequestHeaders requestHeaders = new RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));


        //Query to get all customer
//        String custSql = "SELECT e1.guid,e1.remote_doc_id AS neededguid FROM app_message_event AS e1 " +
//                " WHERE e1.event_code = 'PROCESSED_CUSTOMER_CREATED'" +
//                " AND (e1.guid not in (SELECT e2.link_guid FROM app_message_event AS e2 WHERE e2.event_code = 'CKL_CUSTOMER_LISTING_EXPORTED'))" +
//                " ORDER BY local_created_time" +
//                " LIMIT " + pMap.get("limitExport") + ";";
//       This query logic is incorrect please do not use it.
//        String custSql = "SELECT e1.guid,e1.hdr_guid AS neededguid FROM bl_fi_mst_entity_event AS e1 "+
//                        " WHERE e1.event_code = 'CUSTOMER_CREATED'" +
//                        " AND (e1.guid not in (SELECT e2.link_guid FROM bl_fi_mst_entity_event AS e2 WHERE e2.event_code = 'CKL_CUSTOMER_LISTING_EXPORTED'))" +
//                        " ORDER BY local_created_time" +
//                        " LIMIT " + pMap.get("limitExport") + ";";

        String custSql = " select t1.hdr_guid AS neededguid from  ( select guid,hdr_guid from bl_fi_mst_entity_event where link_guid is null and event_code = 'CUSTOMER_CREATED' ) as t1 " +
        " left join  ( select link_guid from bl_fi_mst_entity_event where event_code = 'CKL_CUSTOMER_LISTING_EXPORTED' and link_guid is not null ) as t2 " +
        " ON t1.guid = t2.link_guid where t2.link_guid is null " +
        " ORDER BY local_created_time " +
        " LIMIT " + pMap.get("limitExport") + ";";
// Harerimana BLPR-11212


        subquery.put("subquery", custSql);

        System.out.println("Querying database for customer");
        //System.out.println("vendorCode:"+pMap.get("vendorCode"));
        System.out.println(custSql);


        try {


            //  get all accepted and new panasonic purchase orders
            ISubqueryService subqueryService = new SubqueryServiceImpl();

            ApiResponse responseSubquery = subqueryService.getGuidsBySubquery(subquery, requestHeaders);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseSubquery.asJson());
            System.out.println("GUIDs: " + json.get("data").toString());


            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            guids = mapper.readValue(json.get("data").toString(), ArrayList.class);

            // Use parallel call to get Customer DTOs

            ParallelCallsService parallelCallCustDto = new ParallelCallsService();


            List<ApiResponse> responses = parallelCallCustDto.getCustomerToDos(guids,requestHeaders);

            System.out.println("Count Customer DTOs: " + responses.size());
            System.out.println("First Customer DTO: " + responses.get(0));

            for (int x = 0; x < responses.size(); x++){

                JSONParser parser1 = new JSONParser();
                JSONObject json1 = (JSONObject) parser1.parse(responses.get(x).asJson());

                ObjectMapper mapper1 = new ObjectMapper();
                mapper1.registerModule(new JavaTimeModule());
                mapper1.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

                EntityDto entityCustHdrDto = mapper1.readValue(json1.get("data").toString(), EntityDto.class);

                if (entityCustHdrDto.getCustomFieldDtos() != null) {
                    System.out.println("cust custom field : " + entityCustHdrDto.getCustomFieldDtos().size());
                } else {
                    System.out.println("No custom field");
                    return pMap;
                }

//                if (genericPoHdrDto.getGenericDocLineDtoList() != null) {
//                    System.out.println("po doc line  " + genericPoHdrDto.getGenericDocLineDtoList().size());
//                } else {
//                    System.out.println("No generic doc line");
//                    return pMap;
//                }

                entityDtoList.add(entityCustHdrDto);
                System.out.println(entityCustHdrDto);
            }

            System.out.println("No. of Custs: " + entityDtoList.size());


            // check if there is a customer
            if (entityDtoList.size() > 0) {

                // Use parallel call to get Customer DTOs transformed

                List<LimtayarCustCsvHeader> limtayarCustCsvHeader = parallelCallCustDto.getTransformDtoToCsvToDos(entityDtoList,requestHeaders);

                System.out.println("No of DTOs: " + limtayarCustCsvHeader.size());

                String cust = "";

                if (limtayarCustCsvHeader.size()>0)
                {

                    List<String> dtoInCsv = null;
                    String stringCusts = "";
                    //Delimiter used in CSV file
                    String BAR_DELIMITER = "|";
                    String NEW_LINE_SEPARATOR = "\n";
                    //String DOUBLE_QUOTE = "\"";

                    String FILE_HEADER = "\"" + "Name" + "\"" + "|"  + "\"" + "Mobile Phone"  + "\"" + "|"
                            + "\"" + "Date Created"  + "\"" + "|"  + "\"" + "Last Update"  + "\"" + "|"  + "\"" + "Vehicle Reg Num"  + "\"" +
                            "|"  + "\"" + "Email"  + "\"" + "|" + "\"" + "Address1"  + "\"" + "|"  + "\"" +
                            "Address2"  + "\"" + "|"  + "\"" + "Address3"  + "\"" +"|" + "\"" + "City"  + "\"" +
                            "|"  + "\"" + "Postcode"  + "\"" + "|"  + "\"" + "State"  + "\"" +
                            "|"  + "\"" + "Country"  + "\"" + "|"  + "\"" + "Branch Code"  + "\"" + "|"  + "\"" + "Dealer Code"  + "\"" +"|"+ "\"" + "Customer PKID" + "\"";

                    //Write the CSV file header
                    stringCusts = stringCusts + FILE_HEADER;

                    //Add a new line separator after the header
                    stringCusts = stringCusts + CsvConstants.NEW_LINE_SEPARATOR;

                    for (int y=0; y< limtayarCustCsvHeader.size(); y++) {

                        //dtoInCsv = parallelCallCustDto.getWriteDtoToCsvToDos(limtayarCustCsvHeader.get(y));

                        stringCusts = stringCusts + parallelCallCustDto.getWriteDtoToCsvToDos(limtayarCustCsvHeader.get(y));

//                        for (int z=0; z < dtoInCsv.size(); z++) {
//                            stringCusts = stringCusts + dtoInCsv.get(z);
//                        }

                    }

                        // Write transformed DTO to processing s3
                        try {


                            System.out.println("writing CSV to s3Bucket:" + pMap.get("s3BucketProcess") + " s3Key:" + pMap.get("s3Key"));
                            System.out.println("Writing CSV");
                            String csvString = stringCusts;
//                            final String CONTENT_TYPE = "application/csv";
                            final String CONTENT_TYPE = "plain/text";
                            byte[] fileContentBytes = csvString.getBytes(StandardCharsets.UTF_8);
                            InputStream fileInputStream = new ByteArrayInputStream(fileContentBytes);
                            ObjectMetadata metadata = new ObjectMetadata();
                            metadata.setContentType(CONTENT_TYPE);
                            metadata.setContentLength(fileContentBytes.length);

                            PutObjectRequest putObjectRequest = new PutObjectRequest(
                                    pMap.get("s3BucketProcess"), pMap.get("s3Key"), fileInputStream, metadata);
                            s3Client.putObject(putObjectRequest);
                        } catch (SdkClientException e) {
                            e.printStackTrace();
                            System.out.println(e.getMessage());
                            return pMap;
                        } finally {
                            System.out.println("Exported Customer in CSV to s3Bucket : " + " " + pMap.get("s3BucketProcess") + " s3Key : " + pMap.get("s3Key"));
                        }

                        cust = stringCusts;

                }

                ZonedDateTime printDate = ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur"));
                String custTimestamp = DateTimeFormatter.ofPattern("yyyyMMddhhMMssS").format(printDate);
                String s3Keytemp = "CKL CUSTOMER LISTING/"+"_"+custTimestamp+".json";

                // Write guids to temp json s3
                try {
                     String stringGuids = "";
//
                    for(int q=0; q<guids.size(); q++){

                        stringGuids=guids.get(q) + CsvConstants.NEW_LINE_SEPARATOR + stringGuids;

                    }

                    System.out.println("writing CSV to s3Bucket:" + pMap.get("s3BucketTempJson") + " s3Key:" + s3Keytemp);
                    System.out.println("Writing CSV");
                    String csvString = stringGuids;
                    final String CONTENT_TYPE = "application/csv";
                    byte[] fileContentBytes = csvString.getBytes(StandardCharsets.UTF_8);
                    InputStream fileInputStream = new ByteArrayInputStream(fileContentBytes);
                    ObjectMetadata metadata = new ObjectMetadata();
                    metadata.setContentType(CONTENT_TYPE);
                    metadata.setContentLength(fileContentBytes.length);

                    PutObjectRequest putObjectRequest = new PutObjectRequest(
                            pMap.get("s3BucketTempJson"), s3Keytemp, fileInputStream, metadata);
                    s3Client.putObject(putObjectRequest);
                } catch (SdkClientException e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                    return pMap;
                } finally {
                    System.out.println("Exported Cust in CSV to s3Bucket : " + " " + pMap.get("s3BucketTempJson") + " s3Key : " + s3Keytemp);
                }

                pMap.put("s3KeyGuids", s3Keytemp);


            }
            else {
                System.out.println("No customer to export");
                return pMap;
            }


        } catch (AkaunApiServerException e) {
            System.out.println("AkaunApiServerException");
            e.printStackTrace();
            System.out.println(e.getMessage());
            return pMap;

        } catch (NetworkException e) {
            System.out.println("NetworkException");
            e.printStackTrace();
            System.out.println(e.getMessage());
            return pMap;
        } catch (Exception ex) {
            System.out.println(ex);
        }

        final long end = Calendar.getInstance().getTimeInMillis();
        System.out.println(" export cust time spent: " + (end - start) / 1000 + "s");

        return pMap;

    }


}





