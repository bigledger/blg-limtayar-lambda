package app;

public class TransformCustToCsvResponse {

    private String hello;

    public TransformCustToCsvResponse(String hello) {
        this.hello = hello;
    }

    public TransformCustToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
