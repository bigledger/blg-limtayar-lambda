package app;

import app.constants.CsvConstants;
import app.constants.LimtayarCustCsvHeader;

public class WriteDtoToCsv {

    public  String getDtoInCsv(LimtayarCustCsvHeader custDtoMapToCsv) {


        String stringCusts = "";

        String name="";
        String mobilePhone="";
        String dateCreated="";
        String lastUpdated="";
        String vehicleRegNum="";
        String email="";
        String address1="";
        String address2="";
        String address3="";
        String city="";
        String postcode="";
        String state="";
        String country="";
        String branchCode="";
        String customerPKID="";
        String customerDealerCode="";

        if(custDtoMapToCsv.getName()!=null) {
            name = custDtoMapToCsv.getName();
        }
        if(custDtoMapToCsv.getMobilePhone()!=null) {
            mobilePhone = custDtoMapToCsv.getMobilePhone();
        }
        if(custDtoMapToCsv.getDateCreated()!=null) {
            dateCreated = custDtoMapToCsv.getDateCreated();
        }
        if(custDtoMapToCsv.getLastUpdated()!=null) {
            lastUpdated = custDtoMapToCsv.getLastUpdated();
        }
        if(custDtoMapToCsv.getVehicleRegNum()!=null) {
            vehicleRegNum = custDtoMapToCsv.getVehicleRegNum();
        }
        if(custDtoMapToCsv.getEmail()!=null) {
            email = custDtoMapToCsv.getEmail();
        }
        if(custDtoMapToCsv.getAddress1()!=null) {
            address1 = custDtoMapToCsv.getAddress1();
        }
        if(custDtoMapToCsv.getAddress2()!=null) {
            address2 = custDtoMapToCsv.getAddress2();
        }
        if(custDtoMapToCsv.getAddress3()!=null) {
            address3 = custDtoMapToCsv.getAddress3();
        }
        if(custDtoMapToCsv.getCity()!=null) {
            city = custDtoMapToCsv.getCity();
        }
        if(custDtoMapToCsv.getPostcode()!=null) {
            postcode = custDtoMapToCsv.getPostcode();
        }
        if(custDtoMapToCsv.getState()!=null) {
            state = custDtoMapToCsv.getState();
        }
        if(custDtoMapToCsv.getCountry()!=null) {
            country = custDtoMapToCsv.getCountry();
        }
        if(custDtoMapToCsv.getBranchCode()!=null) {
            branchCode = custDtoMapToCsv.getBranchCode();
        }
        if(custDtoMapToCsv.getCustomerPKID()!=null) {
            customerPKID = custDtoMapToCsv.getCustomerPKID();
        }
        if(custDtoMapToCsv.getCustomerDealerCode()!=null) {
            customerDealerCode = custDtoMapToCsv.getCustomerDealerCode();
        }


        stringCusts = CsvConstants.DOUBLE_QUOTE + name + CsvConstants.DOUBLE_QUOTE  + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + mobilePhone + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + dateCreated + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + lastUpdated + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + vehicleRegNum + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + email + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + address1 + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + address2 + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + address3 + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + city + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + postcode + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + state + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + country + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + branchCode + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + customerDealerCode + CsvConstants.DOUBLE_QUOTE + CsvConstants.BAR_DELIMITER +
                CsvConstants.DOUBLE_QUOTE + customerPKID + CsvConstants.DOUBLE_QUOTE +
                CsvConstants.NEW_LINE_SEPARATOR;

        return stringCusts;

    }
}
