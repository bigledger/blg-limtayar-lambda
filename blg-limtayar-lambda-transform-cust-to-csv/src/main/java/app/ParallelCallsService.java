package app;

import app.constants.LimtayarCustCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.CustomerServiceImpl;
import com.bigledger.core1.adapter.integration.services.ICustomerService;
import com.bigledger.core1.adapter.integration.services.IPurchaseOrderService;
import com.bigledger.core1.adapter.integration.services.PurchaseOrderServiceImpl;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.EntityDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ParallelCallsService {


        ICustomerService customerService = new CustomerServiceImpl();
        TransformDtoToCsv transformDtoToCsv = new TransformDtoToCsv();
        TransformItemToDto transformItemToDto = new TransformItemToDto();
        WriteDtoToCsv writeDtoToCsv = new WriteDtoToCsv();

        public List<ApiResponse> getCustomerToDos(List<String> ids, RequestHeaders requestHeaders){

            List<CompletableFuture<ApiResponse>> futures =
                    ids.stream()
                            .map(id -> getCustomerToDoAsync(id, requestHeaders))
                            .collect(Collectors.toList());

            List<ApiResponse> result =
                    futures.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList());

            return result;
        }
        CompletableFuture<ApiResponse> getCustomerToDoAsync(String id, RequestHeaders requestHeaders){

            CompletableFuture<ApiResponse> future = CompletableFuture.supplyAsync(new Supplier<ApiResponse>() {
                @Override
                public ApiResponse get() {

                     ApiResponse response = null;

                    try {

                          response = customerService.getCustomerbyGuid(id, requestHeaders);

                    }
                    catch (AkaunApiServerException e) {
                        System.out.println("AkaunApiServerException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;

                    } catch (NetworkException e) {
                        System.out.println("NetworkException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                    return response;
                }
            });

            return future;
        }
//
//    public List<LimtayarCustCsvHeader> getTransformDtoToCsvToDos(List<EntityDto> entityDtos, RequestHeaders requestHeaders){
//
//        List<CompletableFuture<List<LimtayarCustCsvHeader>> futures =
//                entityDtos.stream()
//                        .map(entityDto -> getTransformDtoToCsvToDoAsync(entityDto, requestHeaders))
//                        .collect(Collectors.toList());
//
//        List<LimtayarCustCsvHeader> result =
//                futures.stream()
//                        .map(CompletableFuture::join)
//                        .collect(Collectors.toList());
//
//        return result;
//    }



    public List<LimtayarCustCsvHeader> getTransformDtoToCsvToDos(List<EntityDto> entityDtos, RequestHeaders requestHeaders){

        List<CompletableFuture<LimtayarCustCsvHeader>> futures =
                entityDtos.stream()
                        .map(entityDto -> getTransformDtoToCsvToDoAsync(entityDto, requestHeaders))
                        .collect(Collectors.toList());

        List<LimtayarCustCsvHeader> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<LimtayarCustCsvHeader> getTransformDtoToCsvToDoAsync(EntityDto entityDto, RequestHeaders requestHeaders){

        CompletableFuture<LimtayarCustCsvHeader> future = CompletableFuture.supplyAsync(new Supplier<LimtayarCustCsvHeader>() {
            @Override
            public LimtayarCustCsvHeader get() {

                LimtayarCustCsvHeader response = null;

                response = transformDtoToCsv.getCust(entityDto, requestHeaders);

                return response;
                }

        });

        return future;
    }


//    public List<LimtayarCustCsvHeader> getTransformDtoItemToCsvToDos(List<GenericDocLineDto> genericDocLineDtos, LimtayarCustCsvHeader custDtoToCsvModel, RequestHeaders requestHeaders)
//    {
//
//        List<CompletableFuture<LimtayarCustCsvHeader>> futures =
//                genericDocLineDtos.stream()
//                        .map(genericDocLineDto -> getTransformDtoItemToCsvToDoAsync(genericDocLineDto, custDtoToCsvModel, requestHeaders))
//                        .collect(Collectors.toList());
//
//        List<LimtayarCustCsvHeader> result =
//                futures.stream()
//                        .map(CompletableFuture::join)
//                        .collect(Collectors.toList());
//
//        return result;
//    }
//
//    CompletableFuture<LimtayarCustCsvHeader> getTransformDtoItemToCsvToDoAsync(GenericDocLineDto entityDto, LimtayarCustCsvHeader poDtoToCsvModel, RequestHeaders requestHeaders)
//    {
//
//        CompletableFuture<LimtayarCustCsvHeader> future = CompletableFuture.supplyAsync(new Supplier<LimtayarCustCsvHeader>() {
//            @Override
//            public LimtayarCustCsvHeader get() {
//
//                LimtayarCustCsvHeader response = null;
//
//                response = transformItemToDto.getCustItem(entityDto,poDtoToCsvModel,requestHeaders);
//
//                return response;
//            }
//
//        });
//
//        return future;
//    }


//    public List<String> getWriteDtoToCsvToDos(List<LimtayarCustCsvHeader> poDtoMapToCsvs){
//
//        List<CompletableFuture<String>> futures =
//                poDtoMapToCsvs.stream()
//                        .map(poDtoMapToCsv -> getWriteDtoToCsvToDoAsync(poDtoMapToCsv))
//                        .collect(Collectors.toList());
//
//        List<String> result =
//                futures.stream()
//                        .map(CompletableFuture::join)
//                        .collect(Collectors.toList());
//
//        return result;
//    }

    public String getWriteDtoToCsvToDos(LimtayarCustCsvHeader poDtoMapToCsv){

        CompletableFuture<String> future = getWriteDtoToCsvToDoAsync(poDtoMapToCsv);

        String result = future.join();

        return result;
    }

    CompletableFuture<String> getWriteDtoToCsvToDoAsync(LimtayarCustCsvHeader poDtoMapToCsv){

        CompletableFuture<String> future = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {

                String response = null;

                response = writeDtoToCsv.getDtoInCsv(poDtoMapToCsv);

                return response;
            }

        });

        return future;
    }


}
