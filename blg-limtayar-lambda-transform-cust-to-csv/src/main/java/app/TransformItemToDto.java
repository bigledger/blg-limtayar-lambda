package app;

import app.constants.LimtayarCustCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.CustomerServiceImpl;
import com.bigledger.core1.adapter.integration.services.ICustomerService;
import com.bigledger.core1.adapter.integration.services.IItemService;
import com.bigledger.core1.adapter.integration.services.ItemServiceImpl;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.EntityDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;
import com.bigledger.core1.dto.ItemHeaderDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;

import java.text.DecimalFormat;
import java.util.List;

public class TransformItemToDto {

    public LimtayarCustCsvHeader getCustItem(GenericDocLineDto entityDto, LimtayarCustCsvHeader custDtoToCsvModel, RequestHeaders requestHeaders) {


        LimtayarCustCsvHeader custDtoToCsvModelLine = new LimtayarCustCsvHeader();
        IItemService itemService = new ItemServiceImpl();
        ICustomerService customerService = new CustomerServiceImpl();
        EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
        DecimalFormat df2 = new DecimalFormat("0.00");

        custDtoToCsvModelLine.setName(custDtoToCsvModel.getName());
        custDtoToCsvModelLine.setMobilePhone(custDtoToCsvModel.getMobilePhone());
        custDtoToCsvModelLine.setDateCreated(custDtoToCsvModel.getDateCreated());
        custDtoToCsvModelLine.setLastUpdated(custDtoToCsvModel.getLastUpdated());

        custDtoToCsvModelLine.setVehicleRegNum(custDtoToCsvModel.getVehicleRegNum());
        custDtoToCsvModelLine.setEmail(custDtoToCsvModel.getEmail());
        custDtoToCsvModelLine.setAddress1(custDtoToCsvModel.getAddress1());
        custDtoToCsvModelLine.setAddress2(custDtoToCsvModel.getAddress2());
        custDtoToCsvModelLine.setAddress3(custDtoToCsvModel.getAddress3());
        custDtoToCsvModelLine.setCity(custDtoToCsvModel.getCity());
        custDtoToCsvModelLine.setPostcode(custDtoToCsvModel.getPostcode());
        custDtoToCsvModelLine.setState(custDtoToCsvModel.getState());
        custDtoToCsvModelLine.setCountry(custDtoToCsvModel.getCountry());
        custDtoToCsvModelLine.setBranchCode(custDtoToCsvModel.getBranchCode());
        custDtoToCsvModelLine.setCustomerDealerCode(custDtoToCsvModel.getCustomerDealerCode());
        custDtoToCsvModelLine.setCustomerPKID(custDtoToCsvModel.getCustomerPKID());


        // get UOM

//
//        try {
//            // check if customer exist in BLG
//            ApiResponse response = customerService.getCustomerbyGuid(entityDto.getGuid(), requestHeaders);
//
//
//            JSONParser parser = new JSONParser();
//            JSONObject json = (JSONObject) parser.parse(response.asJson());
////                    System.out.println("Item Data as Json: " + json.get("data"));
//
//            ObjectMapper mapper = new ObjectMapper();
//            mapper.registerModule(new JavaTimeModule());
//            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//            ItemHeaderDto itemHdrDto = mapper.readValue(json.get("data").toString(), ItemHeaderDto.class);
//
//            custDtoToCsvModelLine.setUom(itemHdrDto.getUom());
//
//        } catch (
//                AkaunApiServerException e) {
//            System.out.println(e.getMessage());
//            etlSendDataResponse.setEventCode("FAILED");
//            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        } catch (
//                NetworkException e) {
//            System.out.println(e.getMessage());
//            etlSendDataResponse.setEventCode("RETRY");
//            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//
//            System.out.println(e.getClass().getCanonicalName());
//            etlSendDataResponse.setEventCode("FAILED");
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        }
//
//        double quantity =  genericDocLineDto.getQuantityBase().doubleValue();
//        String strQuantity = df2.format(quantity);
//        double invoicePrice = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//        String invPrice = df2.format(invoicePrice);
//
//        double NettPrice = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//        String netPrice = df2.format(NettPrice);
//
//        poDtoToCsvModelLine.setPoType("0");
//        double gstRate =  genericDocLineDto.getTaxGstRate().doubleValue();
//        String strGstRate = df2.format(gstRate);
//        double gstAmount = genericDocLineDto.getAmountTaxGst().doubleValue();
//        String strGstAmount = df2.format(gstAmount);
//        double totalCostNet = genericDocLineDto.getAmountNet().doubleValue();
//        String strTotalCostNet = df2.format (totalCostNet);
//
//        double totalCost = genericDocLineDto.getAmountTxn().doubleValue();
//        String strTotalCost = df2.format (totalCost);
//
//
//        poDtoToCsvModelLine.setSeqNo(genericDocLineDto.getPositionId());
//        poDtoToCsvModelLine.setItemNo(genericDocLineDto.getItemCode());
//        poDtoToCsvModelLine.setItemBarcode(genericDocLineDto.getItemMachineCode());
//        poDtoToCsvModelLine.setItemDescription(genericDocLineDto.getItemName());
//        poDtoToCsvModelLine.setOrderQuantity(strQuantity);
//        poDtoToCsvModelLine.setInvoicePrice(invPrice);
//        poDtoToCsvModelLine.setNettPrice(netPrice);
//        poDtoToCsvModelLine.setTotalCostNoGst(strTotalCostNet);
//        poDtoToCsvModelLine.setGstCode(genericDocLineDto.getTaxGstCode());
//        //poDtoToCsvModelLine.setGstRate(Double.valueOf(genericDocLineDto.getTaxGstRate().toString()));
//        //poDtoToCsvModelLine.setGstAmount(genericDocLineDto.getAmountTaxGst().doubleValue());
//        //poDtoToCsvModelLine.setTotalCost(genericDocLineDto.getAmountTxn().doubleValue());
//        poDtoToCsvModelLine.setGstRate(strGstRate);
//        poDtoToCsvModelLine.setGstAmount(strGstAmount);
//        poDtoToCsvModelLine.setTotalCost(strTotalCost);


        return custDtoToCsvModelLine;
    }
}
