package app;

import app.constants.LimtayarCustCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.IItemService;
import com.bigledger.core1.adapter.integration.services.ISubqueryService;
import com.bigledger.core1.adapter.integration.services.ItemServiceImpl;
import com.bigledger.core1.adapter.integration.services.SubqueryServiceImpl;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.domain.accounting.Customer;
import com.bigledger.core1.dto.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TransformDtoToCsv {

    public  LimtayarCustCsvHeader getCust(EntityDto entityDto, RequestHeaders requestHeaders) {

        //List<LimtayarCustCsvHeader> custDtoInCsv = new ArrayList<LimtayarCustCsvHeader>();
        EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
        ZoneId zoneId = ZoneId.of("Asia/Kuala_Lumpur");        //Zone information


        List<CustomFieldDto> entityExtDtoList = entityDto.getCustomFieldDtos();

//        ZonedDateTime zdtAtMalaysia = entityDto..withZoneSameInstant(zoneId);
        LimtayarCustCsvHeader custDtoToCsvModel = new LimtayarCustCsvHeader();


        // System.out.println("Zone date time: " + genericDocHdrDto.get(i).getDateTxn().toLocalDateTime());
        // System.out.println("Local date time: " + zdtAtMalaysia);
//
        DecimalFormat nft = new DecimalFormat("#00.###");
        DecimalFormat df2 = new DecimalFormat("#.##");
        nft.setDecimalSeparatorAlwaysShown(false);
//
//
//        String yyyy = Integer.toString(zdtAtMalaysia.getYear());
//        String mm = nft.format(zdtAtMalaysia.getMonthValue());
//        String dd = nft.format(zdtAtMalaysia.getDayOfMonth());
//        String hh = nft.format(zdtAtMalaysia.getHour());
//        String min = nft.format(zdtAtMalaysia.getMinute());
//        String orderDate = yyyy + mm + dd;
////
////        //Displaying current time in 12 hour format with AM/PM
//        DateFormat outputFormat = new SimpleDateFormat("hh:mmaa");
//        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.US);
//
//        // String inputText = "2012-11-17T00:00:00.000-05:00";
//        Date date = null;
//        try {
//            date = inputFormat.parse(zdtAtMalaysia.toString());
//        } catch (ParseException e) {
//            e.printStackTrace();
//            System.out.println(e);
//        }
//
//        String orderTime = outputFormat.format(date);


//         System.out.println("Current time in AM/PM: "+orderTime);


        // convert data from PO Hdr DTO to CSV model

//            custDtoToCsvModel.setPoNumber(genericDocHdrDto.getClientDoc1());
//            custDtoToCsvModel.setRemark1(genericDocHdrDto.getDocRemarks());
//            custDtoToCsvModel.setRemark2("");
//            custDtoToCsvModel.setOrderDate(orderDate);
//            custDtoToCsvModel.setOrderTime(orderTime);
        custDtoToCsvModel.setName(entityDto.getName());

        List<CustomFieldDto> customCriteriaFieldLists = entityDto.getCustomFieldDtos();

        for (int j = 0; j < customCriteriaFieldLists.size(); j++) {

            CustomFieldDto customCriteriaFieldDto = customCriteriaFieldLists.get(j);
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_MOBILE_PHONE")) {
                custDtoToCsvModel.setMobilePhone(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_DATE_CREATED"))
            {
                ZonedDateTime zdtAtMalaysia = customCriteriaFieldDto.getValueDate().withZoneSameInstant(zoneId);
                LocalDateTime createdDateTxnLdt = zdtAtMalaysia.toLocalDateTime();
                DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                custDtoToCsvModel.setDateCreated(createdDateTxnLdt.format(formatter3));
//                custDtoToCsvModel.setDateCreated(customCriteriaFieldDto.getValueDate().toString());
            }

            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_LAST_EDIT"))
            {
                ZonedDateTime zdtAtMalaysia = customCriteriaFieldDto.getValueDate().withZoneSameInstant(zoneId);
                LocalDateTime lastDateTxnLdt = zdtAtMalaysia.toLocalDateTime();
                DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                custDtoToCsvModel.setLastUpdated(lastDateTxnLdt.format(formatter4));
//                String LastDate = lastDateTxnLdt.format(formatter4);
//                custDtoToCsvModel.setLastUpdated(customCriteriaFieldDto.getValueDate().toString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_VEHICLE_REGNUM")) {
                custDtoToCsvModel.setVehicleRegNum(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_EMAIL1")) {
                custDtoToCsvModel.setEmail(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_ADDRESS1")) {
                custDtoToCsvModel.setAddress1(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_ADDRESS2")) {
                custDtoToCsvModel.setAddress2(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_ADDRESS3")) {
                custDtoToCsvModel.setAddress3(customCriteriaFieldDto.getValueString());
            }
//                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("SH_ERP_EXPIRY_DATE")) {
//
//                    ZonedDateTime zdtAtMalaysiaEx = customCriteriaFieldDto.getValueDate().withZoneSameInstant(zoneId);
//                    DecimalFormat nft1 = new DecimalFormat("#00.###");
//                    nft1.setDecimalSeparatorAlwaysShown(false);
//
//
//                    String yyyy1 = Integer.toString(zdtAtMalaysiaEx.getYear());
//                    String mm1 = nft1.format(zdtAtMalaysiaEx.getMonthValue());
//                    String dd1 = nft1.format(zdtAtMalaysiaEx.getDayOfMonth());
//
//                    String expireDate = yyyy1 + mm1 + dd1;
//
//                    poDtoToCsvModel.setExpiryDate(expireDate);
//                }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_CITY")) {
                custDtoToCsvModel.setCity(customCriteriaFieldDto.getValueString());
            }

            //Leave out Remarks 2 this is for 100 Value only not applicable for Panasonic
                /*if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("SH_ERP_REMARKS_2")) {
                    poDtoToCsvModel.setRemark2(customCriteriaFieldDto.getValueString());
                }*/
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_POSTCODE")) {
                custDtoToCsvModel.setPostcode(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_STATE")) {
                custDtoToCsvModel.setState(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_COUNTRY")) {
                custDtoToCsvModel.setCountry(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_BRANCH_CODE")) {
                custDtoToCsvModel.setBranchCode(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUSTOMER_PKID")) {
                custDtoToCsvModel.setCustomerPKID(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_DEALER_CODE")) {
                custDtoToCsvModel.setCustomerDealerCode(customCriteriaFieldDto.getValueString());
            }
        }

        //
        //Query to get delivered to name from delivered to code
        Map<String, String> subqueryDTS = new HashMap<String, String>();
        List<String> entityName = new ArrayList<>();

//            String dTSSql = "select loc.name AS neededguid from bl_inv_mst_location loc " +
//                    "inner join bl_fi_mst_branch branch on loc.code = branch.code " +
//                    "where loc.code = '" + poDtoToCsvModel.getDeliveryToStoreCode() + "' limit 1;";

//        String dTSSql = "select ent.name AS neededguid from bl_fi_mst_entity_hdr ent" +
//                "inner join bl_fi_mst_entity_ext ext on ent.guid = ext.entity_hdr_guid" +
//                "where ent.name = ' " + custDtoToCsvModel.getName() + " ' limit 1";
//
//
//        subqueryDTS.put("subquery", dTSSql);

//        System.out.println("Querying database for customer name ");

        //System.out.println("SQL Query: " + subqueryDTS);


//        try {
//
//            //  get delivered to store name
//            ISubqueryService subqueryServiceD = new SubqueryServiceImpl();
//
//            ApiResponse responseSubqueryD = subqueryServiceD.getGuidsBySubquery(subqueryDTS, requestHeaders);
//
//            JSONParser parser = new JSONParser();
//            JSONObject json2 = (JSONObject) parser.parse(responseSubqueryD.asJson());
//            //System.out.println("Delivered to Store: " + json2.get("data").toString());
//
//            ObjectMapper mapper = new ObjectMapper();
//            mapper.registerModule(new JavaTimeModule());
//            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//            entityName = mapper.readValue(json2.get("data").toString(), ArrayList.class);
//
//            for (int l = 0; l < entityName.size(); l++) {
//
//                // System.out.println("GUID Doc Header Counter: " + deliveryToStore.get(l));
//                custDtoToCsvModel.setName(entityName.get(l));
//
//            }
//
//        } catch (AkaunApiServerException e) {
//            System.out.println(e.getMessage());
//            etlSendDataResponse.setEventCode("FAILED");
//            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        } catch (NetworkException e) {
//            System.out.println(e.getMessage());
//            etlSendDataResponse.setEventCode("RETRY");
//            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//
//            System.out.println(e.getClass().getCanonicalName());
//            etlSendDataResponse.setEventCode("FAILED");
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        }

        return custDtoToCsvModel;
    }
}
