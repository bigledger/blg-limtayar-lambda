package app.constants;

public class LimtayarCustCsvHeader {

    private String name;
    private String mobilePhone;
    private String dateCreated;
    private String lastUpdated;
    private String vehicleRegNum;
    private String email;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String postcode;
    private String state;
    private String country;
    private String branchCode;
    private String customerPKID;
    private String customerDealerCode;

    public String getCustomerPKID() { return customerPKID; }

    public void setCustomerPKID(String customerPKID) { this.customerPKID = customerPKID; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getMobilePhone() { return mobilePhone; }

    public void setMobilePhone(String mobilePhone) { this.mobilePhone = mobilePhone; }

    public String getDateCreated() { return dateCreated; }

    public void setDateCreated(String dateCreated) { this.dateCreated = dateCreated; }

    public String getLastUpdated() { return lastUpdated; }

    public void setLastUpdated(String lastUpdated) { this.lastUpdated = lastUpdated; }

    public String getVehicleRegNum() { return vehicleRegNum; }

    public void setVehicleRegNum(String vehicleRegNum) { this.vehicleRegNum = vehicleRegNum; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getAddress1() { return address1; }

    public void setAddress1(String address1) { this.address1 = address1; }

    public String getAddress2() { return address2; }

    public void setAddress2(String address2) { this.address2 = address2; }

    public String getAddress3() { return address3; }

    public void setAddress3(String address3) { this.address3 = address3; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public String getPostcode() { return postcode; }

    public void setPostcode(String postcode) { this.postcode = postcode; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }

    public String getCountry() { return country; }

    public void setCountry(String country) { this.country = country; }

    public String getBranchCode() { return branchCode; }

    public void setBranchCode(String branchCode) { this.branchCode = branchCode; }

    public String getCustomerDealerCode() { return customerDealerCode; }

    public void setCustomerDealerCode(String customerDealerCode) { this.customerDealerCode = customerDealerCode;}
}
