package app.constants;

public interface CsvConstants {

    //Delimiter used in CSV file
    String BAR_DELIMITER = "|";
    String NEW_LINE_SEPARATOR = "\n";
    String DOUBLE_QUOTE = "\"";

    //CSV file header

            String FILE_HEADER = DOUBLE_QUOTE + "Name" + DOUBLE_QUOTE + "|"  + DOUBLE_QUOTE + "Mobile Phone"  + DOUBLE_QUOTE + "|"
            + DOUBLE_QUOTE + "Date Created"  + DOUBLE_QUOTE + "|"  + DOUBLE_QUOTE + "Last Update"  + DOUBLE_QUOTE + "|"  + DOUBLE_QUOTE + "Vehicle Reg No"  + DOUBLE_QUOTE +
            "|"  + DOUBLE_QUOTE + "Email"  + DOUBLE_QUOTE + "|" + DOUBLE_QUOTE + "Address1"  + DOUBLE_QUOTE + "|"  + DOUBLE_QUOTE +
            "Address2"  + DOUBLE_QUOTE + "|"  + DOUBLE_QUOTE + "Address3"  + DOUBLE_QUOTE +"|" + DOUBLE_QUOTE + "City"  + DOUBLE_QUOTE +
            "|"  + DOUBLE_QUOTE + "Postcode"  + DOUBLE_QUOTE + "|"  + DOUBLE_QUOTE + "State"  + DOUBLE_QUOTE +
            "|"  + DOUBLE_QUOTE + "Country"  + DOUBLE_QUOTE + "|"  + DOUBLE_QUOTE + "Branch Code"  + DOUBLE_QUOTE +"|"  + DOUBLE_QUOTE + "Dealer Code"  + DOUBLE_QUOTE +
            "|"  + DOUBLE_QUOTE + "Customer PKID"  + DOUBLE_QUOTE;
}
