package app;

import app.constants.CsvConstants;
import app.constants.LimtayarSalesReturnCsvHeader;

public class WriteDtoToCsv {

    public  String getDtoInCsv(LimtayarSalesReturnCsvHeader srDtoMapToCsv) {


        String stringSRs = "";

        String srPkid="";
        String invPkid="";
        String txnDate="";
        String customerPkid="";
        String customerName="";
        String branchCode="";
        String invVehicleRegNo="";
        String itemCode="";
        String itemName="";
        String itemDesc="";
        String quantity="";
        String amountStd="";
        String docCcy="";
        String amountNet="0.00";
       // String amountDiscount="0.00";
        String amountTxn="0.00";
        String uom="";
        String gstAmount="0.00";
        String gstCode="";
        String gstRate="0.00";
        String gstType="";


        if(srDtoMapToCsv.getSrPkid()!=null) {
            srPkid = srDtoMapToCsv.getSrPkid();
        }

        if(srDtoMapToCsv.getInvPkid()!=null) {
            invPkid = srDtoMapToCsv.getInvPkid();
        }

        if(srDtoMapToCsv.getTxnDate()!=null) {
            txnDate = srDtoMapToCsv.getTxnDate();
        }
        if(srDtoMapToCsv.getCustomerPkid()!=null) {
            customerPkid = srDtoMapToCsv.getCustomerPkid();
        }
        if(srDtoMapToCsv.getCustomerName()!=null) {
            customerName = srDtoMapToCsv.getCustomerName();
        }
        if(srDtoMapToCsv.getBranchCode()!=null) {
            branchCode = srDtoMapToCsv.getBranchCode();
        }
        if(srDtoMapToCsv.getInvVehicleRegNo()!=null) {
            invVehicleRegNo = srDtoMapToCsv.getInvVehicleRegNo();
        }
        if(srDtoMapToCsv.getItemCode()!=null) {
            itemCode = srDtoMapToCsv.getItemCode();
        }
        if(srDtoMapToCsv.getItemName()!=null) {
            itemName = srDtoMapToCsv.getItemName();
        }
        if(srDtoMapToCsv.getItemDesc()!=null) {
            itemDesc = srDtoMapToCsv.getItemDesc();
        }
        if(srDtoMapToCsv.getQuantity()!=null) {
            quantity = srDtoMapToCsv.getQuantity();
        }
        if(srDtoMapToCsv.getAmountStd()!=null) {
            amountStd = srDtoMapToCsv.getAmountStd();
        }
        if(srDtoMapToCsv.getDocCcy()!=null) {
            docCcy = srDtoMapToCsv.getDocCcy();
        }
        if(srDtoMapToCsv.getAmountNet()!=null) {
            amountNet = srDtoMapToCsv.getAmountNet();
        }
//        if(srDtoMapToCsv.getAmountDiscount()!=null) {
//            amountDiscount = srDtoMapToCsv.getAmountDiscount();
        //}
        if(srDtoMapToCsv.getAmountTxn()!=null) {
            amountTxn = srDtoMapToCsv.getAmountTxn();
        }
        if(srDtoMapToCsv.getUom()!=null) {
            uom = srDtoMapToCsv.getUom();
        }
        if(srDtoMapToCsv.getGstAmount()!=null) {
            gstAmount = srDtoMapToCsv.getGstAmount();
        }
        if(srDtoMapToCsv.getGstCode()!=null) {
            gstCode = srDtoMapToCsv.getGstCode();
        }
        if(srDtoMapToCsv.getGstRate()!=null) {
            gstRate = srDtoMapToCsv.getGstRate();
        }
        if(srDtoMapToCsv.getGstType()!=null) {
            gstType = srDtoMapToCsv.getGstType();
        }




        stringSRs = srPkid   + CsvConstants.BAR_DELIMITER +
                invPkid + CsvConstants.BAR_DELIMITER +
                txnDate +  CsvConstants.BAR_DELIMITER +
                customerPkid +  CsvConstants.BAR_DELIMITER +
                customerName +  CsvConstants.BAR_DELIMITER +
                branchCode +  CsvConstants.BAR_DELIMITER +
                invVehicleRegNo + CsvConstants.BAR_DELIMITER +
                itemCode + CsvConstants.BAR_DELIMITER +
                 itemName +  CsvConstants.BAR_DELIMITER +
                 itemDesc +  CsvConstants.BAR_DELIMITER +
                 quantity +  CsvConstants.BAR_DELIMITER +
                 amountStd +  CsvConstants.BAR_DELIMITER +
                 docCcy + CsvConstants.BAR_DELIMITER +
                 amountNet +  CsvConstants.BAR_DELIMITER +
                 amountTxn + CsvConstants.BAR_DELIMITER +
                 uom +  CsvConstants.BAR_DELIMITER +
                gstAmount +  CsvConstants.BAR_DELIMITER +
                gstCode +  CsvConstants.BAR_DELIMITER +
                gstRate +  CsvConstants.BAR_DELIMITER +
                gstType +  CsvConstants.BAR_DELIMITER +
                CsvConstants.NEW_LINE_SEPARATOR;

        return stringSRs;

    }
}
