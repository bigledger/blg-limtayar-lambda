package app;

public class TransformSalesReturnToCsvResponse {

    private String hello;

    public TransformSalesReturnToCsvResponse(String hello) {
        this.hello = hello;
    }

    public TransformSalesReturnToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
