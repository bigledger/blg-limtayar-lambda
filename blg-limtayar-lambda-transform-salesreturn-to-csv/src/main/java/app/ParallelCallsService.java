package app;

import app.constants.LimtayarSalesReturnCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.ISalesReturnService;
import com.bigledger.core1.adapter.integration.services.SalesReturnServiceImp;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ParallelCallsService {
    ISalesReturnService salesReturnService = new SalesReturnServiceImp();
        TransformDtoToCsv transformDtoToCsv = new TransformDtoToCsv();
        TransformItemToDto transformItemToDto = new TransformItemToDto();
        WriteDtoToCsv writeDtoToCsv = new WriteDtoToCsv();

        public List<ApiResponse> getSalesReturnToDos(List<String> ids, RequestHeaders requestHeaders){

            List<CompletableFuture<ApiResponse>> futures =
                    ids.stream()
                            .map(id -> getSalesReturnToDoAsync(id, requestHeaders))
                            .collect(Collectors.toList());

            List<ApiResponse> result =
                    futures.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList());

            return result;
        }
        CompletableFuture<ApiResponse> getSalesReturnToDoAsync(String id, RequestHeaders requestHeaders){

            CompletableFuture<ApiResponse> future = CompletableFuture.supplyAsync(new Supplier<ApiResponse>() {
                @Override
                public ApiResponse get() {

                     ApiResponse response = null;

                    try {

                          response = salesReturnService.getSalesReturnByGuid(id, requestHeaders);

                    }
                    catch (AkaunApiServerException e) {
                        System.out.println("AkaunApiServerException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;

                    } catch (NetworkException e) {
                        System.out.println("NetworkException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                    return response;
                }
            });

            return future;
        }

    public List<List<LimtayarSalesReturnCsvHeader>> getTransformDtoToCsvToDos(List<GenericDocHdrDto> genericDocHdrDtos, RequestHeaders requestHeaders){

        List<CompletableFuture<List<LimtayarSalesReturnCsvHeader>>> futures =
                genericDocHdrDtos.stream()
                        .map(genericDocHdrDto -> getTransformDtoToCsvToDoAsync(genericDocHdrDto, requestHeaders))
                        .collect(Collectors.toList());

        List<List<LimtayarSalesReturnCsvHeader>> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<List<LimtayarSalesReturnCsvHeader>> getTransformDtoToCsvToDoAsync(GenericDocHdrDto genericDocHdrDto, RequestHeaders requestHeaders){

        CompletableFuture<List<LimtayarSalesReturnCsvHeader>> future = CompletableFuture.supplyAsync(new Supplier<List<LimtayarSalesReturnCsvHeader>>() {
            @Override
            public List<LimtayarSalesReturnCsvHeader> get() {

                List<LimtayarSalesReturnCsvHeader> response = null;

                response = transformDtoToCsv.getSalesReturn(genericDocHdrDto, requestHeaders);

                return response;
                }

        });

        return future;
    }


    public List<LimtayarSalesReturnCsvHeader> getTransformDtoItemToCsvToDos(List<GenericDocLineDto> genericDocLineDtos, LimtayarSalesReturnCsvHeader srDtoToCsvModel, RequestHeaders requestHeaders)
    {

        List<CompletableFuture<LimtayarSalesReturnCsvHeader>> futures =
                genericDocLineDtos.stream()
                        .map(genericDocLineDto -> getTransformDtoItemToCsvToDoAsync(genericDocLineDto, srDtoToCsvModel, requestHeaders))
                        .collect(Collectors.toList());

        List<LimtayarSalesReturnCsvHeader> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<LimtayarSalesReturnCsvHeader> getTransformDtoItemToCsvToDoAsync(GenericDocLineDto genericDocLineDto,LimtayarSalesReturnCsvHeader srDtoToCsvModel, RequestHeaders requestHeaders)
    {

        CompletableFuture<LimtayarSalesReturnCsvHeader> future = CompletableFuture.supplyAsync(new Supplier<LimtayarSalesReturnCsvHeader>() {
            @Override
            public LimtayarSalesReturnCsvHeader get() {

                LimtayarSalesReturnCsvHeader response = null;

                response = transformItemToDto.getSalesReturnItem(genericDocLineDto,srDtoToCsvModel,requestHeaders);

                return response;
            }

        });

        return future;
    }


    public List<String> getWriteDtoToCsvToDos(List<LimtayarSalesReturnCsvHeader> srDtoMapToCsvs){

        List<CompletableFuture<String>> futures =
                srDtoMapToCsvs.stream()
                        .map(srDtoMapToCsv -> getWriteDtoToCsvToDoAsync(srDtoMapToCsv))
                        .collect(Collectors.toList());

        List<String> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<String> getWriteDtoToCsvToDoAsync(LimtayarSalesReturnCsvHeader srDtoMapToCsv){

        CompletableFuture<String> future = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {

                String response = null;

                response = writeDtoToCsv.getDtoInCsv(srDtoMapToCsv);

                return response;
            }

        });

        return future;
    }


}
