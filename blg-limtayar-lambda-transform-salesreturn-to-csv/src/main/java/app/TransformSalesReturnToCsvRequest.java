package app;
public class TransformSalesReturnToCsvRequest {

    private String input;

    public TransformSalesReturnToCsvRequest(String input) {
        this.input = input;
    }

    public TransformSalesReturnToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
