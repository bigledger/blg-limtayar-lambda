package app;

import app.constants.LimtayarSalesReturnCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.IItemService;
import com.bigledger.core1.adapter.integration.services.ISalesReturnService;
import com.bigledger.core1.adapter.integration.services.ItemServiceImpl;
import com.bigledger.core1.adapter.integration.services.SalesReturnServiceImp;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.GenericDocLineDto;
import com.bigledger.core1.dto.ItemHeaderDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;

import java.text.DecimalFormat;

public class TransformItemToDto {

            public LimtayarSalesReturnCsvHeader getSalesReturnItem(GenericDocLineDto genericDocLineDto, LimtayarSalesReturnCsvHeader srDtoToCsvModel, RequestHeaders requestHeaders) {


                LimtayarSalesReturnCsvHeader srDtoToCsvModelLine = new LimtayarSalesReturnCsvHeader();
                IItemService itemService = new ItemServiceImpl();
                EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
                DecimalFormat df2 = new DecimalFormat("0.00");

                srDtoToCsvModelLine.setSrPkid(srDtoToCsvModel.getSrPkid());
                srDtoToCsvModelLine.setInvPkid(srDtoToCsvModel.getInvPkid());
                srDtoToCsvModelLine.setTxnDate(srDtoToCsvModel.getTxnDate());
                srDtoToCsvModelLine.setCustomerPkid(srDtoToCsvModel.getCustomerPkid());

                srDtoToCsvModelLine.setCustomerName(srDtoToCsvModel.getCustomerName());
                srDtoToCsvModelLine.setBranchCode(srDtoToCsvModel.getBranchCode());
                System.out.println("Customer Name:"+ srDtoToCsvModel.getCustomerName());
                srDtoToCsvModelLine.setInvVehicleRegNo(srDtoToCsvModel.getInvVehicleRegNo());
                srDtoToCsvModelLine.setItemCode(genericDocLineDto.getItemCode());
                srDtoToCsvModelLine.setItemName(genericDocLineDto.getItemName());
//                String itemName1 = genericDocLineDto.getItemName();
//                String str = itemName1.replaceAll("\"", "");
                srDtoToCsvModelLine.setItemDesc(genericDocLineDto.getItemDesc());
                srDtoToCsvModelLine.setDocCcy(genericDocLineDto.getDocCcy());


                // get UOM


                try {
                    // check if item exist in BLG
                    ApiResponse response = itemService.getItemHeaderDtoByGuid(genericDocLineDto.getItemGuid(), requestHeaders);


                    JSONParser parser = new JSONParser();
                    JSONObject json = (JSONObject) parser.parse(response.asJson());
//                    System.out.println("Item Data as Json: " + json.get("data"));

                    ObjectMapper mapper = new ObjectMapper();
                    mapper.registerModule(new JavaTimeModule());
                    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

                    ItemHeaderDto itemHdrDto = mapper.readValue(json.get("data").toString(), ItemHeaderDto.class);

                    srDtoToCsvModelLine.setUom(itemHdrDto.getUom());
                    System.out.println("Item Data as Json:done");

                } catch (
    AkaunApiServerException e) {
        System.out.println(e.getMessage());
        etlSendDataResponse.setEventCode("FAILED");
        etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
        etlSendDataResponse.setError(e.getMessage());
        return null;
    } catch (
    NetworkException e) {
        System.out.println(e.getMessage());
        etlSendDataResponse.setEventCode("RETRY");
        etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
        etlSendDataResponse.setError(e.getMessage());
        return null;
    } catch (Exception e) {
        System.out.println(e.getMessage());

        System.out.println(e.getClass().getCanonicalName());
        etlSendDataResponse.setEventCode("FAILED");
        etlSendDataResponse.setError(e.getMessage());
        return null;
    }
        double quantity =  genericDocLineDto.getQuantityBase().doubleValue();

        String strQuantity = df2.format(quantity);
                System.out.println("Item quantity as Json: " + strQuantity);

                double listPrice = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
        String unitAmountStd = df2.format(listPrice);
                System.out.println("Item amountstd as Json: " + unitAmountStd);


                double NettPrice = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
        String netPrice = df2.format(NettPrice);
                System.out.println("Item netprice as Json: " + netPrice);


                double gstRate =  genericDocLineDto.getTaxGstRate().doubleValue();
        String strGstRate = df2.format(gstRate);
                System.out.println("Item rate as Json: " + strGstRate);

                double gstAmount = genericDocLineDto.getAmountTaxGst().doubleValue();
        String strGstAmount = df2.format(gstAmount);
                System.out.println("Item gst as Json: " + strGstAmount);


                double totalAmount = genericDocLineDto.getAmountTxn().doubleValue();
                 String strAmountTxn = df2.format (totalAmount);
                System.out.println("Item amounttxn as Json: " + strAmountTxn);


//                double amountDiscount =  genericDocLineDto.getAmountDiscount().doubleValue();
//                String strAmntDisc = df2.format(amountDiscount);
//                System.out.println("Item discount as Json: " + strAmntDisc);

                srDtoToCsvModelLine.setQuantity(strQuantity);
                System.out.println("Item quantity as Json: " + strQuantity);

                srDtoToCsvModelLine.setGstRate(strGstRate);
                System.out.println("Item rate as Json: " + strGstRate);

                srDtoToCsvModelLine.setAmountTxn(strAmountTxn);
                System.out.println("Item amounttxn as Json: " + strAmountTxn);

                srDtoToCsvModelLine.setGstAmount(strGstAmount);
                System.out.println("Item gst as Json: " + strGstAmount);

                srDtoToCsvModelLine.setAmountNet(netPrice);
                System.out.println("Item netprice as Json: " + netPrice);

                srDtoToCsvModelLine.setAmountStd(unitAmountStd);
                System.out.println("Item amountstd as Json: " + unitAmountStd);

//        srDtoToCsvModelLine.setSeqNo(srDtoToCsvModel.getSeqNo());
//                srDtoToCsvModelLine.setAmountDiscount(strAmntDisc);
//                System.out.println("Item discount as Json: " + strAmntDisc);


                srDtoToCsvModelLine.setGstCode(genericDocLineDto.getTaxGstCode());
                System.out.println("Item code as Json: " + genericDocLineDto.getTaxGstCode());

                srDtoToCsvModelLine.setGstType(genericDocLineDto.getTaxGstType());
                System.out.println("Item code as Json: " + genericDocLineDto.getTaxGstType());



                return srDtoToCsvModelLine;
    }
}
