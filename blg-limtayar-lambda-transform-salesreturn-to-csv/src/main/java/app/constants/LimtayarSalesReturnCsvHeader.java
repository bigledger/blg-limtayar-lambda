package app.constants;

import java.math.BigDecimal;

public class LimtayarSalesReturnCsvHeader {

    private String srPkid;
    private String invPkid;
    private String txnDate;
    private String customerPkid;
    private String customerName;
    private String branchCode;
    private String invVehicleRegNo;
    private String itemCode;
    private String itemName;
    private String itemDesc;
    private String quantity;
    private String amountStd;
    private String docCcy;
    private String amountNet;
   // private String amountDiscount;
    private String amountTxn;
    private String uom;
    private String gstAmount;
    private String gstCode;
    private String gstRate;
    private String gstType;

    public String getCustomerPkid() { return customerPkid; }

    public void setCustomerPkid(String customerPkid) { this.customerPkid = customerPkid; }

    public String getBranchCode() { return branchCode; }

    public void setBranchCode(String branchCode) { this.branchCode = branchCode; }

    public String getSrPkid() {
        return srPkid;
    }

    public void setSrPkid(String srPkid) {
        this.srPkid = srPkid;
    }

    public String getInvPkid() {
        return invPkid;
    }

    public void setInvPkid(String invPkid) {
        this.invPkid = invPkid;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getInvVehicleRegNo() {
        return invVehicleRegNo;
    }

    public void setInvVehicleRegNo(String invVehicleRegNo) {
        this.invVehicleRegNo = invVehicleRegNo;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmountStd() {
        return amountStd;
    }

    public void setAmountStd(String amountStd) {
        this.amountStd = amountStd;
    }

    public String getDocCcy() {
        return docCcy;
    }

    public void setDocCcy(String docCcy) {
        this.docCcy = docCcy;
    }

    public String getAmountNet() {
        return amountNet;
    }

    public void setAmountNet(String amountNet) {
        this.amountNet = amountNet;
    }

//    public String getAmountDiscount() {
//        return amountDiscount;
//    }
//
//    public void setAmountDiscount(String amountDiscount) {
//        this.amountDiscount = amountDiscount;
//    }

    public String getAmountTxn() {
        return amountTxn;
    }

    public void setAmountTxn(String TotalAmount) {
        this.amountTxn = amountTxn;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getGstAmount() {
        return gstAmount;
    }

    public void setGstAmount(String amountTaxGst) {
        this.gstAmount = amountTaxGst;
    }

    public String getGstCode() {
        return gstCode;
    }

    public void setGstCode(String gstCode) {
        this.gstCode = gstCode;
    }

    public String getGstRate() {
        return gstRate;
    }

    public void setGstRate(String gstRate) {
        this.gstRate = gstRate;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

}
