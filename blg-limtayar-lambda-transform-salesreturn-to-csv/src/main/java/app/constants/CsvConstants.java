package app.constants;

public interface CsvConstants {

    //Delimiter used in CSV file
    String BAR_DELIMITER = "|";
    String NEW_LINE_SEPARATOR = "\n";
    //String DOUBLE_QUOTE = "\"";

    //CSV file header

            String FILE_HEADER =  "Sales Return PKID" +  "|"  + "Invoice PKID" +
            "|"  +"Transaction Date"  + "|" + "Customer Pkid" + "|"  + "Customer Name"  + "|" + "Branch Code" +  "|" +
            "|"  + "Invoice Vehicle Regnum"  + "|"  + "Item Code"  +
            "|"  + "Item Name" +  "|"  + "Item Description"  +
            "|"  + "Quantity"  +
            "|"  + "Unit Amount STD"  + "|"  + "Currency"   +  "|"  +
            "|"  + "Nett Price(after discount exclude tax)" + "|"   + "Amount Txn" +
            "|"  + "UOM"  + "|"   + "GST Tax Amount"  +  "|" + "GST Code"  +
            "|"  + "GST Rate"  + "|"   + "GST Type"   + "|" ;
}


