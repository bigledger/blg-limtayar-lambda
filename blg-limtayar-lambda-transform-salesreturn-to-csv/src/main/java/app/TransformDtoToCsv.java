package app;

import app.constants.LimtayarSalesReturnCsvHeader;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.dto.CustomFieldDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

public class TransformDtoToCsv {

    public  List<LimtayarSalesReturnCsvHeader> getSalesReturn(GenericDocHdrDto genericDocHdrDto, RequestHeaders requestHeaders) {

        List<LimtayarSalesReturnCsvHeader> srDtoInCsv = new ArrayList<LimtayarSalesReturnCsvHeader>();
        EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
        ZoneId zoneId = ZoneId.of("Asia/Kuala_Lumpur");        //Zone information


        ZonedDateTime zdtAtMalaysia = genericDocHdrDto.getDateTxn().withZoneSameInstant(zoneId);
        LimtayarSalesReturnCsvHeader srDtoToCsvModel = new LimtayarSalesReturnCsvHeader();


        // System.out.println("Zone date time: " + genericDocHdrDto.get(i).getDateTxn().toLocalDateTime());
        // System.out.println("Local date time: " + zdtAtMalaysia);

        DecimalFormat nft = new DecimalFormat("#00.###");
        DecimalFormat df2 = new DecimalFormat("#.##");
        nft.setDecimalSeparatorAlwaysShown(false);


        String yyyy = Integer.toString(zdtAtMalaysia.getYear());
        String mm = nft.format(zdtAtMalaysia.getMonthValue());
        String dd = nft.format(zdtAtMalaysia.getDayOfMonth());
        String hh = nft.format(zdtAtMalaysia.getHour());
        String min = nft.format(zdtAtMalaysia.getMinute());
        String Date = yyyy + mm + dd + hh + min;

        //Displaying current time in 12 hour format with AM/PM
        DateFormat outputFormat = new SimpleDateFormat("hh:mmaa");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.US);

        // String inputText = "2012-11-17T00:00:00.000-05:00";
        Date date = null;
        try {
            date = inputFormat.parse(zdtAtMalaysia.toString());
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println(e);
        }

        String orderTime = outputFormat.format(date);

        System.out.println("############################ orderTime: "+orderTime);
        System.out.println("############################### Date is: "+Date);

       //  convert data from SR Hdr DTO to CSV model

        srDtoToCsvModel.setTxnDate(Date);

        List<CustomFieldDto> customCriteriaFieldLists= genericDocHdrDto.getCustomFieldList();

        for (int j = 0; j < customCriteriaFieldLists.size(); j++) {
            CustomFieldDto customCriteriaFieldDto = customCriteriaFieldLists.get(j);

            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_SALES_RETURN_PKID")) {
                srDtoToCsvModel.setSrPkid(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_INVOICE_PKID")) {
                srDtoToCsvModel.setInvPkid(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("BLG_ENTITY_NAME")) {
                srDtoToCsvModel.setCustomerName(customCriteriaFieldDto.getValueString());
                System.out.println("customer name" + (customCriteriaFieldDto.getValueString()));
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_INVOICE_VEHICLE_REGNUM")) {
                srDtoToCsvModel.setInvVehicleRegNo(customCriteriaFieldDto.getValueString());
            }

            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ENTITY_KEY")) {
                srDtoToCsvModel.setCustomerPkid(customCriteriaFieldDto.getValueString());
                System.out.println("############################ EMP_ENTITY_KEY: "+ customCriteriaFieldDto.getValueString());
            }

            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_BRANCH_CODE")) {
                srDtoToCsvModel.setBranchCode(customCriteriaFieldDto.getValueString());
                System.out.println("############################ EMP_BRANCH_CODE: "+ customCriteriaFieldDto.getValueString());
            }
        }

        // convert data from generic Doc Line DTO to CSV model
        List<GenericDocLineDto> genericDocLineLists = genericDocHdrDto.getGenericDocLineDtoList();


        // do a parallel call to get all items
        System.out.println("ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService()");
        ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService();

        System.out.println("srDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(genericDocLineLists,srDtoToCsvModel,requestHeaders)");
        srDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(genericDocLineLists,srDtoToCsvModel,requestHeaders);

        System.out.println("List<LimtayarSalesReturnCsvHeader> orderedPOLineList = new ArrayList<LimtayarSalesReturnCsvHeader>()");
        List<LimtayarSalesReturnCsvHeader> SRLineList = new ArrayList<LimtayarSalesReturnCsvHeader>();


        System.out.println("TreeMap <String, LimtayarSalesReturnCsvHeader> poLineMap = new TreeMap < String, LimtayarSalesReturnCsvHeader>()");
        TreeMap <String, LimtayarSalesReturnCsvHeader> srLineMap = new TreeMap < String, LimtayarSalesReturnCsvHeader>();


        System.out.println("for (int i = 0; i < srDtoInCsv.size(); i++)");
        for (int i = 0; i < srDtoInCsv.size(); i++)
        {
            LimtayarSalesReturnCsvHeader srLineCsv = srDtoInCsv.get(i);
            srLineMap.put("srHeader-"+i, srLineCsv);

        }
        System.out.println("for(Map.Entry<String,LimtayarSalesReturnCsvHeader> entry : srLineMap.entrySet())");
        for(Map.Entry<String,LimtayarSalesReturnCsvHeader> entry : srLineMap.entrySet()) {
            String key = entry.getKey();
            LimtayarSalesReturnCsvHeader value = entry.getValue();
            SRLineList.add (value);
            //System.out.println(key + " => " + value);
        }

//
//        // convert data from generic Doc Line DTO to CSV model
//        List<GenericDocLineDto> genericDocLineLists = genericDocHdrDto.getGenericDocLineDtoList();


//        // do a parallel call to get all items
//        System.out.println("ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService()");
//        ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService();
//
//        System.out.println("poDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(genericDocLineLists,poDtoToCsvModel,requestHeaders)");
//         poDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(genericDocLineLists,poDtoToCsvModel,requestHeaders);
//
//        System.out.println("List<SenhengPanaEdiPoCsvHeader> orderedPOLineList = new ArrayList<SenhengPanaEdiPoCsvHeader>()");
//        List<SenhengPanaEdiPoCsvHeader> orderedPOLineList = new ArrayList<SenhengPanaEdiPoCsvHeader>();
//
//
//        System.out.println("TreeMap <String, SenhengPanaEdiPoCsvHeader> poLineMap = new TreeMap < String, SenhengPanaEdiPoCsvHeader>()");
//        TreeMap <String, SenhengPanaEdiPoCsvHeader> poLineMap = new TreeMap < String, SenhengPanaEdiPoCsvHeader>();
//
//
//        System.out.println("for (int i = 0; i < poDtoInCsv.size(); i++)");
//        for (int i = 0; i < poDtoInCsv.size(); i++)
//        {
//            LimtayarCustCsvHeader poLineCsv = poDtoInCsv.get(i);
//            poLineMap.put(poLineCsv.getSeqNo(), poLineCsv);
//
//        }
//        System.out.println("for(Map.Entry<String,SenhengPanaEdiPoCsvHeader> entry : poLineMap.entrySet())");
//        for(Map.Entry<String,SenhengPanaEdiPoCsvHeader> entry : poLineMap.entrySet()) {
//            String key = entry.getKey();
//            SenhengPanaEdiPoCsvHeader value = entry.getValue();
//            orderedPOLineList.add (value);
//            //System.out.println(key + " => " + value);
//        }

//            for (int k = 0; k < genericDocLineLists.size(); k++) {
//                GenericDocLineDto genericDocLineDto = genericDocLineLists.get(k);
//                SenhengPanaEdiPoCsvHeader poDtoToCsvModelLine = new SenhengPanaEdiPoCsvHeader();
//
//                poDtoToCsvModelLine.setLineType("D");
//                poDtoToCsvModelLine.setpNumber(poDtoToCsvModel.getpoNumber());
//                poDtoToCsvModelLine.setOrderDate(poDtoToCsvModel.getOrderDate());
//                poDtoToCsvModelLine.setOrderTime(poDtoToCsvModel.getOrderTime());
//
//                poDtoToCsvModelLine.setSupplierNo(poDtoToCsvModel.getSupplierNo());
//                poDtoToCsvModelLine.setDeliveryToStoreCode(poDtoToCsvModel.getDeliveryToStoreCode());
//                poDtoToCsvModelLine.setDeliveryToStoreName(poDtoToCsvModel.getDeliveryToStoreName());
//                poDtoToCsvModelLine.setOrderedBy(poDtoToCsvModel.getOrderedBy());
//                poDtoToCsvModelLine.setIssuedBy(poDtoToCsvModel.getIssuedBy());
//                poDtoToCsvModelLine.setExpiryDate(poDtoToCsvModel.getExpiryDate());
//                poDtoToCsvModelLine.setSupplier(poDtoToCsvModel.getSupplier());
//                poDtoToCsvModelLine.setRemark1(poDtoToCsvModel.getRemark1());
//                poDtoToCsvModelLine.setRemark2(poDtoToCsvModel.getRemark2());
//                poDtoToCsvModelLine.setRemark3(poDtoToCsvModel.getRemark3());
//                poDtoToCsvModelLine.setRemark4(poDtoToCsvModel.getRemark4());
//                poDtoToCsvModelLine.setRemark5(poDtoToCsvModel.getRemark5());
//
//                // get UOM
//
//
//                try {
//                    // check if item exist in BLG
//                    ApiResponse response = itemService.getItemHeaderDtoByGuid(genericDocLineDto.getItemGuid(), requestHeaders);
//
//
//                    JSONParser parser = new JSONParser();
//                    JSONObject json = (JSONObject) parser.parse(response.asJson());
////                    System.out.println("Item Data as Json: " + json.get("data"));
//
//                    ObjectMapper mapper = new ObjectMapper();
//                    mapper.registerModule(new JavaTimeModule());
//                    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//                    ItemHeaderDto itemHdrDto = mapper.readValue(json.get("data").toString(), ItemHeaderDto.class);
//
//                    poDtoToCsvModelLine.setUom(itemHdrDto.getUom());
//
//                } catch (AkaunApiServerException e) {
//                    System.out.println(e.getMessage());
//                    etlSendDataResponse.setEventCode("FAILED");
//                    etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//                    etlSendDataResponse.setError(e.getMessage());
//                    return null;
//                } catch (NetworkException e) {
//                    System.out.println(e.getMessage());
//                    etlSendDataResponse.setEventCode("RETRY");
//                    etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//                    etlSendDataResponse.setError(e.getMessage());
//                    return null;
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//
//                    System.out.println(e.getClass().getCanonicalName());
//                    etlSendDataResponse.setEventCode("FAILED");
//                    etlSendDataResponse.setError(e.getMessage());
//                    return null;
//                }
//
//                double invoicePrice =  genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//                String invPrice = df2.format(invoicePrice);
//
//                double NettPrice =  genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//                String netPrice = df2.format(NettPrice);
//
//                poDtoToCsvModelLine.setPoType("0");
//
//                poDtoToCsvModelLine.setSeqNo(genericDocLineDto.getPositionId());
//                poDtoToCsvModelLine.setItemNo(genericDocLineDto.getItemCode());
//                poDtoToCsvModelLine.setItemBarcode(genericDocLineDto.getItemMachineCode());
//                poDtoToCsvModelLine.setItemDescription(genericDocLineDto.getItemName());
//                poDtoToCsvModelLine.setOrderQuantity(genericDocLineDto.getQuantityBase().doubleValue());
//                poDtoToCsvModelLine.setInvoicePrice(Double.valueOf(invPrice));
//                poDtoToCsvModelLine.setNettPrice(Double.valueOf(netPrice));
//                poDtoToCsvModelLine.setTotalCostNoGst(genericDocLineDto.getAmountNet().doubleValue());
//                poDtoToCsvModelLine.setGstCode(genericDocLineDto.getTaxGstCode());
//                poDtoToCsvModelLine.setGstRate(Double.valueOf(genericDocLineDto.getTaxGstRate().toString()));
//                poDtoToCsvModelLine.setGstAmount(genericDocLineDto.getAmountTaxGst().doubleValue());
//                poDtoToCsvModelLine.setTotalCost(genericDocLineDto.getAmountTxn().doubleValue());
//
//                poDtoInCsv.add(k, poDtoToCsvModelLine);
//            }


//        return orderedPOLineList;
        return SRLineList;
    }
}
