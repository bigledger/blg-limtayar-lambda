package app;


import app.constants.CsvConstants;
import app.constants.LimtayarSalesReturnCsvHeader;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.DocumentEventServiceImpl;
import com.bigledger.core1.adapter.integration.services.IDocumentEventService;
import com.bigledger.core1.adapter.integration.services.ISubqueryService;
import com.bigledger.core1.adapter.integration.services.SubqueryServiceImpl;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.api.criteria.DocEventCriteria;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.GenericDocEventDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.id.PagingResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;


public class TransformSalesReturnToCsv implements RequestHandler<Map<String, String>, Map<String, String>> {

    @Override
    public Map<String, String> handleRequest(Map<String, String> pMap, Context context) {

        final long start = Calendar.getInstance().getTimeInMillis();
        LambdaLogger logger = context.getLogger();

        List<GenericDocHdrDto> genericDocHdrDto = new ArrayList<GenericDocHdrDto>();
        Map<String, String> subquery = new HashMap<String, String>();
        List<String> guids = new ArrayList<>();

        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        System.out.println("Input Map: " + pMap);
        System.out.println("Filename: " + pMap.get("s3Key"));
        System.out.println("Processing Bucket: " + pMap.get("s3BucketProcess"));

        RequestHeaders requestHeaders = new RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));


        //Query to get all Sales Return
//        String srSql = "SELECT e1.guid,e1.guid_doc_hdr AS neededguid FROM bl_fi_generic_doc_event AS e1 " +
//                " WHERE e1.event_code = 'SALES_RETURN_CREATED'" +
//                " AND (e1.guid not in (SELECT e2.link_guid FROM bl_fi_generic_doc_event AS e2 WHERE e2.event_code = 'CKL_SALES_RETURN_EXPORTED'))" +
//                " LIMIT " + pMap.get("limitExport") + ";";

        String srSql = "select e1.guid, e1.guid_doc_hdr as neededguid from bl_fi_generic_doc_event e1 "+
                " left join bl_fi_generic_doc_event as e2 " +
                " on e1.guid = e2.link_guid"+
                " where e2.guid is null and (e1.action = 'SALES_RETURN_CREATED') "+
                " LIMIT " + pMap.get("limitExport") + ";";

        subquery.put("subquery", srSql);

        System.out.println("Querying database for sales return");
        System.out.println(srSql);


        try {


            //  get all sales return
            ISubqueryService subqueryService = new SubqueryServiceImpl();

            ApiResponse responseSubquery = subqueryService.getGuidsBySubquery(subquery, requestHeaders);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseSubquery.asJson());
            System.out.println("GUIDs: " + json.get("data").toString());


            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            guids = mapper.readValue(json.get("data").toString(), ArrayList.class);

            // Use parallel call to get SR DTOs

            ParallelCallsService parallelCallSrDto = new ParallelCallsService();

            List<ApiResponse> responses = parallelCallSrDto.getSalesReturnToDos(guids,requestHeaders);

            System.out.println("Count Sales Return DTOs: " + responses.size());
            System.out.println("First Sales Return DTO: " + responses.get(0));

            for (int x = 0; x < responses.size(); x++){

                JSONParser parser1 = new JSONParser();
                JSONObject json1 = (JSONObject) parser1.parse(responses.get(x).asJson());

                ObjectMapper mapper1 = new ObjectMapper();
                mapper1.registerModule(new JavaTimeModule());
                mapper1.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

                GenericDocHdrDto genericSrHdrDto = mapper1.readValue(json1.get("data").toString(), GenericDocHdrDto.class);

                if (genericSrHdrDto.getCustomFieldList() != null) {
                    System.out.println("sr custom field : " + genericSrHdrDto.getCustomFieldList().size());
                } else {
                    System.out.println("No custom field");
                    return pMap;
                }


                if (genericSrHdrDto.getGenericDocLineDtoList() != null) {
                    System.out.println("po doc line  " + genericSrHdrDto.getGenericDocLineDtoList().size());
                } else {
                    System.out.println("No generic doc line");
                    return pMap;
                }

                genericDocHdrDto.add(genericSrHdrDto);
            }

            System.out.println("No. of Srs: " + genericDocHdrDto.size());


            // check if there is a sales return
            if (genericDocHdrDto.size() > 0) {

                // Use parallel call to get Sales Return DTOs transformed


                List<List<LimtayarSalesReturnCsvHeader>> limtayarSalesReturnCsvHeader = parallelCallSrDto.getTransformDtoToCsvToDos(genericDocHdrDto,requestHeaders);

                System.out.println("No of DTOs: " + limtayarSalesReturnCsvHeader.size());

                if (limtayarSalesReturnCsvHeader.size()>0){

                    List<String> dtoSrCsv = null;
                    String stringSrs = "";
                    //Delimiter used in CSV file
                    String BAR_DELIMITER = "|";
                    String NEW_LINE_SEPARATOR = "\n";
                    //String DOUBLE_QUOTE = "\"";

                    String FILE_HEADER ="Sales Return PKID" + "|" + "Invoice PKID"  + "|"
                           + "Transaction Date" + "|"  + "Customer Pkid" + "|"  + "Customer Name" + "|" +  "Branch Code" + "|" + "Invoice Vehicle Regnum" +
                            "|"  + "Item Code" + "|"  + "Item Name" + "|"  +
                            "Item Description"  + "|"  + "Quantity"  +"|"  + "Unit Amount STD"  +
                            "|"  + "Currency"  + "|"  + "Nett Price(after discount exclude tax)"  +
                            "|"  + "Amount Txn"  + "|"  + "UOM"  +
                            "|"  + "GST Tax Amount"  + "|"  +  "GST Code"  + "|"  + "GST Rate"  +
                            "|"  + "GST Type"  ;

                    //Write the CSV file header
                    stringSrs = stringSrs + FILE_HEADER;

                    //Add a new line separator after the header
                    stringSrs = stringSrs + CsvConstants.NEW_LINE_SEPARATOR;

                    for (int y=0; y< limtayarSalesReturnCsvHeader.size(); y++) {

                        dtoSrCsv = parallelCallSrDto.getWriteDtoToCsvToDos(limtayarSalesReturnCsvHeader.get(y));

                        for (int z=0; z < dtoSrCsv.size(); z++) {
                            stringSrs = stringSrs + dtoSrCsv.get(z);
                        }

                    }

                        // Write transformed DTO to processing s3
                        try {



                            System.out.println("writing CSV to s3Bucket:" + pMap.get("s3BucketProcess") + " s3Key:" + pMap.get("s3Key"));
                            System.out.println("Writing CSV");
                            String csvString = stringSrs;
                            //final String CONTENT_TYPE = "application/csv";
                            final String CONTENT_TYPE = "plain/text";
                            byte[] fileContentBytes = csvString.getBytes(StandardCharsets.UTF_8);
                            InputStream fileInputStream = new ByteArrayInputStream(fileContentBytes);
                            ObjectMetadata metadata = new ObjectMetadata();
                            metadata.setContentType(CONTENT_TYPE);
                            metadata.setContentLength(fileContentBytes.length);

                            PutObjectRequest putObjectRequest = new PutObjectRequest(
                                    pMap.get("s3BucketProcess"), pMap.get("s3Key"), fileInputStream, metadata);
                            s3Client.putObject(putObjectRequest);
                        } catch (SdkClientException e) {
                            e.printStackTrace();
                            System.out.println(e.getMessage());
                            return pMap;
                        } finally {
                            System.out.println("Exported SR in CSV to s3Bucket : " + " " + pMap.get("s3BucketProcess") + " s3Key : " + pMap.get("s3Key"));
                        }



                }

                // Write guids to temp json s3
                String s3Keytemp = "CKL SALES RETURN LISTING/"+"_"+guids.get(0)+".json";
                String stringGuids = "";


                try {


                    for(int q=0; q<guids.size(); q++){

                        stringGuids=guids.get(q) + CsvConstants.NEW_LINE_SEPARATOR + stringGuids;

                    }

                    System.out.println("writing CSV to s3Bucket:" + pMap.get("s3BucketTempJson") + " s3Key:" + s3Keytemp);
                    System.out.println("Writing CSV");
                    String csvString = stringGuids;
                    final String CONTENT_TYPE = "application/csv";
                    byte[] fileContentBytes = csvString.getBytes(StandardCharsets.UTF_8);
                    InputStream fileInputStream = new ByteArrayInputStream(fileContentBytes);
                    ObjectMetadata metadata = new ObjectMetadata();
                    metadata.setContentType(CONTENT_TYPE);
                    metadata.setContentLength(fileContentBytes.length);

                    PutObjectRequest putObjectRequest = new PutObjectRequest(
                            pMap.get("s3BucketTempJson"), s3Keytemp, fileInputStream, metadata);
                    s3Client.putObject(putObjectRequest);
                } catch (SdkClientException e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                    return pMap;
                } finally {
                    System.out.println("Exported SR in CSV to s3Bucket : " + " " + pMap.get("s3BucketTempJson") + " s3Key : " + s3Keytemp);

                }

                pMap.put("s3KeyGuids", s3Keytemp);


            }
            else {
                System.out.println("No sales return to export");
                return pMap;
            }


//            try
//            {
//                DocEventCriteria DocEventCriteria = new DocEventCriteria();
//                DocEventCriteria.setGuidDochdr(guids.get(0));
//                DocEventCriteria.setAction("SALES_RETURN_CREATED");
//                IDocumentEventService SreventService = new DocumentEventServiceImpl();
//
//                PagingResponse<GenericDocEventDto> SrEventResponse = SreventService.getGenericDocEventDtoByCriteriaProcess(DocEventCriteria, requestHeaders);
//                List<GenericDocEventDto> genericDocEventDtosList = SrEventResponse.getObjectList();
//
//                {
//                    GenericDocEventDto docEventDto_Srs = genericDocEventDtosList.get(0);
//
//                    docEventDto_Srs.setAction("SALES_RETURN_CREATED");
//                    genericDocEventDtosList.set(0, docEventDto_Srs);
//                    ZonedDateTime currentDate = ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur"));
//
//                    GenericDocEventDto docEventDto_Sr = new GenericDocEventDto();
//
//                    docEventDto_Sr.setStatus("ACTIVE");
//                    docEventDto_Sr.setTxnType("SYS_APPLET");
//                    docEventDto_Sr.setCreatedDate(currentDate);
//                    docEventDto_Sr.setAction("CKL_SALES_RETURN_EXPORTED");
//                    docEventDto_Sr.setEventCode("CKL_SALES_RETURN_EXPORTED");
//                    docEventDto_Sr.setLinkGuid(docEventDto_Srs.getGuid());
//                    docEventDto_Sr.setDateTxn(currentDate);
//                    docEventDto_Sr.setGuidDocHdr(guids.get(0));
//                    genericDocEventDtosList.add(docEventDto_Sr);
//
//
//                    ApiResponse response1 = SreventService.createDocumentEvent(docEventDto_Sr, requestHeaders);
//                    logger.log("response.getCode():" + response1.getCode());
//                    logger.log("response.getMessage():" + response1.getMessage());
//
//                }
//
//
//            }
//            catch (AkaunApiServerException e) {
//                System.out.println("AkaunApiServerException");
//                e.printStackTrace();
//                System.out.println(e.getMessage());
//
//            } catch (NetworkException e) {
//                System.out.println("NetworkException");
//                e.printStackTrace();
//                System.out.println(e.getMessage());
//            } catch (Exception ex) {
//                System.out.println(ex);
//            }


        } catch (AkaunApiServerException e) {
            System.out.println("AkaunApiServerException");
            e.printStackTrace();
            System.out.println(e.getMessage());
            return pMap;

        } catch (NetworkException e) {
            System.out.println("NetworkException");
            e.printStackTrace();
            System.out.println(e.getMessage());
            return pMap;
        } catch (Exception ex) {
            System.out.println(ex);
        }

        final long end = Calendar.getInstance().getTimeInMillis();
        System.out.println(" export sr time spent: " + (end - start) / 1000 + "s");

        return pMap;

    }





}





