package app;

import app.constants.LimtayarCustCsvFields;
import com.bigledger.core1.dto.EntityDto;

import java.io.BufferedReader;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ParallelCallsService {

    TransformCsvToDto transformCsvToDto = new TransformCsvToDto();

    public List<LimtayarCustCsvFields> getCustToDos(BufferedReader br) {

        List<CompletableFuture<LimtayarCustCsvFields>> futures =
                br.lines()
                        .map(line -> getCustToDoAsync(line))
                        .collect(Collectors.toList());

        List<LimtayarCustCsvFields> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<LimtayarCustCsvFields> getCustToDoAsync(String line) {

        CompletableFuture<LimtayarCustCsvFields> future = CompletableFuture.supplyAsync(new Supplier<LimtayarCustCsvFields>() {

            @Override
            public LimtayarCustCsvFields get() {
                LimtayarCustCsvFields inputData = new LimtayarCustCsvFields();

                try {
                    // Columns used currently is the previous csv file got from customer,
                    // not sure whether it will be changed.
                    // TODO: After have the real csv file, please update the column
                    String[] col = line.split(",");

                    inputData.setWaveletInternalId(col[0]);
                    inputData.setMemberId(col[1]);
                    inputData.setCardNo(col[2]);
                    inputData.setJoinDate(col[3]);
                    inputData.setMembershipStatusCode(col[4]);
                    inputData.setSalutation(col[5]);
                    inputData.setName(col[6]);
                    inputData.setNric(col[7]);
                    inputData.setPassport(col[8]);
                    inputData.setGender(col[9]);
                    inputData.setMaritalStatus(col[10]);
                    inputData.setEmail(col[11]);
                    inputData.setDateOfBirth(col[12]);
                    inputData.setRace(col[13]);
                    inputData.setNationality(col[14]);
                    inputData.setBlock(col[15]);
                    inputData.setLevel(col[16]);
                    inputData.setUnit(col[17]);
                    inputData.setStreet(col[18]);
                    inputData.setBuilding(col[19]);
                    inputData.setPostalCode(col[20]);
                    inputData.setAddress1(col[21]);
                    inputData.setAddress2(col[22]);
                    inputData.setAddress3(col[23]);
                    inputData.setCity(col[24]);
                    inputData.setCountry(col[25]);
                    inputData.setContactNo(col[26]);
                    inputData.setMobileNo(col[27]);
                    inputData.setSmsNotification(col[28]);
                    inputData.setEmailNotication(col[29]);
                    inputData.setAddedOn(col[30]);
                    inputData.setAddedBy(col[31]);
                    inputData.setModifiedOn(col[32]);
                    inputData.setModifiedBy(col[33]);
                    inputData.setDeletedOn(col[34]);
                    inputData.setDeletedBy(col[35]);

                } catch (Exception e) {
                    System.out.println(e);
                }

                return inputData;
            }
        });

        return future;
    }

    public List<EntityDto> getCsvTransformDtoToDos(List<LimtayarCustCsvFields> inputDataList) {

        List<CompletableFuture<EntityDto>> futures =
                inputDataList.stream()
                        .map(inputData -> getCsvTransformDtoToDoAsync(inputData))
                        .collect(Collectors.toList());

        List<EntityDto> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<EntityDto> getCsvTransformDtoToDoAsync(LimtayarCustCsvFields inputData) {

        CompletableFuture<EntityDto> future = CompletableFuture.supplyAsync(new Supplier<EntityDto>() {

            @Override
            public EntityDto get() {
                EntityDto customer = new EntityDto();
                try {

                    customer = transformCsvToDto.getCust(inputData);

                } catch (Exception e) {
                    System.out.println(e);
                }

                return customer;
            }
        });

        return future;
    }

}
