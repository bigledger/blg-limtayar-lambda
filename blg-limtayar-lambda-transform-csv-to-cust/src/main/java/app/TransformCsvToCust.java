package app;


import app.constants.LimtayarCustCsvFields;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;


import java.io.*;
import java.util.*;

import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.dto.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;


public class TransformCsvToCust implements RequestHandler<Map<String, String>, Map<String, String>> {

    @Override
    public Map<String, String> handleRequest(Map<String, String> pMap, Context context) {

        final long start = Calendar.getInstance().getTimeInMillis();
        LambdaLogger logger = context.getLogger();

        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        System.out.println("Input Map: " + pMap);
        System.out.println("Filename: " + pMap.get("s3Key"));
        System.out.println("Processing Bucket: " + pMap.get("s3BucketProcess"));

        String bucketName = pMap.get("s3BucketName");
        String fileName = pMap.get("s3FileName");

        RequestHeaders requestHeaders = new RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));

        // get customer csv object from S3
        List<LimtayarCustCsvFields> inputDataList = new ArrayList<>();
        ParallelCallsService parallelCallsService = new ParallelCallsService();

        S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, fileName));
        InputStream objectData = object.getObjectContent();
        BufferedReader br = new BufferedReader(new InputStreamReader(objectData));

        inputDataList = parallelCallsService.getCustToDos(br);

        List<EntityDto> customerList = parallelCallsService.getCsvTransformDtoToDos(inputDataList);

        // Transform customerList into json String
        if (customerList.size() > 0) {
            ObjectMapper objectMapper = new ObjectMapper();
            JSONArray jsonArray = new JSONArray();

            for (EntityDto customer : customerList) {
                jsonArray.put(customer);
            }

            try {
                String jsonString = objectMapper.writeValueAsString(jsonArray);
                pMap.put("jsonCustArray", jsonString);
            } catch (JsonProcessingException e) {
                logger.log("JsonProcessingException: " + e.getMessage());
            } catch (Exception e) {
                logger.log("Exception: " + e.getMessage());
            }

        } else {
            logger.log("customerList is empty");
        }

        final long end = Calendar.getInstance().getTimeInMillis();
        System.out.println("transform cust csv time spent: " + (end - start) / 1000 + "s");

        return pMap;

    }

}





