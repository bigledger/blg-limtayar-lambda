package app;

import app.constants.LimtayarCustCsvFields;
import com.bigledger.core1.adapter.integration.utils.TimeZoneConversions;
import com.bigledger.core1.api.constants.ExtensionParamTypes;
import com.bigledger.core1.dto.CustomFieldDto;
import com.bigledger.core1.dto.EntityDto;
import com.bigledger.core1.util.StringUtil;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class TransformCsvToDto {

    public EntityDto getCust(LimtayarCustCsvFields inputData) {

        // Transform the csv input data into entity dto
        EntityDto customer = new EntityDto();

        //TODO: after confirm the latest format of the csv file provided, update the mapping
        customer.setGuid(StringUtil.getGuid());
        customer.setName(inputData.getName());

        List<CustomFieldDto> customFieldDtoList = new ArrayList<>();

        String pattern = "dd/mm/yyyy hh:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

        CustomFieldDto lastUpdateDate = new CustomFieldDto();
        lastUpdateDate.setHdrGuid(customer.getGuid());
        lastUpdateDate.setParamCode("EMP_CUSTOMER_LAST_EDIT");
        lastUpdateDate.setParamName("EMP_CUSTOMER_LAST_EDIT");
        lastUpdateDate.setParamType(ExtensionParamTypes.PARAM_TYPE_TIMESTAMP);
        LocalDateTime ldtLastEdit = LocalDateTime.from(formatter.parse(inputData.getModifiedOn()));
        Timestamp lastEdit = Timestamp.valueOf(ldtLastEdit);
        ZonedDateTime zdtLastEdit = TimeZoneConversions.getZonedDateTime(lastEdit, ZoneId.of("Asia/Kuala_Lumpur"));
        lastUpdateDate.setValueDate(zdtLastEdit);
        lastUpdateDate.setValueString("");
        lastUpdateDate.setStatus("ACTIVE");
        lastUpdateDate.setExtType("SYS_APPLET");
        customFieldDtoList.add(lastUpdateDate);

        CustomFieldDto creationDate = new CustomFieldDto();
        creationDate.setHdrGuid(customer.getGuid());
        creationDate.setParamCode("EMP_CUSTOMER_DATE_CREATED");
        creationDate.setParamName("EMP_CUSTOMER_DATE_CREATED");
        creationDate.setParamType(ExtensionParamTypes.PARAM_TYPE_TIMESTAMP);
        LocalDateTime ldtDateCreated = LocalDateTime.from(formatter.parse(inputData.getJoinDate()));
        Timestamp dateCreated = Timestamp.valueOf(ldtDateCreated);
        ZonedDateTime zdtDateCreated = TimeZoneConversions.getZonedDateTime(dateCreated, ZoneId.of("Asia/Kuala_Lumpur"));
        creationDate.setValueDate(zdtDateCreated);
        creationDate.setValueString("");
        creationDate.setStatus("ACTIVE");
        creationDate.setExtType("SYS_APPLET");
        customFieldDtoList.add(creationDate);

        CustomFieldDto mainAddress1 = new CustomFieldDto();
        mainAddress1.setHdrGuid(customer.getGuid());
        mainAddress1.setParamCode("EMP_CUSTOMER_ADDRESS1");
        mainAddress1.setParamName("EMP_CUSTOMER_ADDRESS1");
        mainAddress1.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mainAddress1.setValueString(inputData.getAddress1());
        mainAddress1.setStatus("ACTIVE");
        mainAddress1.setExtType("SYS_APPLET");
        customFieldDtoList.add(mainAddress1);

        CustomFieldDto mainAddress2 = new CustomFieldDto();
        mainAddress2.setHdrGuid(customer.getGuid());
        mainAddress2.setParamCode("EMP_CUSTOMER_ADDRESS2");
        mainAddress2.setParamName("EMP_CUSTOMER_ADDRESS2");
        mainAddress2.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mainAddress2.setValueString(inputData.getAddress2());
        mainAddress2.setStatus("ACTIVE");
        mainAddress2.setExtType("SYS_APPLET");
        customFieldDtoList.add(mainAddress2);

        CustomFieldDto mainAddress3 = new CustomFieldDto();
        mainAddress3.setHdrGuid(customer.getGuid());
        mainAddress3.setParamCode("EMP_CUSTOMER_ADDRESS3");
        mainAddress3.setParamName("EMP_CUSTOMER_ADDRESS3");
        mainAddress3.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mainAddress3.setValueString(inputData.getAddress3());
        mainAddress3.setStatus("ACTIVE");
        mainAddress3.setExtType("SYS_APPLET");
        customFieldDtoList.add(mainAddress3);

        CustomFieldDto mainCity = new CustomFieldDto();
        mainCity.setHdrGuid(customer.getGuid());
        mainCity.setParamCode("EMP_CUSTOMER_CITY");
        mainCity.setParamName("EMP_CUSTOMER_CITY");
        mainCity.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mainCity.setValueString(inputData.getCity());
        mainCity.setStatus("ACTIVE");
        mainCity.setExtType("SYS_APPLET");
        customFieldDtoList.add(mainCity);

        CustomFieldDto mainPostcode = new CustomFieldDto();
        mainPostcode.setHdrGuid(customer.getGuid());
        mainPostcode.setParamCode("EMP_CUSTOMER_POSTCODE");
        mainPostcode.setParamName("EMP_CUSTOMER_POSTCODE");
        mainPostcode.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mainPostcode.setValueString(inputData.getPostalCode());
        mainPostcode.setStatus("ACTIVE");
        mainPostcode.setExtType("SYS_APPLET");
        customFieldDtoList.add(mainPostcode);

        CustomFieldDto mainState = new CustomFieldDto();
        mainState.setHdrGuid(customer.getGuid());
        mainState.setParamCode("EMP_CUSTOMER_STATE");
        mainState.setParamName("EMP_CUSTOMER_STATE");
        mainState.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mainState.setValueString("");
        mainState.setStatus("ACTIVE");
        mainState.setExtType("SYS_APPLET");
        customFieldDtoList.add(mainState);

        CustomFieldDto mainCountry = new CustomFieldDto();
        mainCountry.setHdrGuid(customer.getGuid());
        mainCountry.setParamCode("EMP_CUSTOMER_COUNTRY");
        mainCountry.setParamName("EMP_CUSTOMER_COUNTRY");
        mainCountry.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mainCountry.setValueString(inputData.getCountry());
        mainCountry.setStatus("ACTIVE");
        mainCountry.setExtType("SYS_APPLET");
        customFieldDtoList.add(mainCountry);

        CustomFieldDto mobilePhone = new CustomFieldDto();
        mobilePhone.setHdrGuid(customer.getGuid());
        mobilePhone.setParamCode("EMP_CUSTOMER_MOBILE_PHONE");
        mobilePhone.setParamName("EMP_CUSTOMER_MOBILE_PHONE");
        mobilePhone.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        mobilePhone.setValueString(inputData.getMobileNo());
        mobilePhone.setStatus("ACTIVE");
        mobilePhone.setExtType("SYS_APPLET");
        customFieldDtoList.add(mobilePhone);

        CustomFieldDto email1 = new CustomFieldDto();
        email1.setHdrGuid(customer.getGuid());
        email1.setParamCode("EMP_CUSTOMER_EMAIL1");
        email1.setParamName("EMP_CUSTOMER_EMAIL1");
        email1.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        email1.setValueString(inputData.getEmail());
        email1.setStatus("ACTIVE");
        email1.setExtType("SYS_APPLET");
        customFieldDtoList.add(email1);

        CustomFieldDto pkid = new CustomFieldDto();
        pkid.setHdrGuid(customer.getGuid());
        pkid.setParamCode("EMP_CUSTOMER_PKID");
        pkid.setParamName("EMP_CUSTOMER_PKID");
        pkid.setParamType(ExtensionParamTypes.PARAM_TYPE_INTEGER);
        pkid.setValueString(inputData.getWaveletInternalId());
        pkid.setStatus("ACTIVE");
        pkid.setExtType("SYS_APPLET");
        customFieldDtoList.add(pkid);

        /*CustomFieldDto branchCode = new CustomFieldDto();
        branchCode.setHdrGuid(entityDto.getGuid());
        branchCode.setParamCode("EMP_CUSTOMER_BRANCH_CODE");
        branchCode.setParamName("EMP_CUSTOMER_BRANCH_CODE");
        branchCode.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        branchCode.setValueString(String.valueOf(empBranchCode));
        branchCode.setStatus("ACTIVE");
        branchCode.setExtType("SYS_APPLET");
        customFieldDtoList.add(branchCode);*/

        /*CustomFieldDto regnum = new CustomFieldDto();
        regnum.setHdrGuid(entityDto.getGuid());
        regnum.setParamCode("EMP_CUSTOMER_VEHICLE_REGNUM");
        regnum.setParamName("EMP_CUSTOMER_VEHICLE_REGNUM");
        regnum.setParamType(ExtensionParamTypes.PARAM_TYPE_STRING);
        regnum.setValueString(strRegNumList);
        regnum.setStatus("ACTIVE");
        regnum.setExtType("SYS_APPLET");
        customFieldDtoList.add(regnum);*/

        customer.setCustomFieldDtos(customFieldDtoList);

        return customer;
    }
}
