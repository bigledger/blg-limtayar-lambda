package app;
public class TransformCsvToCustRequest {

    private String input;

    public TransformCsvToCustRequest(String input) {
        this.input = input;
    }

    public TransformCsvToCustRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
