package app;

public class TransformCsvToCustResponse {

    private String hello;

    public TransformCsvToCustResponse(String hello) {
        this.hello = hello;
    }

    public TransformCsvToCustResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
