package app;

import app.constants.CsvConstants;
import app.constants.LimtayarItemCsvHeader;

import java.math.BigDecimal;

public class WriteDtoToCsv {

    public  String getDtoInCsv(LimtayarItemCsvHeader itemDtoMapToCsv) {


        String stringItems = "";

        String code="";
        String name="";
        String descr="";
//        String status="";
        String txnType="";
        String category1="";
        String category2="";
        String category3="";
        String category4="";
        String category5="";
        String category6="";
        String category7="";
        String category8="";
        String category9="";
        String category10="";
        String list_price="0.00";
        String uom="";

        if(itemDtoMapToCsv.getCode()!=null) {
            code = itemDtoMapToCsv.getCode();
        }
        if(itemDtoMapToCsv.getName()!=null) {
            name = itemDtoMapToCsv.getName();
        }
        if(itemDtoMapToCsv.getDescr()!=null) {
            descr = itemDtoMapToCsv.getDescr();
        }
//        if(itemDtoMapToCsv.getStatus()!=null) {
//            status = itemDtoMapToCsv.getStatus();
//        }
        if(itemDtoMapToCsv.getTxnType()!=null) {
            txnType = itemDtoMapToCsv.getTxnType();
        }
        if(itemDtoMapToCsv.getCategory1()!=null) {
            category1 = itemDtoMapToCsv.getCategory1();
        }
        if(itemDtoMapToCsv.getCategory2()!=null) {
            category2 = itemDtoMapToCsv.getCategory2();
        }
        if(itemDtoMapToCsv.getCategory3()!=null) {
            category3 = itemDtoMapToCsv.getCategory3();
        }
        if(itemDtoMapToCsv.getCategory4()!=null) {
            category4 = itemDtoMapToCsv.getCategory4();
        }
        if(itemDtoMapToCsv.getCategory5()!=null) {
            category5 = itemDtoMapToCsv.getCategory5();
        }
        if(itemDtoMapToCsv.getCategory6()!=null) {
            category6 = itemDtoMapToCsv.getCategory6();
        }
        if(itemDtoMapToCsv.getCategory7()!=null) {
            category7 = itemDtoMapToCsv.getCategory7();
        }
        if(itemDtoMapToCsv.getCategory8()!=null) {
            category8 = itemDtoMapToCsv.getCategory8();
        }
        if(itemDtoMapToCsv.getCategory9()!=null) {
            category9 = itemDtoMapToCsv.getCategory9();
        }
        if(itemDtoMapToCsv.getCategory10()!=null) {
            category10 = itemDtoMapToCsv.getCategory10();
        }
        if(itemDtoMapToCsv.getList_price()!=null) {
            list_price = itemDtoMapToCsv.getList_price();
        }
        if(itemDtoMapToCsv.getUom()!=null) {
            uom = itemDtoMapToCsv.getUom();
        }

        stringItems =   code +  CsvConstants.BAR_DELIMITER +
                        name +  CsvConstants.BAR_DELIMITER +
                        descr +  CsvConstants.BAR_DELIMITER +
//                        status +  CsvConstants.BAR_DELIMITER +
                        txnType +  CsvConstants.BAR_DELIMITER +
                        category1 +  CsvConstants.BAR_DELIMITER +
                        category2 +  CsvConstants.BAR_DELIMITER +
                        category3 +  CsvConstants.BAR_DELIMITER +
                        category4 +  CsvConstants.BAR_DELIMITER +
                        category5 +  CsvConstants.BAR_DELIMITER +
                        category6 +  CsvConstants.BAR_DELIMITER +
                        category7 +  CsvConstants.BAR_DELIMITER +
                        category8 +  CsvConstants.BAR_DELIMITER +
                        category9 +  CsvConstants.BAR_DELIMITER +
                        category10 +  CsvConstants.BAR_DELIMITER +
                        list_price +  CsvConstants.BAR_DELIMITER +
                        uom + CsvConstants.NEW_LINE_SEPARATOR;

        return stringItems;

    }
}
