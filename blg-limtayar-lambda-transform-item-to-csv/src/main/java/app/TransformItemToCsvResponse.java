package app;

public class TransformItemToCsvResponse {

    private String hello;

    public TransformItemToCsvResponse(String hello) {
        this.hello = hello;
    }

    public TransformItemToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
