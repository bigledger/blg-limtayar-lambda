package app;
public class TransformItemToCsvRequest {

    private String input;

    public TransformItemToCsvRequest(String input) {
        this.input = input;
    }

    public TransformItemToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
