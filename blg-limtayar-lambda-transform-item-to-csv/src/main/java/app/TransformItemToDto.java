package app;

import app.constants.LimtayarItemCsvHeader;
import com.bigledger.core1.adapter.integration.services.IItemService;
import com.bigledger.core1.adapter.integration.services.ItemServiceImpl;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.dto.GenericDocLineDto;

import java.text.DecimalFormat;

public class TransformItemToDto {

    public LimtayarItemCsvHeader getItemLine(LimtayarItemCsvHeader itemDtoToCsvModel, RequestHeaders requestHeaders) {


        LimtayarItemCsvHeader itemDtoToCsvModelLine = new LimtayarItemCsvHeader();
        IItemService itemService = new ItemServiceImpl();
        EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
        DecimalFormat df2 = new DecimalFormat("0.00");

        itemDtoToCsvModelLine.setCode(itemDtoToCsvModel.getCode());
        itemDtoToCsvModelLine.setName(itemDtoToCsvModel.getName());
        itemDtoToCsvModelLine.setDescr(itemDtoToCsvModel.getDescr());
        itemDtoToCsvModelLine.setTxnType(itemDtoToCsvModel.getTxnType());
        itemDtoToCsvModelLine.setCategory1(itemDtoToCsvModel.getCategory1());
        itemDtoToCsvModelLine.setCategory2(itemDtoToCsvModel.getCategory2());
        itemDtoToCsvModelLine.setCategory3(itemDtoToCsvModel.getCategory3());
        itemDtoToCsvModelLine.setCategory4(itemDtoToCsvModel.getCategory4());
        itemDtoToCsvModelLine.setCategory5(itemDtoToCsvModel.getCategory5());
        itemDtoToCsvModelLine.setCategory6(itemDtoToCsvModel.getCategory6());
        itemDtoToCsvModelLine.setCategory7(itemDtoToCsvModel.getCategory7());
        itemDtoToCsvModelLine.setCategory8(itemDtoToCsvModel.getCategory8());
        itemDtoToCsvModelLine.setCategory9(itemDtoToCsvModel.getCategory9());
        itemDtoToCsvModelLine.setCategory10(itemDtoToCsvModel.getCategory10());
        itemDtoToCsvModelLine.setList_price(itemDtoToCsvModel.getList_price());
        itemDtoToCsvModelLine.setUom(itemDtoToCsvModel.getUom());


        // get UOM


//        try {
//            // check if item exist in BLG
//            ApiResponse response = itemService.getItemHeaderDtoByGuid(genericDocLineDto.getItemGuid(), requestHeaders);
//
//
//            JSONParser parser = new JSONParser();
//            JSONObject json = (JSONObject) parser.parse(response.asJson());
////                    System.out.println("Item Data as Json: " + json.get("data"));
//
//            ObjectMapper mapper = new ObjectMapper();
//            mapper.registerModule(new JavaTimeModule());
//            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//            ItemHeaderDto itemHdrDto = mapper.readValue(json.get("data").toString(), ItemHeaderDto.class);
//
//            itemDtoToCsvModel.setUom(itemHdrDto.getUom());
//
//        } catch (
//                AkaunApiServerException e) {
//            System.out.println(e.getMessage());
//            etlSendDataResponse.setEventCode("FAILED");
//            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        } catch (
//                NetworkException e) {
//            System.out.println(e.getMessage());
//            etlSendDataResponse.setEventCode("RETRY");
//            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//
//            System.out.println(e.getClass().getCanonicalName());
//            etlSendDataResponse.setEventCode("FAILED");
//            etlSendDataResponse.setError(e.getMessage());
//            return null;
//        }

//        double quantity =  genericDocLineDto.getQuantityBase().doubleValue();
//        String strQuantity = df2.format(quantity);
//        double listPrice = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//        String unitAmountStd = df2.format(listPrice);
//
//        double NettPrice = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//        String netPrice = df2.format(NettPrice);
//
//        double gstRate =  genericDocLineDto.getTaxGstRate().doubleValue();
//        String strGstRate = df2.format(gstRate);
//        double gstAmount = genericDocLineDto.getAmountTaxGst().doubleValue();
//        String strGstAmount = df2.format(gstAmount);
//
//        double totalAmount = genericDocLineDto.getAmountTxn().doubleValue();
//        String strAmountTxn = df2.format (totalAmount);
//
//        srDtoToCsvModelLine.setQuantity(strQuantity);
//        srDtoToCsvModelLine.setGstRate(strGstRate);
//        srDtoToCsvModelLine.setAmountTxn(strAmountTxn);
//        srDtoToCsvModelLine.setGstAmount(strGstAmount);
//        srDtoToCsvModelLine.setAmountNet(netPrice);
//        srDtoToCsvModelLine.setAmountStd(unitAmountStd);
//        srDtoToCsvModelLine.setSeqNo(srDtoToCsvModel.getSeqNo());


        return itemDtoToCsvModelLine;
    }
}
