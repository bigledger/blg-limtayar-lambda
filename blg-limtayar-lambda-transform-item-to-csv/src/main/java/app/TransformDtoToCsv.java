package app;

import app.constants.LimtayarItemCsvHeader;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.dto.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TransformDtoToCsv {

    public  LimtayarItemCsvHeader getItem(ItemHeaderDto itemHeaderDto, RequestHeaders requestHeaders) {

//        List<LimtayarItemCsvHeader> itemDtoInCsv = new ArrayList<LimtayarItemCsvHeader>();
        EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
        LimtayarItemCsvHeader itemDtoToCsvModel = new LimtayarItemCsvHeader();


//        DecimalFormat nft = new DecimalFormat("#00.###");
//        DecimalFormat df2 = new DecimalFormat("#.##");
//        nft.setDecimalSeparatorAlwaysShown(false);

        // convert data from Item Hdr DTO to CSV model

            itemDtoToCsvModel.setCode(itemHeaderDto.getCode());
            itemDtoToCsvModel.setName(itemHeaderDto.getName());
            itemDtoToCsvModel.setStatus(itemHeaderDto.getStatus());
            itemDtoToCsvModel.setTxnType(itemHeaderDto.getTxnType());
            List<CustomFieldDto> customCriteriaFieldLists = itemHeaderDto.getCustomFieldList();
        for (int j = 0; j < customCriteriaFieldLists.size(); j++) {
            CustomFieldDto customCriteriaFieldDto = customCriteriaFieldLists.get(j);
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_1")) {
                itemDtoToCsvModel.setCategory1(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_2")) {
                itemDtoToCsvModel.setCategory2(customCriteriaFieldDto.getValueString());
            }

            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_3")) {
                itemDtoToCsvModel.setCategory3(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_4")) {
                itemDtoToCsvModel.setCategory4(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_5")) {
                itemDtoToCsvModel.setCategory5(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_6")) {
                itemDtoToCsvModel.setCategory6(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_7")) {
                itemDtoToCsvModel.setCategory7(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_8")) {
                itemDtoToCsvModel.setCategory8(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_9")) {
                itemDtoToCsvModel.setCategory9(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_CATEGORY_10")) {
                itemDtoToCsvModel.setCategory10(customCriteriaFieldDto.getValueString());
            }
            if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ITEM_LIST_PRICE")) {
                itemDtoToCsvModel.setList_price(customCriteriaFieldDto.getValueNumeric().toString());
            }
            itemDtoToCsvModel.setUom(itemHeaderDto.getUom());


            // convert data from generic Doc Line DTO to CSV model
            //List<ItemLineDto> itemLineList = itemHeaderDto.getItemLineList();


            // do a parallel call to get all items
            /*System.out.println("ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService()");
            ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService();

            System.out.println("cinvDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(genericDocLineLists,cinvDtoToCsvModel,requestHeaders)");
            //itemDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(itemLineList,itemDtoToCsvModel,requestHeaders);

            System.out.println("List<LimtayarCinvCsvHeader> orderedPOLineList = new ArrayList<LimtayarCinvCsvHeader>()");
            List<LimtayarItemCsvHeader> orderedCinvLineList = new ArrayList<LimtayarItemCsvHeader>();


            System.out.println("TreeMap <String, LimtayarCinvCsvHeader> itemLineMap = new TreeMap < String, LimtayarCinvCsvHeader>()");
            TreeMap<String, LimtayarItemCsvHeader> itemLineMap = new TreeMap < String, LimtayarItemCsvHeader>();


            System.out.println("for (int i = 0; i < cinvDtoInCsv.size(); i++)");
//            for (int i = 0; i < itemDtoInCsv.size(); i++)
//            {
//                LimtayarItemCsvHeader cinvLineCsv = itemDtoInCsv.get(i);
//                itemLineMap.put("itemHeader-"+i, cinvLineCsv);
//
//            }

                itemLineMap.put("itemHeader", itemDtoToCsvModel);


            System.out.println("for(Map.Entry<String,LimtayarCinvCsvHeader> entry : cinvLineMap.entrySet())");
            for(Map.Entry<String,LimtayarItemCsvHeader> entry : itemLineMap.entrySet()) {
                String key = entry.getKey();
                LimtayarItemCsvHeader value = entry.getValue();
                orderedCinvLineList.add (value);
                //System.out.println(key + " => " + value);
            }*/
        }
        return itemDtoToCsvModel;
    }
}
