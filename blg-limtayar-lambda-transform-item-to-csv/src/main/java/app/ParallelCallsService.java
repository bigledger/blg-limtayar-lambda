package app;

import app.constants.LimtayarItemCsvHeader;
import com.bigledger.core1.adapter.integration.constants.Constants;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.IItemService;
import com.bigledger.core1.adapter.integration.services.ItemServiceImpl;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.GenericDocLineDto;
import com.bigledger.core1.dto.ItemHeaderDto;
import com.bigledger.core1.dto.ItemLineDto;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ParallelCallsService {


        IItemService itemService = new ItemServiceImpl();
        TransformDtoToCsv transformDtoToCsv = new TransformDtoToCsv();
        TransformItemToDto transformItemToDto = new TransformItemToDto();
        WriteDtoToCsv writeDtoToCsv = new WriteDtoToCsv();

        public List<ApiResponse> getItemToDos(List<String> ids, RequestHeaders requestHeaders){

            List<CompletableFuture<ApiResponse>> futures =
                    ids.stream()
                            .map(id -> getItemToDoAsync(id, requestHeaders))
                            .collect(Collectors.toList());

            List<ApiResponse> result =
                    futures.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList());

            return result;
        }
        CompletableFuture<ApiResponse> getItemToDoAsync(String id, RequestHeaders requestHeaders){

            CompletableFuture<ApiResponse> future = CompletableFuture.supplyAsync(new Supplier<ApiResponse>() {
                @Override
                public ApiResponse get() {

                     ApiResponse response = null;

                    try {

                          response = itemService.getItemHeaderDtoByGuid(id, requestHeaders);

                    }
                    catch (AkaunApiServerException e) {
                        System.out.println("AkaunApiServerException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;

                    } catch (NetworkException e) {
                        System.out.println("NetworkException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                    return response;
                }
            });

            return future;
        }


    public List<LimtayarItemCsvHeader> getTransformDtoToCsvToDos(List<ItemHeaderDto> itemHeaderDtos, RequestHeaders requestHeaders){

        List<CompletableFuture<LimtayarItemCsvHeader>> futures =
                itemHeaderDtos.stream()
                        .map(itemHeaderDto -> getTransformDtoToCsvToDoAsync(itemHeaderDto, requestHeaders))
                        .collect(Collectors.toList());

        List<LimtayarItemCsvHeader> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<LimtayarItemCsvHeader> getTransformDtoToCsvToDoAsync(ItemHeaderDto itemHeaderDto, RequestHeaders requestHeaders){

        CompletableFuture<LimtayarItemCsvHeader> future = CompletableFuture.supplyAsync(new Supplier<LimtayarItemCsvHeader>() {
            @Override
            public LimtayarItemCsvHeader get() {

                LimtayarItemCsvHeader response = null;

                response = transformDtoToCsv.getItem(itemHeaderDto, requestHeaders);

                return response;
                }

        });

        return future;
    }


    public String getWriteDtoToCsvToDos(LimtayarItemCsvHeader itemDtoMapToCsv){

        CompletableFuture<String> future = getWriteDtoToCsvToDoAsync(itemDtoMapToCsv);

        String result = future.join();

        return result;
    }

    CompletableFuture<String> getWriteDtoToCsvToDoAsync(LimtayarItemCsvHeader itemDtoMapToCsv){

        CompletableFuture<String> future = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {

                String response = null;

                response = writeDtoToCsv.getDtoInCsv(itemDtoMapToCsv);

                return response;
            }

        });

        return future;
    }

    /*public List<LimtayarItemCsvHeader> getTransformDtoItemToCsvToDos(List<GenericDocLineDto> genericDocLineDtos, LimtayarItemCsvHeader itemDtoToCsvModel, RequestHeaders requestHeaders){

        List<CompletableFuture<LimtayarItemCsvHeader>> futures =
                genericDocLineDtos.stream()
                        .map(genericDocLineDto -> getTransformDtoItemToCsvToDoAsync(genericDocLineDto, itemDtoToCsvModel, requestHeaders))
                        .collect(Collectors.toList());

        List<LimtayarItemCsvHeader> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<LimtayarItemCsvHeader> getTransformDtoItemToCsvToDoAsync(GenericDocLineDto genericDocLineDto, LimtayarItemCsvHeader itemDtoToCsvModel, RequestHeaders requestHeaders){

        CompletableFuture<LimtayarItemCsvHeader> future = CompletableFuture.supplyAsync(new Supplier<LimtayarItemCsvHeader>() {
            @Override
            public LimtayarItemCsvHeader get() {

                LimtayarItemCsvHeader response = null;

                response = transformItemToDto.getItemLine(genericDocLineDto,itemDtoToCsvModel,requestHeaders);

                return response;
            }

        });

        return future;
    }*/


}
