package app.constants;

public interface EventQueueConstants {


    //EMP QUEUE	EVENTS

    String TXN_TYPE = "SYS_APPLET";

    String MESSAGE_EXPORT_EVENT_CODE = "CKL_ITEM_LISTING_EXPORTED";
    String MESSAGE_EXPORT_ACTION = "CKL_ITEM_LISTING_EXPORTED";
    String MESSAGE_EXPORT_DESC = "CKL ITEM LISTING HAS BEEN EXPORTED";


    //Sleep timers
    Integer HOUR = 3600;
    Integer ONE_MINUTE = 60;
    Integer FIVE_MINUTES = ONE_MINUTE * 5;
    Integer TEN_MINUTES = ONE_MINUTE * 10;
    Integer DAY = HOUR * 24;
}
