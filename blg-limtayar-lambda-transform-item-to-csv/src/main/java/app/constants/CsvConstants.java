package app.constants;

public interface CsvConstants {

    //Delimiter used in CSV file
    String BAR_DELIMITER = "|";
    String NEW_LINE_SEPARATOR = "\n";
    String DOUBLE_QUOTE = "\"";

    //CSV file header

    String FILE_HEADER =  "Item Code" +  "|"  +  "Item Name"  +  "|" +  "Description"  +  "|"  +  "Status"  +  "|" + "Item Type"  +  "|"  +  "Category 1"  +
            "|"  +  "Category 2"  +  "|" +  "Category 3"  +  "|"  + "Category 4"  +  "|"  +  "Category 5"  + "|" +  "Category 6"  +
            "|"  +  "Category 7"  +  "|"  +  "Category 8"  + "|"  +  "Category 9"  +  "|"  +  "Category 10"  +   "|"  +  "List Price"  +
            "|"  +  "UOM";
}


