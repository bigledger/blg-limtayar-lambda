package app.constants;

import java.math.BigDecimal;

public class LimtayarItemCsvHeader {

    private String code;
    private String name;
    private String descr;
    private String status;
    private String txnType;
    private String category1;
    private String category2;
    private String category3;
    private String category4;
    private String category5;
    private String category6;
    private String category7;
    private String category8;
    private String category9;
    private String category10;
    private String list_price;
    private String uom;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getCategory1() {
        return category1;
    }

    public void setCategory1(String category1) {
        this.category1 = category1;
    }

    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        this.category2 = category2;
    }

    public String getCategory3() {
        return category3;
    }

    public void setCategory3(String category3) {
        this.category3 = category3;
    }

    public String getCategory4() {
        return category4;
    }

    public void setCategory4(String category4) {
        this.category4 = category4;
    }

    public String getCategory5() {
        return category5;
    }

    public void setCategory5(String category5) {
        this.category5 = category5;
    }

    public String getCategory6() {
        return category6;
    }

    public void setCategory6(String category6) {
        this.category6 = category6;
    }

    public String getCategory7() {
        return category7;
    }

    public void setCategory7(String category7) {
        this.category7 = category7;
    }

    public String getCategory8() {
        return category8;
    }

    public void setCategory8(String category8) {
        this.category8 = category8;
    }

    public String getCategory9() {
        return category9;
    }

    public void setCategory9(String category9) {
        this.category9 = category9;
    }

    public String getCategory10() {
        return category10;
    }

    public void setCategory10(String category10) {
        this.category10 = category10;
    }

    public String getList_price() {
        return list_price;
    }

    public void setList_price(String list_price) {
        this.list_price = list_price;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }
}
