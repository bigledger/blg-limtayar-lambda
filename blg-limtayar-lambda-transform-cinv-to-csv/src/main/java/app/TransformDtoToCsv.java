package app;

import app.constants.LimtayarCinvCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.IItemService;
import com.bigledger.core1.adapter.integration.services.ISubqueryService;
import com.bigledger.core1.adapter.integration.services.ItemServiceImpl;
import com.bigledger.core1.adapter.integration.services.SubqueryServiceImpl;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.CustomFieldDto;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;
import com.bigledger.core1.dto.ItemHeaderDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

public class TransformDtoToCsv {

    public  List<LimtayarCinvCsvHeader> getCinv(GenericDocHdrDto genericDocHdrDto, RequestHeaders requestHeaders) {

        List<LimtayarCinvCsvHeader> cinvDtoInCsv = new ArrayList<LimtayarCinvCsvHeader>();
        EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
        ZoneId zoneId = ZoneId.of("Asia/Kuala_Lumpur");        //Zone information



            ZonedDateTime zdtAtMalaysia = genericDocHdrDto.getDateTxn().withZoneSameInstant(zoneId);
            LimtayarCinvCsvHeader cinvDtoToCsvModel = new LimtayarCinvCsvHeader();


            // System.out.println("Zone date time: " + genericDocHdrDto.get(i).getDateTxn().toLocalDateTime());
            // System.out.println("Local date time: " + zdtAtMalaysia);

            DecimalFormat nft = new DecimalFormat("#00.###");
            DecimalFormat df2 = new DecimalFormat("#.##");
            nft.setDecimalSeparatorAlwaysShown(false);


            String yyyy = Integer.toString(zdtAtMalaysia.getYear());
            String mm = nft.format(zdtAtMalaysia.getMonthValue());
            String dd = nft.format(zdtAtMalaysia.getDayOfMonth());
            String hh = nft.format(zdtAtMalaysia.getHour());
            String min = nft.format(zdtAtMalaysia.getMinute());
            String Date = yyyy + mm + dd + hh + min;

            //Displaying current time in 12 hour format with AM/PM
            DateFormat outputFormat = new SimpleDateFormat("hh:mmaa");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.US);

            // String inputText = "2012-11-17T00:00:00.000-05:00";
            Date date = null;
            try {
                date = inputFormat.parse(zdtAtMalaysia.toString());
            } catch (ParseException e) {
                e.printStackTrace();
                System.out.println(e);
            }

            String orderTime  = outputFormat.format(date);


             System.out.println("############################ orderTime: "+orderTime);
             System.out.println("############################### Date is: "+Date);

            // convert data from PO Hdr DTO to CSV model

            cinvDtoToCsvModel.setTxnDate(Date);
//            poDtoToCsvModel.setRemark1(genericDocHdrDto.getDocRemarks());
//            poDtoToCsvModel.setRemark2("");

            List<CustomFieldDto> customCriteriaFieldLists= genericDocHdrDto.getCustomFieldList();

            for (int j = 0; j < customCriteriaFieldLists.size(); j++) {
                CustomFieldDto customCriteriaFieldDto = customCriteriaFieldLists.get(j);
                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_CUST_INVOICE_PKID")) {
                    cinvDtoToCsvModel.setInvPkid(customCriteriaFieldDto.getValueString());
                    System.out.println("############################ EMP_INVOICE_PKID: "+ customCriteriaFieldDto.getValueString());
                }
                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_JOBSHEET_PKID")) {
                    cinvDtoToCsvModel.setJobsheetPkid(customCriteriaFieldDto.getValueString());
                    System.out.println("############################ EMP_JOBSHEET_PKID: "+ customCriteriaFieldDto.getValueString());
                }
                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("BLG_ENTITY_NAME")) {
                    cinvDtoToCsvModel.setCustomerName(customCriteriaFieldDto.getValueString());
                    System.out.println("############################ BLG_ENTITY_NAME: "+ customCriteriaFieldDto.getValueString());
                }
                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_INVOICE_VEHICLE_REGNUM")) {
                    cinvDtoToCsvModel.setInvVehicleRegNo(customCriteriaFieldDto.getValueString());
                    System.out.println("############################ EMP_INVOICE_VEHICLE_REGNUM: "+ customCriteriaFieldDto.getValueString());
                }

                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_INVOICE_DOC_TYPE")) {
                    cinvDtoToCsvModel.setDocType(customCriteriaFieldDto.getValueString());
                    System.out.println("############################ EMP_INVOICE_DOC_TYPE: "+ customCriteriaFieldDto.getValueString());
                }

                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_ENTITY_KEY")) {
                    cinvDtoToCsvModel.setCustomerPkid(customCriteriaFieldDto.getValueString());
                    System.out.println("############################ EMP_ENTITY_KEY: "+ customCriteriaFieldDto.getValueString());
                }

                if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("EMP_BRANCH_CODE")) {
                    cinvDtoToCsvModel.setBranchCode(customCriteriaFieldDto.getValueString());
                    System.out.println("############################ EMP_BRANCH_CODE: "+ customCriteriaFieldDto.getValueString());
                }

                //Leave out Remarks 2 this is for 100 Value only not applicable for Panasonic
                /*if (customCriteriaFieldDto.getParamCode().equalsIgnoreCase("SH_ERP_REMARKS_2")) {
                    poDtoToCsvModel.setRemark2(customCriteriaFieldDto.getValueString());
                }*/

            }

            //
            //Query to get delivered to name from delivered to code
//            Map<String, String> subqueryDTS= new HashMap<String, String>();
//            List<String> entityHdrGuid = new ArrayList<>();
//
//            String dTSSql = "select ent.name AS neededguid from bl_fi_mst_entity_hdr ent" +
//                    "inner join bl_fi_mst_entity_ext ext on ent.guid = ext.entity_hdr_guid" +
//                    "where ent.name = ' " + cinvDtoToCsvModel.getEntityHdrGuid() + " ' limit 1";
//
//
//            subqueryDTS.put("subquery", dTSSql);
//
//            System.out.println("Querying database for delivered to name ");

            //System.out.println("SQL Query: " + subqueryDTS);

//
//            try {
//
//                //  get delivered to store name
//                ISubqueryService subqueryServiceD = new SubqueryServiceImpl();
//
//                ApiResponse responseSubqueryD = subqueryServiceD.getGuidsBySubquery(subqueryDTS, requestHeaders);
//
//                JSONParser parser = new JSONParser();
//                JSONObject json2 = (JSONObject) parser.parse(responseSubqueryD.asJson());
//                //System.out.println("Delivered to Store: " + json2.get("data").toString());
//
//                ObjectMapper mapper = new ObjectMapper();
//                mapper.registerModule(new JavaTimeModule());
//                mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//                entityHdrGuid = mapper.readValue(json2.get("data").toString(), ArrayList.class);
//
//                for (int l = 0; l < entityHdrGuid.size(); l++) {
//
//                    // System.out.println("GUID Doc Header Counter: " + deliveryToStore.get(l));
//                    cinvDtoToCsvModel.setEntityHdrGuid(entityHdrGuid.get(l));
//
//                }
//
//            } catch (AkaunApiServerException e) {
//                System.out.println(e.getMessage());
//                etlSendDataResponse.setEventCode("FAILED");
//                etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//                etlSendDataResponse.setError(e.getMessage());
//                return null;
//            } catch (NetworkException e) {
//                System.out.println(e.getMessage());
//                etlSendDataResponse.setEventCode("RETRY");
//                etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//                etlSendDataResponse.setError(e.getMessage());
//                return null;
//            } catch (Exception e) {
//                System.out.println(e.getMessage());
//
//                System.out.println(e.getClass().getCanonicalName());
//                etlSendDataResponse.setEventCode("FAILED");
//                etlSendDataResponse.setError(e.getMessage());
//                return null;
//            }


            // convert data from generic Doc Line DTO to CSV model
            List<GenericDocLineDto> genericDocLineLists = genericDocHdrDto.getGenericDocLineDtoList();


        // do a parallel call to get all items
        System.out.println("ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService()");
        ParallelCallsService parallelTransformDtoItemToCsv = new ParallelCallsService();

        System.out.println("cinvDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(genericDocLineLists,cinvDtoToCsvModel,requestHeaders)");
         cinvDtoInCsv = parallelTransformDtoItemToCsv.getTransformDtoItemToCsvToDos(genericDocLineLists,cinvDtoToCsvModel,requestHeaders);

        System.out.println("List<LimtayarCinvCsvHeader> orderedPOLineList = new ArrayList<LimtayarCinvCsvHeader>()");
        List<LimtayarCinvCsvHeader> orderedCinvLineList = new ArrayList<LimtayarCinvCsvHeader>();


        System.out.println("TreeMap <String, LimtayarCinvCsvHeader> cinvLineMap = new TreeMap < String, LimtayarCinvCsvHeader>()");
        TreeMap <String, LimtayarCinvCsvHeader> cinvLineMap = new TreeMap < String, LimtayarCinvCsvHeader>();


        System.out.println("for (int i = 0; i < cinvDtoInCsv.size(); i++)");
        for (int i = 0; i < cinvDtoInCsv.size(); i++)
        {
            LimtayarCinvCsvHeader cinvLineCsv = cinvDtoInCsv.get(i);
            cinvLineMap.put("cinvHeader-"+i, cinvLineCsv);

        }
        System.out.println("for(Map.Entry<String,LimtayarCinvCsvHeader> entry : cinvLineMap.entrySet())");
        for(Map.Entry<String,LimtayarCinvCsvHeader> entry : cinvLineMap.entrySet()) {
            String key = entry.getKey();
            LimtayarCinvCsvHeader value = entry.getValue();
            orderedCinvLineList.add (value);
            //System.out.println(key + " => " + value);
        }

//            for (int k = 0; k < genericDocLineLists.size(); k++) {
//                GenericDocLineDto genericDocLineDto = genericDocLineLists.get(k);
//                SenhengPanaEdiPoCsvHeader poDtoToCsvModelLine = new SenhengPanaEdiPoCsvHeader();
//
//                poDtoToCsvModelLine.setLineType("D");
//                poDtoToCsvModelLine.setpNumber(poDtoToCsvModel.getpoNumber());
//                poDtoToCsvModelLine.setOrderDate(poDtoToCsvModel.getOrderDate());
//                poDtoToCsvModelLine.setOrderTime(poDtoToCsvModel.getOrderTime());
//
//                poDtoToCsvModelLine.setSupplierNo(poDtoToCsvModel.getSupplierNo());
//                poDtoToCsvModelLine.setDeliveryToStoreCode(poDtoToCsvModel.getDeliveryToStoreCode());
//                poDtoToCsvModelLine.setDeliveryToStoreName(poDtoToCsvModel.getDeliveryToStoreName());
//                poDtoToCsvModelLine.setOrderedBy(poDtoToCsvModel.getOrderedBy());
//                poDtoToCsvModelLine.setIssuedBy(poDtoToCsvModel.getIssuedBy());
//                poDtoToCsvModelLine.setExpiryDate(poDtoToCsvModel.getExpiryDate());
//                poDtoToCsvModelLine.setSupplier(poDtoToCsvModel.getSupplier());
//                poDtoToCsvModelLine.setRemark1(poDtoToCsvModel.getRemark1());
//                poDtoToCsvModelLine.setRemark2(poDtoToCsvModel.getRemark2());
//                poDtoToCsvModelLine.setRemark3(poDtoToCsvModel.getRemark3());
//                poDtoToCsvModelLine.setRemark4(poDtoToCsvModel.getRemark4());
//                poDtoToCsvModelLine.setRemark5(poDtoToCsvModel.getRemark5());
//
//                // get UOM
//
//
//                try {
//                    // check if item exist in BLG
//                    ApiResponse response = itemService.getItemHeaderDtoByGuid(genericDocLineDto.getItemGuid(), requestHeaders);
//
//
//                    JSONParser parser = new JSONParser();
//                    JSONObject json = (JSONObject) parser.parse(response.asJson());
////                    System.out.println("Item Data as Json: " + json.get("data"));
//
//                    ObjectMapper mapper = new ObjectMapper();
//                    mapper.registerModule(new JavaTimeModule());
//                    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//
//                    ItemHeaderDto itemHdrDto = mapper.readValue(json.get("data").toString(), ItemHeaderDto.class);
//
//                    poDtoToCsvModelLine.setUom(itemHdrDto.getUom());
//
//                } catch (AkaunApiServerException e) {
//                    System.out.println(e.getMessage());
//                    etlSendDataResponse.setEventCode("FAILED");
//                    etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//                    etlSendDataResponse.setError(e.getMessage());
//                    return null;
//                } catch (NetworkException e) {
//                    System.out.println(e.getMessage());
//                    etlSendDataResponse.setEventCode("RETRY");
//                    etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
//                    etlSendDataResponse.setError(e.getMessage());
//                    return null;
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//
//                    System.out.println(e.getClass().getCanonicalName());
//                    etlSendDataResponse.setEventCode("FAILED");
//                    etlSendDataResponse.setError(e.getMessage());
//                    return null;
//                }
//
//                double invoicePrice =  genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//                String invPrice = df2.format(invoicePrice);
//
//                double NettPrice =  genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
//                String netPrice = df2.format(NettPrice);
//
//                poDtoToCsvModelLine.setPoType("0");
//
//                poDtoToCsvModelLine.setSeqNo(genericDocLineDto.getPositionId());
//                poDtoToCsvModelLine.setItemNo(genericDocLineDto.getItemCode());
//                poDtoToCsvModelLine.setItemBarcode(genericDocLineDto.getItemMachineCode());
//                poDtoToCsvModelLine.setItemDescription(genericDocLineDto.getItemName());
//                poDtoToCsvModelLine.setOrderQuantity(genericDocLineDto.getQuantityBase().doubleValue());
//                poDtoToCsvModelLine.setInvoicePrice(Double.valueOf(invPrice));
//                poDtoToCsvModelLine.setNettPrice(Double.valueOf(netPrice));
//                poDtoToCsvModelLine.setTotalCostNoGst(genericDocLineDto.getAmountNet().doubleValue());
//                poDtoToCsvModelLine.setGstCode(genericDocLineDto.getTaxGstCode());
//                poDtoToCsvModelLine.setGstRate(Double.valueOf(genericDocLineDto.getTaxGstRate().toString()));
//                poDtoToCsvModelLine.setGstAmount(genericDocLineDto.getAmountTaxGst().doubleValue());
//                poDtoToCsvModelLine.setTotalCost(genericDocLineDto.getAmountTxn().doubleValue());
//
//                poDtoInCsv.add(k, poDtoToCsvModelLine);
//            }




        return orderedCinvLineList;
    }
}
