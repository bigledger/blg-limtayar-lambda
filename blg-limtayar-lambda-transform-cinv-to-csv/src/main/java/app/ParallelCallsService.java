package app;

import app.constants.LimtayarCinvCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.IPurchaseOrderService;
import com.bigledger.core1.adapter.integration.services.ISalesInvoiceService;
import com.bigledger.core1.adapter.integration.services.PurchaseOrderServiceImpl;
import com.bigledger.core1.adapter.integration.services.SalesInvoiceServiceImpl;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class ParallelCallsService {


        ISalesInvoiceService salesInvoiceService = new SalesInvoiceServiceImpl();
        TransformDtoToCsv transformDtoToCsv = new TransformDtoToCsv();
        TransformItemToDto transformItemToDto = new TransformItemToDto();
        WriteDtoToCsv writeDtoToCsv = new WriteDtoToCsv();

        public List<ApiResponse> getCinvToDos(List<String> ids, RequestHeaders requestHeaders){

            List<CompletableFuture<ApiResponse>> futures =
                    ids.stream()
                            .map(id -> getCinvToDoAsync(id, requestHeaders))
                            .collect(Collectors.toList());

            List<ApiResponse> result =
                    futures.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList());

            return result;
        }
        CompletableFuture<ApiResponse> getCinvToDoAsync(String id, RequestHeaders requestHeaders){

            CompletableFuture<ApiResponse> future = CompletableFuture.supplyAsync(new Supplier<ApiResponse>() {
                @Override
                public ApiResponse get() {

                     ApiResponse response = null;

                    try {

                          response = salesInvoiceService.getSalesInvoiceByGuid(id, requestHeaders);

                    }
                    catch (AkaunApiServerException e) {
                        System.out.println("AkaunApiServerException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;

                    } catch (NetworkException e) {
                        System.out.println("NetworkException");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return response;
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                    return response;
                }
            });

            return future;
        }

    public List<List<LimtayarCinvCsvHeader>> getTransformDtoToCsvToDos(List<GenericDocHdrDto> genericDocHdrDtos, RequestHeaders requestHeaders){

        List<CompletableFuture<List<LimtayarCinvCsvHeader>>> futures =
                genericDocHdrDtos.stream()
                        .map(genericDocHdrDto -> getTransformDtoToCsvToDoAsync(genericDocHdrDto, requestHeaders))
                        .collect(Collectors.toList());

        List<List<LimtayarCinvCsvHeader>> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<List<LimtayarCinvCsvHeader>> getTransformDtoToCsvToDoAsync(GenericDocHdrDto genericDocHdrDto, RequestHeaders requestHeaders){

        CompletableFuture<List<LimtayarCinvCsvHeader>> future = CompletableFuture.supplyAsync(new Supplier<List<LimtayarCinvCsvHeader>>() {
            @Override
            public List<LimtayarCinvCsvHeader> get() {

                List<LimtayarCinvCsvHeader> response = null;

                response = transformDtoToCsv.getCinv(genericDocHdrDto, requestHeaders);

                return response;
                }

        });

        return future;
    }


    public List<LimtayarCinvCsvHeader> getTransformDtoItemToCsvToDos(List<GenericDocLineDto> genericDocLineDtos, LimtayarCinvCsvHeader poDtoToCsvModel, RequestHeaders requestHeaders){

        List<CompletableFuture<LimtayarCinvCsvHeader>> futures =
                genericDocLineDtos.stream()
                        .map(genericDocLineDto -> getTransformDtoItemToCsvToDoAsync(genericDocLineDto, poDtoToCsvModel, requestHeaders))
                        .collect(Collectors.toList());

        List<LimtayarCinvCsvHeader> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<LimtayarCinvCsvHeader> getTransformDtoItemToCsvToDoAsync(GenericDocLineDto genericDocLineDto,LimtayarCinvCsvHeader cinvDtoToCsvModel, RequestHeaders requestHeaders){

        CompletableFuture<LimtayarCinvCsvHeader> future = CompletableFuture.supplyAsync(new Supplier<LimtayarCinvCsvHeader>() {
            @Override
            public LimtayarCinvCsvHeader get() {

                LimtayarCinvCsvHeader response = null;

                response = transformItemToDto.getCinvItem(genericDocLineDto,cinvDtoToCsvModel,requestHeaders);

                return response;
            }

        });

        return future;
    }


    public List<String> getWriteDtoToCsvToDos(List<LimtayarCinvCsvHeader> poDtoMapToCsvs){

        List<CompletableFuture<String>> futures =
                poDtoMapToCsvs.stream()
                        .map(poDtoMapToCsv -> getWriteDtoToCsvToDoAsync(poDtoMapToCsv))
                        .collect(Collectors.toList());

        List<String> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        return result;
    }

    CompletableFuture<String> getWriteDtoToCsvToDoAsync(LimtayarCinvCsvHeader cinvDtoMapToCsv){

        CompletableFuture<String> future = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {

                String response = null;

                response = writeDtoToCsv.getDtoInCsv(cinvDtoMapToCsv);

                return response;
            }

        });

        return future;
    }


}
