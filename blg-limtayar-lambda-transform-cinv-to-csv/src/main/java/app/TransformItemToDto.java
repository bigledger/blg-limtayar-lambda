package app;

import app.constants.LimtayarCinvCsvHeader;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.IItemService;
import com.bigledger.core1.adapter.integration.services.ItemServiceImpl;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.GenericDocHdrDto;
import com.bigledger.core1.dto.GenericDocLineDto;
import com.bigledger.core1.dto.ItemHeaderDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;

import java.text.DecimalFormat;
import java.util.List;

public class TransformItemToDto {

    public LimtayarCinvCsvHeader getCinvItem(GenericDocLineDto genericDocLineDto, LimtayarCinvCsvHeader cinvDtoToCsvModel, RequestHeaders requestHeaders) {


        LimtayarCinvCsvHeader cinvDtoToCsvModelLine = new LimtayarCinvCsvHeader();
        IItemService itemService = new ItemServiceImpl();
        EtlSendDataResponse etlSendDataResponse = new EtlSendDataResponse();
        DecimalFormat df2 = new DecimalFormat("0.00");

        cinvDtoToCsvModelLine.setInvPkid(cinvDtoToCsvModel.getInvPkid());
        cinvDtoToCsvModelLine.setJobsheetPkid(cinvDtoToCsvModel.getJobsheetPkid());
        cinvDtoToCsvModelLine.setTxnDate(cinvDtoToCsvModel.getTxnDate());
        cinvDtoToCsvModelLine.setCustomerPkid(cinvDtoToCsvModel.getCustomerPkid());

        cinvDtoToCsvModelLine.setCustomerName(cinvDtoToCsvModel.getCustomerName());
        cinvDtoToCsvModelLine.setBranchCode(cinvDtoToCsvModel.getBranchCode());
        cinvDtoToCsvModelLine.setInvVehicleRegNo(cinvDtoToCsvModel.getInvVehicleRegNo());
        cinvDtoToCsvModelLine.setDocCcy(genericDocLineDto.getDocCcy());
        cinvDtoToCsvModelLine.setDocType(cinvDtoToCsvModel.getDocType());


        // get UOM


        try {
            // check if item exist in BLG
            ApiResponse response = itemService.getItemHeaderDtoByGuid(genericDocLineDto.getItemGuid(), requestHeaders);


            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(response.asJson());
//                    System.out.println("Item Data as Json: " + json.get("data"));

            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            ItemHeaderDto itemHdrDto = mapper.readValue(json.get("data").toString(), ItemHeaderDto.class);

            cinvDtoToCsvModelLine.setUom(itemHdrDto.getUom());

        } catch (
                AkaunApiServerException e) {
            System.out.println(e.getMessage());
            etlSendDataResponse.setEventCode("FAILED");
            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
            etlSendDataResponse.setError(e.getMessage());
            return null;
        } catch (
                NetworkException e) {
            System.out.println(e.getMessage());
            etlSendDataResponse.setEventCode("RETRY");
            etlSendDataResponse.setApiResponseCode(e.getApiResponse().getCode());
            etlSendDataResponse.setError(e.getMessage());
            return null;
        } catch (Exception e) {
            System.out.println(e.getMessage());

            System.out.println(e.getClass().getCanonicalName());
            etlSendDataResponse.setEventCode("FAILED");
            etlSendDataResponse.setError(e.getMessage());
            return null;
        }

        double quantity =  genericDocLineDto.getQuantityBase().doubleValue();
        String strQuantity = df2.format(quantity);
        double amountStd = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
        String AmountStd = df2.format(amountStd);

        double NettPrice = genericDocLineDto.getAmountNet().doubleValue() / genericDocLineDto.getQuantityBase().doubleValue();
        String netPrice = df2.format(NettPrice);

        double gstRate =  genericDocLineDto.getTaxGstRate().doubleValue();
        String strGstRate = df2.format(gstRate);
        double gstAmount = genericDocLineDto.getAmountTaxGst().doubleValue();
        String strGstAmount = df2.format(gstAmount);


        double AmountTxn = genericDocLineDto.getAmountTxn().doubleValue();
        String strAmountTxn = df2.format (AmountTxn);

        double amountDiscount =  genericDocLineDto.getAmountDiscount().doubleValue();
        String strAmntDisc = df2.format(amountDiscount);


        cinvDtoToCsvModelLine.setItemCode(genericDocLineDto.getItemCode());
        cinvDtoToCsvModelLine.setItemName(genericDocLineDto.getItemName());
        cinvDtoToCsvModelLine.setItemDesc(genericDocLineDto.getItemDesc());
        cinvDtoToCsvModelLine.setQuantity(strQuantity);
        cinvDtoToCsvModelLine.setAmountStd(AmountStd);
        cinvDtoToCsvModelLine.setAmountNet(netPrice);
        cinvDtoToCsvModelLine.setAmountDiscount(strAmntDisc);
        cinvDtoToCsvModelLine.setGstCode(genericDocLineDto.getTaxGstCode());
        cinvDtoToCsvModelLine.setGstRate(strGstRate);
        cinvDtoToCsvModelLine.setGstAmount(strGstAmount);
        cinvDtoToCsvModelLine.setAmountTxn(strAmountTxn);
        cinvDtoToCsvModelLine.setGstType(genericDocLineDto.getTaxGstType());



        return cinvDtoToCsvModelLine;
    }
}
