package app;


import app.constants.CsvConstants;
import app.constants.LimtayarCinvCsvHeader;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaAsyncClient;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification;


import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.*;
import com.bigledger.core1.adapter.integration.exception.AkaunApiServerException;
import com.bigledger.core1.adapter.integration.exception.NetworkException;
import com.bigledger.core1.adapter.integration.services.*;
import com.bigledger.core1.adapter.integration.utils.EtlSendDataResponse;
import com.bigledger.core1.adapter.integration.utils.RequestHeaders;
import com.bigledger.core1.api.ResponseCodeConstants;
import com.bigledger.core1.api.criteria.DocEventCriteria;
import com.bigledger.core1.common.ApiResponse;
import com.bigledger.core1.dto.*;
import com.bigledger.core1.dto.id.LoginResponse;
import com.bigledger.core1.dto.id.PagingResponse;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import app.constants.EventQueueConstants;
import com.amazonaws.services.lambda.runtime.LambdaLogger;


public class TransformCinvToCsv implements RequestHandler<Map<String, String>, Map<String, String>> {

    @Override
    public Map<String, String> handleRequest(Map<String, String> pMap, Context context) {

        final long start = Calendar.getInstance().getTimeInMillis();
        LambdaLogger logger = context.getLogger();

        List<GenericDocHdrDto> genericDocHdrDto = new ArrayList<GenericDocHdrDto>();
        Map<String, String> subquery = new HashMap<String, String>();
        List<String> guids = new ArrayList<>();

        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        System.out.println("Input Map: " + pMap);
        System.out.println("Filename: " + pMap.get("s3Key"));
        System.out.println("Vendor Code: " + pMap.get("vendorCode"));
        System.out.println("Processing Bucket: " + pMap.get("s3BucketProcess"));

        RequestHeaders requestHeaders = new RequestHeaders(pMap.get("token"),pMap.get("tenantCode"),pMap.get("appId"));


        //Query to get all customer sales invoice
//        String cinvSql = "SELECT e1.guid,e1.guid_doc_hdr AS neededguid FROM bl_fi_generic_doc_event AS e1 " +
//                " WHERE e1.event_code = 'SALES_INVOICE_CREATED'" +
//                " AND (e1.guid not in (SELECT e2.link_guid FROM bl_fi_generic_doc_event AS e2 WHERE e2.event_code = 'CKL_SALES_INVOICE_EXPORTED'))" +
//                " LIMIT " + pMap.get("limitExport") + ";";

        String cinvSql = "select e1.guid, e1.guid_doc_hdr as neededguid from bl_fi_generic_doc_event e1 "+
                    " left join bl_fi_generic_doc_event as e2 " +
                    " on e1.guid = e2.link_guid"+
                    " where e2.guid is null and (e1.action = 'SALES_INVOICE_CREATED') "+
                    " LIMIT " + pMap.get("limitExport") + ";";

        subquery.put("subquery", cinvSql);

        System.out.println("Querying database for customer sales invoice");
        System.out.println("vendorCode:"+pMap.get("vendorCode"));
        System.out.println(cinvSql);


        try {


            //  get all customer sales invoice
            ISubqueryService subqueryService = new SubqueryServiceImpl();

            ApiResponse responseSubquery = subqueryService.getGuidsBySubquery(subquery, requestHeaders);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseSubquery.asJson());
            System.out.println("GUIDs: " + json.get("data").toString());


            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            guids = mapper.readValue(json.get("data").toString(), ArrayList.class);

            // Use parallel call to get Customer Sales Invoice DTOs

            ParallelCallsService parallelCallCinvDto = new ParallelCallsService();


            List<ApiResponse> responses = parallelCallCinvDto.getCinvToDos(guids,requestHeaders);

            System.out.println("Count Customer Sales Invoice DTOs: " + responses.size());
            System.out.println("First Customer Sales Invoice DTO: " + responses.get(0));

            for (int x = 0; x < responses.size(); x++){

                JSONParser parser1 = new JSONParser();
                JSONObject json1 = (JSONObject) parser1.parse(responses.get(x).asJson());

                ObjectMapper mapper1 = new ObjectMapper();
                mapper1.registerModule(new JavaTimeModule());
                mapper1.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

                GenericDocHdrDto genericCinvHdrDto = mapper1.readValue(json1.get("data").toString(), GenericDocHdrDto.class);

                if (genericCinvHdrDto.getCustomFieldList() != null) {
                    System.out.println("cinv custom field : " + genericCinvHdrDto.getCustomFieldList().size());
                } else {
                    System.out.println("No custom field");
                    return pMap;
                }


                if (genericCinvHdrDto.getGenericDocLineDtoList() != null) {
                    System.out.println("cinv doc line  " + genericCinvHdrDto.getGenericDocLineDtoList().size());
                } else {
                    System.out.println("No generic doc line");
                    return pMap;
                }

                genericDocHdrDto.add(genericCinvHdrDto);
            }

            System.out.println("No. of Cinvs: " + genericDocHdrDto.size());


            // check if there is a purchase order
            if (genericDocHdrDto.size() > 0) {

                // Use parallel call to get Purchase Order DTOs transformed


                List<List<LimtayarCinvCsvHeader>> limtayarCinvCsvHeader = parallelCallCinvDto.getTransformDtoToCsvToDos(genericDocHdrDto,requestHeaders);

                System.out.println("No of DTOs: " + limtayarCinvCsvHeader.size());

//                String cinv2 = "";

                if (limtayarCinvCsvHeader.size()>0){

                    List<String> dtoInCsv = null;
                    String stringCinvs = "";
                    //Delimiter used in CSV file
                    String PIPE_DELIMITER = "|";
                    String NEW_LINE_SEPARATOR = "\n";
                    //String DOUBLE_QUOTE = "\"";

                    String FILE_HEADER = "Invoice PKID" + "|" + "Jobsheet PKID" + "|"
                            + "Transaction Date" + "|" + "Customer Pkid" + "|"+ "Customer Name" + "|"+  "Branch Code" + "|"  + "Invoice Vehicle Regnum"  +
                            "|" + "Item Code" + "|" + "Item Name"  + "|"  +
                            "Item Description" + "|" + "Quantity" +"|" + "Amount Std"  +
                            "|" + "Currency" + "|"  + "Nett Price(after discount exclude tax)"  +
                            "|" + "Discount"  + "|"  + "Amount Txn" + "|" + "UOM"  +
                            "|" + "GST Tax Amount"  + "|"  + "GST Code"  + "|" + "GST Rate"  +
                            "|" + "GST Type" + "|"  + "Document Type";

                    //Write the CSV file header
                    stringCinvs = stringCinvs + FILE_HEADER;

                    //Add a new line separator after the header
                    stringCinvs = stringCinvs + CsvConstants.NEW_LINE_SEPARATOR;

                    for (int y=0; y< limtayarCinvCsvHeader.size(); y++) {

                        dtoInCsv = parallelCallCinvDto.getWriteDtoToCsvToDos(limtayarCinvCsvHeader.get(y));

                        for (int z=0; z < dtoInCsv.size(); z++) {
                            stringCinvs = stringCinvs + dtoInCsv.get(z);
                        }

                    }

                        // Write transformed DTO to processing s3
                        try {



                            System.out.println("writing CSV to s3Bucket:" + pMap.get("s3BucketProcess") + " s3Key:" + pMap.get("s3Key"));
                            System.out.println("Writing CSV");
                            String csvString = stringCinvs;
//                            final String CONTENT_TYPE = "application/csv";
                            final String CONTENT_TYPE = "plain/text";
                            byte[] fileContentBytes = csvString.getBytes(StandardCharsets.UTF_8);
                            InputStream fileInputStream = new ByteArrayInputStream(fileContentBytes);
                            ObjectMetadata metadata = new ObjectMetadata();
                            metadata.setContentType(CONTENT_TYPE);
                            metadata.setContentLength(fileContentBytes.length);

                            PutObjectRequest putObjectRequest = new PutObjectRequest(
                                    pMap.get("s3BucketProcess"), pMap.get("s3Key"), fileInputStream, metadata);
                            s3Client.putObject(putObjectRequest);
                        } catch (SdkClientException e) {
                            e.printStackTrace();
                            System.out.println(e.getMessage());
                            return pMap;
                        } finally {
                            System.out.println("Exported Cinv in CSV to s3Bucket : " + " " + pMap.get("s3BucketProcess") + " s3Key : " + pMap.get("s3Key"));
                        }

//                    cinv2 = stringCinvs;

                }

                String stringGuids = "";

                String s3Keytemp = "CKL CINV LISTING/"+"_"+guids.get(0)+".json";

                // Write guids to temp json s3
                try {


                    for(int q=0; q<guids.size(); q++)
                    {

                        stringGuids=guids.get(q) + CsvConstants.NEW_LINE_SEPARATOR + stringGuids;

                    }

                    System.out.println("writing CSV to s3Bucket:" + pMap.get("s3BucketTempJson") + " s3Key:" + s3Keytemp);
                    System.out.println("Writing CSV");
                    String csvString = stringGuids;
                    final String CONTENT_TYPE = "application/csv";
                    byte[] fileContentBytes = csvString.getBytes(StandardCharsets.UTF_8);
                    InputStream fileInputStream = new ByteArrayInputStream(fileContentBytes);
                    ObjectMetadata metadata = new ObjectMetadata();
                    metadata.setContentType(CONTENT_TYPE);
                    metadata.setContentLength(fileContentBytes.length);

                    PutObjectRequest putObjectRequest = new PutObjectRequest(
                            pMap.get("s3BucketTempJson"),s3Keytemp, fileInputStream, metadata);
                    s3Client.putObject(putObjectRequest);
                } catch (SdkClientException e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                    return pMap;
                } finally {
                    System.out.println("Exported Cinv in CSV to s3Bucket : " + " " + pMap.get("s3BucketTempJson") + " s3Key : " + s3Keytemp);
                }

                pMap.put("s3KeyGuids", s3Keytemp);

            }
            else {
                System.out.println("No customer sales invoice to export");
                return pMap;
            }


//            try
//            {
//                DocEventCriteria DocEventCriteria = new DocEventCriteria();
//                DocEventCriteria.setGuidDochdr(guids.get(0));
//                DocEventCriteria.setAction("SALES_INVOICE_CREATED");
//                IDocumentEventService CinveventService = new DocumentEventServiceImpl();
//
//                PagingResponse<GenericDocEventDto> CinvEventResponse = CinveventService.getGenericDocEventDtoByCriteriaProcess(DocEventCriteria, requestHeaders);
//                List<GenericDocEventDto> genericDocEventDtosList = CinvEventResponse.getObjectList();
//
//                {
//                    GenericDocEventDto docEventDto_Cinvs = genericDocEventDtosList.get(0);
//
//                    docEventDto_Cinvs.setAction("SALES_INVOICE_CREATED");
//                    genericDocEventDtosList.set(0, docEventDto_Cinvs);
//                    ZonedDateTime currentDate = ZonedDateTime.now(ZoneId.of("Asia/Kuala_Lumpur"));
//
//
//                    GenericDocEventDto docEventDto_Cinv = new GenericDocEventDto();
//
//                    docEventDto_Cinv.setStatus("ACTIVE");
//                    docEventDto_Cinv.setTxnType("SYS_APPLET");
//                    docEventDto_Cinv.setCreatedDate(currentDate);
//                    docEventDto_Cinv.setAction("CKL_SALES_INVOICE_EXPORTED");
//                    docEventDto_Cinv.setEventCode("CKL_SALES_INVOICE_EXPORTED");
//                    docEventDto_Cinv.setLinkGuid(docEventDto_Cinvs.getGuid());
//                    docEventDto_Cinv.setDateTxn(currentDate);
//                    docEventDto_Cinv.setGuidDocHdr(guids.get(0));
//                    genericDocEventDtosList.add(docEventDto_Cinv);
//
//
//                    ApiResponse response1 = CinveventService.createDocumentEvent(docEventDto_Cinv, requestHeaders);
//                    logger.log("response.getCode():" + response1.getCode());
//                    logger.log("response.getMessage():" + response1.getMessage());
//
//                }
//
//
//
////                }
////            else
////                {
////                return soGuidMap;
////            }
////                return soGuidMap;
//
//            }
//            catch (AkaunApiServerException e) {
//                System.out.println("AkaunApiServerException");
//                e.printStackTrace();
//                System.out.println(e.getMessage());
//
//            } catch (NetworkException e) {
//                System.out.println("NetworkException");
//                e.printStackTrace();
//                System.out.println(e.getMessage());
//            } catch (Exception ex) {
//                System.out.println(ex);
//            }

        } catch (AkaunApiServerException e) {
            System.out.println("AkaunApiServerException");
            e.printStackTrace();
            System.out.println(e.getMessage());
            return pMap;

        } catch (NetworkException e) {
            System.out.println("NetworkException");
            e.printStackTrace();
            System.out.println(e.getMessage());
            return pMap;
        } catch (Exception ex) {
            System.out.println(ex);
        }

        final long end = Calendar.getInstance().getTimeInMillis();
        System.out.println(" export cinv time spent: " + (end - start) / 1000 + "s");

        return pMap;

    }





}





