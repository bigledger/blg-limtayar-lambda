package app;

import app.constants.CsvConstants;
import app.constants.LimtayarCinvCsvHeader;

public class WriteDtoToCsv {

    public  String getDtoInCsv(LimtayarCinvCsvHeader cinvDtoMapToCsv) {


        String stringCinvs = "";

        String invPkid="";
        String jobsheetPkid="";
        String txnDate="";
        String customerPkid="";
        String customerName="";
        String branchCode="";
        String invVehicleRegNo="";
        String itemCode="";
        String itemName="";
        String itemDesc="";
        String quantity="";
        String amountStd="0.00";
        String docCcy="";
        String amountNet="0.00";
        String amountDiscount="0.00";
        String amountTxn="0.00";
        String uom="";
        String gstAmount="0.00";
        String gstCode="";
        String gstRate="0.00";
        String gstType="";
        String docType="";


        if(cinvDtoMapToCsv.getInvPkid()!=null) {
            invPkid = cinvDtoMapToCsv.getInvPkid();
        }
        if(cinvDtoMapToCsv.getJobsheetPkid()!=null) {
            jobsheetPkid = cinvDtoMapToCsv.getJobsheetPkid();
        }
        if(cinvDtoMapToCsv.getTxnDate()!=null) {
            txnDate = cinvDtoMapToCsv.getTxnDate();
        }
        if(cinvDtoMapToCsv.getCustomerPkid()!=null) {
            customerPkid = cinvDtoMapToCsv.getCustomerPkid();
        }
        if(cinvDtoMapToCsv.getCustomerName()!=null) {
            customerName = cinvDtoMapToCsv.getCustomerName();
        }
        if(cinvDtoMapToCsv.getBranchCode()!=null) {
            branchCode = cinvDtoMapToCsv.getBranchCode();
        }
        if(cinvDtoMapToCsv.getInvVehicleRegNo()!=null) {
            invVehicleRegNo = cinvDtoMapToCsv.getInvVehicleRegNo();
        }
        if(cinvDtoMapToCsv.getItemCode()!=null) {
            itemCode = cinvDtoMapToCsv.getItemCode();
        }
        if(cinvDtoMapToCsv.getItemName()!=null) {
            itemName = cinvDtoMapToCsv.getItemName();
        }
        if(cinvDtoMapToCsv.getItemDesc()!=null) {
            itemDesc = cinvDtoMapToCsv.getItemDesc();
        }
        if(cinvDtoMapToCsv.getQuantity()!=null) {
            quantity = cinvDtoMapToCsv.getQuantity();
        }
        if(cinvDtoMapToCsv.getAmountStd()!=null) {
            amountStd = cinvDtoMapToCsv.getAmountStd();
        }
        if(cinvDtoMapToCsv.getDocCcy()!=null) {
            docCcy = cinvDtoMapToCsv.getDocCcy();
        }
        if(cinvDtoMapToCsv.getAmountNet()!=null) {
            amountNet = cinvDtoMapToCsv.getAmountNet();
        }
        if(cinvDtoMapToCsv.getAmountDiscount()!=null) {
            amountDiscount = cinvDtoMapToCsv.getAmountDiscount();
        }
        if(cinvDtoMapToCsv.getAmountTxn()!=null) {
            amountTxn = cinvDtoMapToCsv.getAmountTxn();
        }
        if(cinvDtoMapToCsv.getUom()!=null) {
            uom = cinvDtoMapToCsv.getUom();
        }
        if(cinvDtoMapToCsv.getGstAmount()!=null) {
            gstAmount = cinvDtoMapToCsv.getGstAmount();
        }
        if(cinvDtoMapToCsv.getGstCode()!=null) {
            gstCode = cinvDtoMapToCsv.getGstCode();
        }
        if(cinvDtoMapToCsv.getGstRate()!=null) {
            gstRate = cinvDtoMapToCsv.getGstRate();
        }
        if(cinvDtoMapToCsv.getGstType()!=null) {
            gstType = cinvDtoMapToCsv.getGstType();
        }
        if(cinvDtoMapToCsv.getDocType()!=null) {
            docType = cinvDtoMapToCsv.getDocType();
        }


        stringCinvs = invPkid + CsvConstants.BAR_DELIMITER +
                jobsheetPkid + CsvConstants.BAR_DELIMITER +
                txnDate  +   CsvConstants.BAR_DELIMITER +
                customerPkid + CsvConstants.BAR_DELIMITER +
                customerName + CsvConstants.BAR_DELIMITER +
                branchCode + CsvConstants.BAR_DELIMITER +
                invVehicleRegNo + CsvConstants.BAR_DELIMITER +
                itemCode +  CsvConstants.BAR_DELIMITER +
                itemName +  CsvConstants.BAR_DELIMITER +
                itemDesc + CsvConstants.BAR_DELIMITER +
                quantity + CsvConstants.BAR_DELIMITER +
                amountStd + CsvConstants.BAR_DELIMITER +
                docCcy +  CsvConstants.BAR_DELIMITER +
                amountNet  + CsvConstants.BAR_DELIMITER +
                amountDiscount + CsvConstants.BAR_DELIMITER +
                amountTxn  + CsvConstants.BAR_DELIMITER +
                uom + CsvConstants.BAR_DELIMITER +
                gstAmount + CsvConstants.BAR_DELIMITER +
                gstCode +  CsvConstants.BAR_DELIMITER +
                gstRate +  CsvConstants.BAR_DELIMITER +
                gstType +  CsvConstants.BAR_DELIMITER +
                docType + CsvConstants.NEW_LINE_SEPARATOR;

        return stringCinvs;

    }
}
