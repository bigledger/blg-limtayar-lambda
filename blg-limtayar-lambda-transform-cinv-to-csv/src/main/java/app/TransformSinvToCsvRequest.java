package app;
public class TransformSinvToCsvRequest {

    private String input;

    public TransformSinvToCsvRequest(String input) {
        this.input = input;
    }

    public TransformSinvToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
