package app;

public class TransformSinvToCsvResponse {

    private String hello;

    public TransformSinvToCsvResponse(String hello) {
        this.hello = hello;
    }

    public TransformSinvToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
