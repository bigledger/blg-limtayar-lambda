package app;

public class ExportSalesReturnToCsvResponse {

    private String hello;

    public ExportSalesReturnToCsvResponse(String hello) {
        this.hello = hello;
    }

    public ExportSalesReturnToCsvResponse() {
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

}
