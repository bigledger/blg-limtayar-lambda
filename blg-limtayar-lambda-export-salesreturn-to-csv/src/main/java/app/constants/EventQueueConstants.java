package app.constants;

public interface EventQueueConstants {


    //EMP QUEUE	EVENTS

    String TXN_TYPE = "SYS_APPLET";

    String MESSAGE_EXPORT_EVENT_CODE = "CKL_SALES_RETURN_EXPORTED";
    String MESSAGE_EXPORT_ACTION = "CKL_SALES_RETURN_EXPORTED";
    String MESSAGE_EXPORT_DESC = "CKL SALES RETURN HAS BEEN EXPORTED";


    //Sleep timers
    Integer HOUR = 3600;
    Integer ONE_MINUTE = 60;
    Integer FIVE_MINUTES = ONE_MINUTE * 5;
    Integer TEN_MINUTES = ONE_MINUTE * 10;
    Integer DAY = HOUR * 24;
}
