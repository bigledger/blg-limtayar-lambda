package app;
public class ExportSalesReturnToCsvRequest {

    private String input;

    public ExportSalesReturnToCsvRequest(String input) {
        this.input = input;
    }

    public ExportSalesReturnToCsvRequest() {
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
